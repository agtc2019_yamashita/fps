class FpsCamera
{
	public :
		FpsCamera() ;  // コンストラクタ
		~FpsCamera() ; // デストラクタ

		void InitSet( int mode , float Hrot ) ;		// プレイヤーモードの設定等初期セット
		void HorizonAngle()  ;			// キー入力&水平方向の回転
		void SetPos( VECTOR ppos ) ;		// カメラの座標セット

		// 変数-----------------------------------
		int pmode		;  // 1pか2pかチェック  0:1p  1:2p
		float HRotate	;  // 水平方向カメラ回転用 
		VECTOR pos		;  // プレイヤーのポジションをセット
		int operation_mode ;	// 操作方法管理 0:LRカメラ	1:○■カメラ

		VECTOR direction ; // 向き
		VECTOR ctarget   ; // 注視点
		VECTOR cposition ; // 位置
		
} ;