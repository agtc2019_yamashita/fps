/* -----------------------------------------------------------------------------------------
|	
|   プレイヤー　やられ時処理
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
void G_Respawn()
{

	// Go画像アニメーション-----------------------------
	// アルファ値を変化
	goalpha += goadd ;
	go_size -= 0.05 ; 

	if ( go_size <= 3.0)
		go_rot -= 0.1 ;

	if ( go_size <= 0.0 ){
		game_mode = eG_Play ; 
		goalpha = 1;
		goadd = 6 ;
		go_size = 5.0 ;
		go_rot = 0.0 ;
	}
	// アルファ値が 0 か 255 になったら変化の方向を反転する
	if( goalpha <= 0 || goalpha >= 255 )
	{
		goadd = -goadd ;
	}

	// カメラの設定
	p_cam[PLAYER1].SetPos( g_player[PLAYER1].pos ) ;
	p_cam[PLAYER2].SetPos( g_player[PLAYER2].pos ) ;

	// 王冠ポジション決定-----------------------------
	for ( int i = 0 ; i < 2 ; i++ ){
		crown.CrownMove( i ) ;
	}

	drone.Move() ;	// ドローン

	// --- 風船移動
	for ( int i = 0 ; i < 10 ; i++ )
		balloon[i].Move() ;
	// 風船アニメーション
	for ( int i = 0 ; i < 10 ; i++ ){
		balloon[i].Play_Anim( ) ;
		MV1SetAttachAnimTime( balloon[i].mh_balloon,balloon[i].attachidx,balloon[i].playtime) ;
	}

	//爆発の演出
	for ( int y = 0  ; y < 2 ; y++ ){                     
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].exploflg == TRUE ){
				p_bullet[y][x].Boomhandl() ;
			}
		}
	}

	// 弾のヒットチェック
	for ( int y = 0 ,  i = 1 ; y < 2 ; y++ , i-- ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE ){
				p_bullet[y][x].BulletHitCheck( g_player[i].pos ) ;
			}
		}
	}
	// 弾アクション---------------------------------
	for ( int y = 0 ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE )
				p_bullet[y][x].Move() ;
		}
	}


	// アニメーション-----------------------------------------------------------------------
	for ( int i = 0 ; i < 2 ; i++ ){
		g_player[i].AnimMode_Run() ;
		g_player[i].Play_Anim() ;
		MV1SetAttachAnimTime(g_player[i].model_hnd,g_player[i].attachidx,g_player[i].playtime) ;
	}

	// 描画--------------------------------------------------------------------------
	for ( int i = 0 ; i < 2 ; i++ ){	
		SetCameraNearFar( 35.0f,  150000.0f ) ;								// 描画距離を設定
		SetCameraPositionAndAngle( p_cam[i].pos , 0, p_cam[i].HRotate, 0 ) ;	// カメラの位置と向きの設定
		SetDrawArea( (WINDOWSIZE_W/2) *i, 0, WINDOWSIZE_W/2*(i+1), WINDOWSIZE_H );	                // 画面分割設定(描画範囲)
		SetCameraScreenCenter( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i), WINDOWSIZE_H/2 );            // 画面分割設定
		// 各モデルのMV1DrawModelを実行
		DrawLoop() ;
	}

	SetDrawArea( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H );		// 画面分割の設定(描画範囲)

	UIDrawLoop() ;

	// GO表示--------------------------------------------
	SetDrawBlendMode( DX_BLENDMODE_ALPHA, goalpha ) ;
	DrawRotaGraph( (WINDOWSIZE_W / 2)  , WINDOWSIZE_H/2 , go_size  , go_rot , gh_go , true , 0, 0 ) ;
	SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

}

