
/* -----------------------------------------------------------------------------------------
|
|
|       DEFINE宣言
|
|
+ --------------------------------------------------------------------------------------- */
#define CAMROTSPD		120.0f									// カメラの回転スピード　大きくなるほど遅くなる
#define PLAYER1			0										// 操作の識別に必要 
#define PLAYER2			1				                        // 操作の識別に必要
#define P1_INIT_POS		VGet( -1150.0f , -3.0f , -1900.0f )		// プレイヤー1初期座標
#define P2_INIT_POS		VGet(  1150.0f , -3.0f ,  1900.0f )		// プレイヤー2初期座標
#define PLYSPD			10.0f									// プレイヤー移動速度
#define WINDOWSIZE_W    1920									// ウィンドウサイズ横幅
#define WINDOWSIZE_H    1080									// ウィンドウサイズ縦幅
#define P1_INIT_ROT		0.0f									// プレイヤー1初期方向
#define P2_INIT_ROT     3.14f									// プレイヤー2初期方向
#define BOUNDMAX		2										// ボールの反射回数
#define BULLETMAX		5										// 弾発射最大数
#define BULLETSPD		18.0f									// 弾の速度
#define BLOCKMAX		9										// 箱の最大数
#define CHAIRMAX		6										// 椅子の最大数
#define BANNER_POSX		0										// バナーのXポジション
#define BANNER_POSZ		0										// バナーのYポジション
// --- 制限時間用DEFINE
#define GAMETIME		32										// ゲームトータル時間

#define FINISHTIME      120										// 終了後リザルトに移るまでの時間
#define NUMBERSIZE_W	86										// 数字画像の横幅
#define NUMBERSIZE_H	114										// 数字画像の縦幅

// --- 得点用DEFINE
#define SCORESIZE_W		62										// スコア画像の横幅
#define SCORESIZE_H		73										// スコア画像の縦幅

// --- ミニマップ用DEFINE
#define MINIMAPSIZE_W	380										// ミニマップ画像の横幅
#define MINIMAPSIZE_H	230										// ミニマップ画像の縦幅

#define STAGESCALE		2.5f									// マップのサイズの倍率

// ヒットチェック用-------------------------------------
// ステージ　プレイヤー
#define STAGESIZE_X		460.0f									// マップのXサイズ
#define STAGESIZE_Z		760.0f									// マップのZサイズ
// ステージ　弾
#define STAGESIZE_BX	490.0f									// マップのXサイズ
#define STAGESIZE_BZ	790.0f									// マップのZサイズ
// ブロック プレイヤー
#define BLOCKSIZE_OFF	180.0 * 0.95f							// ブロックの大きさ(ブロックの中心からの距離)
// ブロック 弾
#define BLOCKSIZE_BOFF	110.0f * 0.8f							// ブロックの大きさ(ブロックの中心からの距離)
// 椅子プレイヤー
#define CHAIRSIZEX_OFF	270.0f * 0.95f					
#define CHAIRSIZEZ_OFF	180.0f * 0.95f							
// ブロック　弾
#define CHAIRSIZEX_BOFF	210.0f * 0.8f
#define CHAIRSIZEZ_BOFF	110.0f * 0.8f

// シーンチェンジ用----------------------------------------
#define FAST			0										// シーンチェンジスピード [速い]
#define SLOW			1										// シーンチェンジスピード [遅い]

// サウンド用---------------------------------------------

#define BGM_VOLUME 85											//  BGMの音量
#define SHOOT_VOLUMN 255										//  射撃の音量
#define BOOM_VOLUMN 200											//  爆発の音量
#define CHARASLC_VOLUMN 200                                     //  キャラ選択音量
#define COMMIT_VOLUMN 200                                       //  確定の音量
#define CANCEL_VOLUMN 200                                       //  キャンセル音量

enum ActionMode{ eStand, eRun, eDeth , eWin , eTotal , eNeetTitle , eMagicTitle , eTenshTitle } ;			    // アニメーション モード
enum PlayerModel{ Magic , Neet , Angel } ;								// プレイヤーモデル
enum GameMode{  eG_Init      , eTitle  , eCharaSelect , eCharaShow , 
				eG_Start = 4 , eG_Play , eG_Deth      , eG_Respawn , 
				eG_SceneChange , eG_End	 = 9 , eResult  } ;								// ゲーム モード
enum NumberColor{ n_Green , n_Orange , n_Red } ;						// --- 数字色
enum Sound_BGM{ s_Title , s_CharaSelect , s_GamePlay , s_Result } ;		// --- BGM(追加するときはここと、クラスに記入)
enum Sound_SE{ s_Shot , s_Explosion , s_Charaslc , s_Commit , s_Cancel , s_Ready , s_Cursor , s_Countdown ,
				s_Roll , s_Jan , s_Crownget , s_Vs , s_Kansei } ;				// --- SE(追加するときはここと、クラスに記入)

/* -----------------------------------------------------------------------------------------
|
|       共通ヘッダー
|
+ --------------------------------------------------------------------------------------- */
#include <DxLib.h> 
#include "Player.h"
#include "Camera.h"
#include "Bullet.h"
#include "Stage.h"
#include "Crown.h"
#include "Time.h"
#include "MiniMap.h"
#include "Sound.h"
#include "Drone.h"
#include "Transition.h"
#include "Balloon.h"

/* -----------------------------------------------------------------------------------------
|
|
|       型宣言
|
|
+ --------------------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------------------
|
|
|       プロトタイプ宣言
|
|
+ --------------------------------------------------------------------------------------- */
void DrawLoop( void ) ;
void UIDrawLoop() ;				// --- UI画像描画まとめ
int  StageInit() ;				// --- ステージ初期セット(引数 : 特になし)
void G_Init() ;
void Title() ;					// --- タイトル処理
void CharaSelect() ;			// --- キャラ選択処理
void CharaShow() ;				// --- 対戦前キャラ表示
void G_Play() ;					// --- ゲームプレイ中処理
void G_Deth() ;					// --- プレイヤーやられ時処理
void G_End() ;					// --- ゲーム終了＆メモリ開放処理
void G_Respawn() ;				// --- リスポーン処理
void DethInit() ;				// --- 死亡時初期セット
void G_SceneChange( ) ;			// --- シーンチェンジ(30秒後処理)
void Result() ;					// --- 結果発表

// --- クラス化されていないちょっとした関数
void ScoreCount() ;								// --- 得点処理
void ManualDraw() ;								// --- 操作説明描画
void BB_HitCheck(int b_posy , int b_posx) ;		// --- 弾と弾のヒットチェック
void SC_UIMove() ;								// --- シーンチェンジ時のUI処理

/* -----------------------------------------------------------------------------------------
|
|
|       外部参照宣言
|
|
+ --------------------------------------------------------------------------------------- */
extern Player		g_player[2] ;				// プレイヤーオブジェクト	要素数 0 : 1P , 1 : 2P
extern Player		t_player[3] ;				// タイトルプレイヤーオブジェクト	要素数 0 : マジック , 1 : ニート , 2 : エンジェル
extern FpsCamera    p_cam[2] ;					// カメラオブジェクト		要素数 0 : 1P , 1 : 2P
extern Bullet       p_bullet[2][BULLETMAX+1] ;	// 弾オブジェクト			要素数 0 : 1P , 1 : 2P
extern Stage		stage[BLOCKMAX] ;			// ステージオブジェクト		要素数 ステージ配置木箱数
extern Crown		crown ;						// 王冠オブジェクト
extern Time			limtime ;					// 制限時間オブジェクト
extern MiniMap		minimap ;					// ミニマップオブジェクト
extern Sound		sound ;						// サウンドオブジェクト
extern Drone		drone ;						// ドローンオブジェクト
extern Transition	trans ;						// シーンチェンジオブジェクト
extern Balloon		balloon[10] ;				// バルーンオブジェクト

extern int			game_mode		;		// ゲームモード管理用
extern VECTOR       dethpos			;		// プレイヤー死亡時座標
extern int			dethplayer		;		// 死亡プレイヤー
extern VECTOR		balloonpos[10]  ;		// バルーンオブジェクト

extern float        skyroll         ;       // 空を回転させる

// モデルハンドル------------------------------------------------
extern int			ground			;		// 地面				モデル
extern int			ground_castle	;		// 地面				モデル
extern int			kabe			;		// 壁				モデル
extern int			sky				;		// 空				モデル
extern int			sky_castle		;		// 空				モデル
extern int			player1			;		// プレーヤー		モデル
extern int			player2			;		// プレーヤー		モデル
extern int			mh_players[2][3];		// プレイヤーのモデルハンドル
extern int			bullet[2]		;		// 弾丸				モデル
extern int		    land			;		// 土台				モデル
extern int			cursor[2]		;		// 矢印				モデル
extern int          boom            ;		// 爆発
extern int          boom2           ;		// 爆発             モデル
extern int          p1mark          ;       // p1ポインタ       モデル
extern int          p2mark          ;       // p2ポインタ       モデル
extern int			castle			;		// 城				モデル
extern int			door_1			;		// ドア				モデル
extern int			door_2			;		// ドア				モデル
extern int			m_banner_red	;		// 赤バナー			モデルハンド
extern int			m_banner_blue	;		// 青バナー			モデルハンド
extern VECTOR		rbanner_pos		;		// 赤バナー			ポジション
extern VECTOR		bbanner_pos		;		// 青バナー			ポジション

// UI画像ハンドル------------------------------------------------
extern int			gh_usebullet[2]	;		// 赤弾						画像ハンドル
extern int			gh_nonbullet	;		// 白弾						画像ハンドル
extern int			gh_stage		;		// ステージ					画像ハンドル
extern int			gh_minicube		;		// ミニマップ木箱			画像ハンドル
extern int			gh_miniplayer[2];		// ミニマッププレイヤー		画像ハンドル
extern int			gh_minichair	;		// ミニマップ椅子
extern int			gh_numberback	;		// 数字背景					画像ハンドル
extern int			gh_number[3]	;		// 数字						画像ハンドル
extern int			gh_score		;		// SCORE					画像ハンドル
extern int			gh_go			;		// GO						画像ハンドル
extern int			gh_finish		;		// 終了						画像ハンドル
extern int			gh_lrbutton		;		// 操作説明LRボタン			画像ハンドル
extern int			gh_atkbutton	;		// 操作説明攻撃ボタン		画像ハンドル
extern int			gh_cmrbutton	;		// 操作説明カメラボタン		画像ハンドル
extern int			gh_lratk		;		// 操作説明カメラボタン		画像ハンドル
extern int			gh_oukanget		;		// 操作説明王冠取得ボタン	画像ハンドル
extern int			gh_oukangetaf	;		// 操作説明王冠取得後ボタン	画像ハンドル
extern int			gh_oukandes		;		// 操作説明王冠ロスボタン	画像ハンドル
extern int			gh_blackball	;
extern int			gh_charatrans	;		// キャラトランジション		画像ハンドル
extern int			gh_charatransbk	;		// キャラトランジション背景	画像ハンドル
extern int			gh_option_A		;		// 操作方法○射撃
extern int			gh_option_B		;		// 操作方法LR射撃
extern int			gh_charasel_bo	;		// キャラ選択ブラックアウト
extern int			gh_option_bo	;		// 操作方法ブラックアウト
extern int			gh_start		;		// スタート用				画像ハンドル
extern int			gh_number321[3]	;		// 1 2 3　が入る			画像ハンドル
extern int			gh_V			;		// V						画像ハンドル
extern int			gh_S			;		// S						画像ハンドル
extern int			gh_sc_font		;		// シーンチェンジ時文字		画像ハンドル
extern int			gh_sc_fontbk	;		// シーンチェンジ時文字背景	画像ハンドル
extern int			gh_brackoutsc	;		// 暗転処理					画像ハンドル
extern int			gh_1P_deth		;		// 1Pやられ表示				画像ハンドル
extern int			gh_2P_deth		;		// 2Pやられ表示				画像ハンドル

extern int          shadow1         ;		// 弾丸の影					画像ハンドル
extern int			gh_explosion	;		// 爆発						画像ハンドル
extern int			gh_waku			;		// 枠線						画像ハンドル
extern int			gh_charaselect_waku ;	// キャラセレ時の枠			画像ハンドル
extern int			gh_kill			;		// キル						画像ハンドル
extern int			gh_ready		;		// 準備完了					画像ハンドル
extern int			gh_title[7]		;		// タイトル文字				画像ハンドル
extern int			gh_subtitle		;		// サブタイトル文字			画像ハンドル
extern int			gh_startop		;		// スタートボタン			画像ハンドル

extern double		go_size			;		// GO画像サイズ
extern double		go_rot			;		// GO画像回転
extern int			goalpha			;		// 画像透過変数 
extern int			goadd			;		// 画像透過変数 
extern int			scf_alpha		;		// スクリーンチェンジ透過
extern int			scbk_alpha		;		// スクリーンチェンジ背景透過
extern float		scf_posx		;		// スクリーンチェンジ文字座標
extern bool			sc_finish		;		// スクリーンチェンジ文字終了
extern bool			sc_fin_fun		;		// スクリーンチェンジ関数終了チェックフラグ
extern int			usebulletcnt	;		// 使われている弾カウント
extern int			infotime		;		// お知らせ描画時間
extern int			rd				;		// ランダムくん

extern int			Pad[2]			;		// ジョイパッド入力 (要素数 0 : 1P , 1 : 2P)

extern int			add_bullet 		;		// 30秒処理で増やす弾数


