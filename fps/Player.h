class Player
{
	public :
	Player() ;	// コンストラクタ
	~Player() ; // デストラクタ
	
	void InitSet( int mode , VECTOR initpos , int model , int playertype ) ;     // プレイヤーモードとモデル取得　初期座標のセット
	void TitleInitSet( int mode , VECTOR initpos , int model , int playertype ) ;// タイトル時にアニメーションセット
	void pAction( ) ;								// キー入力移動 射撃
	void Move() ;									// 移動
	void PlayerHitcheck( VECTOR blockpos ) ;		// プレイヤと壁の当たり判定
	void pRespawn( VECTOR respos ) ;				// リスポーン処理
	int AnimMode_Run() ;			// 走り状態に切替え
	int AnimMode_Nutral() ;			// 立ち状態に切替え
	int AnimMode_Deth() ;			// やられ
	int AnimMode_Win() ;			// 勝ちアニメ
	int Play_Anim() ;				// アニメーションの再生

	// 変数------------------------------------
	int pmode ;				// 1pか2pかチェック  0:1p  1:2p
	int model_hnd ;			// モデルハンドル
	int operation_mode ;	// 操作方法管理 0:○射撃	1:LR射撃

	VECTOR pos ;	// ポジション
	VECTOR spd ;	// スピード
//	float direction ;// 向いてる向き
	bool shotflg ;	// 連射管理 
	bool dethflg ;	// 生死管理
	int killcnt ;
	int dethcnt ;
	int crownkeep ;	// 王冠保持時間
	int	shadow	; // プレイヤ影のハンドル

	// アニメーション関連変数------------------------------------
	int rootflm						;			// プレイヤーモデルのルートフレーム（骨組み）の位置を格納
	int anim_nutral					;			// プレイヤーの立ちアニメーション格納ハンドル
	int anim_run					;			// プレイヤーの走りアニメーション格納ハンドル
	int anim_deth					;			// プレイヤーのやられアニメーション格納ハンドル
	int anim_win					;			// プレイヤーの勝ちアニメーション格納ハンドル
	int animtitle_n					;			// ニートのタイトルアニメーション格納ハンドル
	int animtitle_m					;			// マジックのタイトルアニメーション格納ハンドル
	int animtitle_t					;			// 天使のタイトルアニメーション格納ハンドル
	int attachidx					;			// キャラのアタッチインデックス
	float anim_totaltime			;			// 現在のキャラアニメーションの総時間
	float playtime					;			// キャラのアニメ進捗時間
	int tmp_attachidx				;			// アニメ総時間取得のための一時変数
	bool running					;			// 走り中かどうかのフラグ
	float anim_action_totaltime[10] ;			// 各アクションのアニメ総時間の格納配列
	int nowanim						;			// 現在再生しているアニメーションを設定　enumの定数を使用




} ;