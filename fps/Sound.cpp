/* -----------------------------------------------------------------------------------------
|	
|   BGM ・ SE 処理
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"

/* ----------------------------------------------------- */
/*					　コンストラクタ					 */
/* ----------------------------------------------------- */
Sound::Sound() {
}

/* ----------------------------------------------------- */
/*					　デストラクタ						 */
/* ----------------------------------------------------- */
Sound::~Sound() {

}

/* ----------------------------------------------------- */
/*					　サウンド初期セット				 */
/* ----------------------------------------------------- */
void Sound::SoundInit() {

	changesoundflg = FALSE ;

	// BGM-------------------------------------------------------------------------------------
	SetCreateSoundDataType( DX_SOUNDDATATYPE_FILE ) ;
	

	b_title_handle = LoadSoundMem( "..\\Data\\Sound\\BGM\\Title.wav" ) ;			// --- BGM読み込み

	b_charaslc_handle = LoadSoundMem( "..\\Data\\Sound\\BGM\\CharaSelect.wav" ) ;	// --- BGM読み込み

	b_gplay_handle = LoadSoundMem( "..\\Data\\Sound\\BGM\\Game.wav" ) ;				// --- BGM読み込み

	b_g2play_handle = LoadSoundMem( "..\\Data\\Sound\\BGM\\Game2.wav" ) ;			// --- BGM読み込み

	b_result_handle = LoadSoundMem( "..\\Data\\Sound\\BGM\\Result.wav" ) ;			// --- BGM読み込み
  
	// 効果音-------------------------------------------------------------------------------------------
	s_boom			= LoadSoundMem( "..\\Data\\Sound\\SE\\Boom.wav" ) ;			// --- 爆発音			読み込み

	s_shoot			= LoadSoundMem( "..\\Data\\Sound\\SE\\Shot.mp3" ) ;			// --- ショット音		読み込み

	s_charaslc		= LoadSoundMem( "..\\Data\\Sound\\SE\\sword.mp3" ) ;        // --- キャラ選音		読み込み

	s_commit		= LoadSoundMem( "..\\Data\\Sound\\SE\\picon.wav" ) ;        // --- 決定音			読み込み

	s_cancel		= LoadSoundMem( "..\\Data\\Sound\\SE\\konnga.wav" ) ;       // --- キャンセル音　	読み込み        

	s_ready			= LoadSoundMem( "..\\Data\\Sound\\SE\\ready.mp3" ) ;		// --- 準備完了音

	s_cursor		= LoadSoundMem( "..\\Data\\Sound\\SE\\cursor.mp3" ) ;		// --- 操作選択

	s_countdown		= LoadSoundMem( "..\\Data\\Sound\\SE\\countdownT.wav" ) ;	// --- カウントダウン

	s_roll			= LoadSoundMem( "..\\Data\\Sound\\SE\\snareroll.mp3" ) ;	// --- スネアロール

	s_jan			= LoadSoundMem( "..\\Data\\Sound\\SE\\jean1.mp3" ) ;		// --- スネアロール締め

	s_crownget		= LoadSoundMem( "..\\Data\\Sound\\SE\\008_se_kira8.wav" ) ;	// --- 王冠ゲット

	s_kansei		= LoadSoundMem( "..\\Data\\Sound\\SE\\kansei.mp3" ) ;		// --- 勝利後歓声
	
	s_vs = LoadSoundMem( "..\\Data\\Sound\\SE\\008_se_kira8.wav" ) ;			// --- VS
	

	volume_title = 0 ;
	volume_charaselect = 50 ;
	volume_gameplay = 0 ;
	volume_gameplay2 = 0 ;
	volume_result = 0 ;
}

/* ----------------------------------------------------- */
/*					　サウンド再生(BGM)					 */
/* ----------------------------------------------------- */
void Sound::PlaySoundBGM( ) {
	
	switch( now_playsound ) {

		// --- タイトルBGM
		case s_Title :
			ChangeVolumeSoundMem(volume_title, b_title_handle) ;				// --- 音量調整
			PlaySoundMem( b_title_handle,DX_PLAYTYPE_LOOP,TRUE ) ;				// --- BGMプレー
			break ;

		// --- キャラセレクトBGM
		case s_CharaSelect :
			ChangeVolumeSoundMem(volume_charaselect, b_charaslc_handle) ;		// --- 音量調整
			PlaySoundMem( b_charaslc_handle,DX_PLAYTYPE_LOOP,TRUE ) ;			// --- BGMプレー
			break ;

		// --- ゲームBGM
		case s_GamePlay :
			if ( changesoundflg == 0 ) {
				ChangeVolumeSoundMem(volume_gameplay, b_gplay_handle) ;			// --- 音量調整
				PlaySoundMem( b_gplay_handle,DX_PLAYTYPE_LOOP,TRUE ) ;			// --- BGMプレー
			}
			else {
				ChangeVolumeSoundMem(volume_gameplay, b_g2play_handle) ;		// --- 音量調整
				PlaySoundMem( b_g2play_handle,DX_PLAYTYPE_LOOP,TRUE ) ;			// --- BGMプレー
			}
			break ;

		// --- リザルトBGM
		case s_Result :			
			ChangeVolumeSoundMem(volume_result, b_result_handle) ;				// --- 音量調整
			PlaySoundMem( b_result_handle,DX_PLAYTYPE_LOOP,TRUE ) ;				// --- BGMプレー
			break ;
	}

}

/* ----------------------------------------------------- */
/*					　サウンド再生(SE)					 */
/* ----------------------------------------------------- */
void Sound::PlaySoundSE( int sound_number ) {

	switch( sound_number ) {

		// --- 爆発音再生
		case s_Explosion :
			ChangeVolumeSoundMem(BOOM_VOLUMN, s_boom);
			PlaySoundMem( s_boom , DX_PLAYTYPE_BACK , TRUE ) ;
			break ;

		// --- 射撃音再生
		case s_Shot :
			ChangeVolumeSoundMem( SHOOT_VOLUMN, s_shoot ) ;
			PlaySoundMem( s_shoot , DX_PLAYTYPE_BACK , TRUE ) ;
			break ;

		// --- キャラ選音再生
		case s_Charaslc :
			ChangeVolumeSoundMem( CHARASLC_VOLUMN, s_charaslc ) ;
			PlaySoundMem( s_charaslc , DX_PLAYTYPE_BACK , TRUE ) ;
			break ;
		
		// --- キャラ確定
		case s_Commit :
			ChangeVolumeSoundMem( COMMIT_VOLUMN, s_commit ) ;
			PlaySoundMem( s_commit , DX_PLAYTYPE_BACK , TRUE ) ;
			break ;

		// --- キャラキャンセル音
		case s_Cancel :
			ChangeVolumeSoundMem( CANCEL_VOLUMN, s_cancel ) ;
			PlaySoundMem( s_cancel , DX_PLAYTYPE_BACK , TRUE ) ;
			break ;

		// --- 準備完了音
		case s_Ready :
			ChangeVolumeSoundMem( CANCEL_VOLUMN, s_ready ) ;
			PlaySoundMem( s_ready , DX_PLAYTYPE_BACK , TRUE ) ;
			break ;

		// --- 操作選択
		case s_Cursor :
			ChangeVolumeSoundMem(CANCEL_VOLUMN, s_cursor) ;				// --- 音量調整
			PlaySoundMem( s_cursor,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

		// --- カウントダウン
		case s_Countdown :
			ChangeVolumeSoundMem(225, s_countdown) ;				// --- 音量調整
			PlaySoundMem( s_countdown,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

		// --- スネアロール
		case s_Roll :
			ChangeVolumeSoundMem(225, s_roll) ;				// --- 音量調整
			PlaySoundMem( s_roll,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

		// --- スネアロール締め
		case s_Jan :
			ChangeVolumeSoundMem(225, s_jan) ;				// --- 音量調整
			PlaySoundMem( s_jan,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

		// --- 王冠ゲット
		case s_Crownget :
			ChangeVolumeSoundMem(225, s_crownget) ;				// --- 音量調整
			PlaySoundMem( s_crownget,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

		// --- VS時
		case s_Vs :
			ChangeVolumeSoundMem(225, s_vs) ;				// --- 音量調整
			PlaySoundMem( s_vs,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

		// --- 勝利後歓声
		case s_Kansei :
			ChangeVolumeSoundMem(225, s_kansei) ;				// --- 音量調整
			PlaySoundMem( s_kansei,DX_PLAYTYPE_BACK,TRUE ) ;				// --- BGMプレー
			break ;

	}
}

/* ----------------------------------------------------- */
/*					　音量を上げる					 */
/* ----------------------------------------------------- */
void Sound::VolumeAdd( ) {

	switch( now_playsound ) {

		// --- タイトルBGM
		case s_Title :
			if ( volume_title < BGM_VOLUME ) {
				fade_out++ ;
				if ( fade_out > 2 ) {
					fade_out = 0 ;
					volume_title++ ;
				}
				ChangeVolumeSoundMem(volume_title, b_title_handle) ;				// --- 音量調整
			}
			break ;

		// --- キャラセレクトBGM
		case s_CharaSelect :
			fade_out = 0 ;			// --- 値初期化
			if ( volume_charaselect < BGM_VOLUME ) {
				volume_charaselect++ ;
				ChangeVolumeSoundMem(volume_charaselect, b_charaslc_handle) ;				// --- 音量調整
			}
			break ;

		// --- ゲームBGM
		case s_GamePlay :
			if ( changesoundflg == 0 ) {
				if ( volume_gameplay < BGM_VOLUME ) {
					volume_gameplay++ ;
					ChangeVolumeSoundMem(volume_gameplay, b_gplay_handle) ;				// --- 音量調整
				}
			}
			else {
				if ( volume_gameplay2 < BGM_VOLUME ) {
					volume_gameplay2++ ;
					ChangeVolumeSoundMem(volume_gameplay2, b_g2play_handle) ;				// --- 音量調整
				}
			}
			break ;

		// --- リザルトBGM
		case s_Result :
			if ( volume_result < BGM_VOLUME ) {
				volume_result++ ;
				ChangeVolumeSoundMem(volume_result, b_result_handle) ;				// --- 音量調整
			}
			break ;
	}

}

/* ----------------------------------------------------- */
/*					　音量を下げる						 */
/* ----------------------------------------------------- */
void Sound::VolumeSub( ) {

	switch( now_playsound - 1 ) {

		// --- タイトルBGM
		case s_Title :
			if ( volume_title > 0 ) {
				volume_title-- ;
				ChangeVolumeSoundMem(volume_title, b_title_handle) ;				// --- 音量調整
			}
			break ;

		// --- キャラセレクトBGM
		case s_CharaSelect :
			if ( volume_charaselect > 0 ) {
				volume_charaselect-- ;
				ChangeVolumeSoundMem(volume_charaselect, b_charaslc_handle) ;				// --- 音量調整
			}
			if ( changesoundflg == 1 ) {
				if ( volume_gameplay > 0 ) {
					volume_gameplay-- ;
					ChangeVolumeSoundMem(volume_gameplay, b_gplay_handle) ;				// --- 音量調整
				}
			}
			break ;

		// --- ゲームBGM
		case s_GamePlay :
			if ( volume_gameplay2 > 0 ) {
				fade_out++ ;
				if ( fade_out > 1 ) {
					fade_out = 0 ;
					volume_gameplay2-- ;
				}
			}
			ChangeVolumeSoundMem(volume_gameplay2, b_g2play_handle) ;				// --- 音量調整
			break ;
	}

}




