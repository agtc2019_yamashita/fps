#include <windows.h>
#include "Common.h"


/* ----------------------------------------------------- */
/*					　コンストラクタ					 */
/* ----------------------------------------------------- */
Stage::Stage() {

}

/* ----------------------------------------------------- */
/*					　デストラクタ						 */
/* ----------------------------------------------------- */
Stage::~Stage() {

}

/* ----------------------------------------------------- */
/*			ステージ初期セット (メンバ関数ではない)		 */
/* ----------------------------------------------------- */
int StageInit() {

	// --- ブロックモデルの読み込み
	for ( int i = 0 ; i < BLOCKMAX ; i++ ) {
		stage[i].m_block = MV1LoadModel( "..\\Models\\woodbox.mv1")	;
		stage[i].m_shadow= MV1LoadModel( "..\\Models\\boxshadow.mv1")	;
		stage[i].drawflg = TRUE ;
	}

	// 椅子モデル読み込み
	for ( int i = 0 ; i < CHAIRMAX ; i++ ){
		stage[i].m_chair = MV1LoadModel( "..\\Models\\chair.mv1")	;
		stage[i].m_cshadow= MV1LoadModel( "..\\Models\\chairshadow.mv1")	;	
	}

	// --- ブロック座標
	stage[0].p_block = VGet( 0.0f , -20.0f , 0.0f ) ;			// 中心(シーン切替後も使用)
	
	stage[1].p_block = VGet( -500.0f , -20.0f , 675.0f ) ;
	stage[2].p_block = VGet( -500.0f , -20.0f , -675.0f ) ;
	stage[3].p_block = VGet( 500.0f ,  -20.0f , 675.0f ) ;
	stage[4].p_block = VGet( 500.0f , -20.0f , -675.0f ) ;

	stage[5].p_block = VGet( 1000.0f , -20.0f , 0.0f ) ;
	stage[6].p_block = VGet( -1000.0f , -20.0f , 0.0f ) ;

//	stage[5].p_block = VGet( 660.0f , -20.0f , 675.0f ) ;
//	stage[6].p_block = VGet( 660.0f , -20.0f , -675.0f ) ;

	stage[7].p_block = VGet( 0.0f , -20.0f , 1275.0f ) ;
	stage[8].p_block = VGet( 0.0f , -20.0f , -1275.0f ) ;

	// --- 椅子座標
	stage[0].p_chair = VGet( -866.0f , -20.0f , -1425.0f ) ;
	stage[1].p_chair = VGet( 866.0f , -20.0f , 1425.0f ) ;
	stage[2].p_chair = VGet( -383.0f , -20.0f , 775.0f ) ;
	stage[3].p_chair = VGet( 383.0f , -20.0f , -775.0f ) ;
	stage[4].p_chair = VGet( -866.0f , -20.0f , 0.0f ) ;
	stage[5].p_chair = VGet( 866.0f , -20.0f , 0.0f ) ;

	// --- ブロック座標とモデルハンドル結びつけ
	for ( int i = 0 ; i < BLOCKMAX ; i++ ) {
		MV1SetScale( stage[i].m_block , VGet( 0.8f , 0.8f , 0.8f) );		// モデルの座標を設定
		MV1SetScale( stage[i].m_shadow , VGet( 1.1312f , 1.0f , 1.1312f) );		// モデルの座標を設定
		MV1SetPosition( stage[i].m_block , stage[i].p_block );				// モデルの座標を設定
		MV1SetPosition( stage[i].m_shadow , VGet( stage[i].p_block.x+78.5,  -99.0f , stage[i].p_block.z+78.5) );				// モデルの座標を設定
		MV1SetRotationXYZ(stage[i].m_shadow,VGet( 0.0f ,-0.785f , 0.0f) ) ;   //影回転
	}

	for ( int i = 0 ; i < CHAIRMAX ; i++ ) {	
		MV1SetScale( stage[i].m_chair , VGet( 0.8f , 1.0f , 0.8f) );		// モデルのサイズ
		MV1SetScale( stage[i].m_cshadow , VGet( 1.1312f , 1.0f , 1.1312f) );	// モデルのサイズ
		MV1SetPosition( stage[i].m_chair , stage[i].p_chair );				// モデルの座標を設定
		MV1SetPosition( stage[i].m_cshadow , VGet( stage[i].p_chair.x+78.5,  -99.0f , stage[i].p_chair.z+78.5) );				// モデルの座標を設定
		if ( i < 4 ){
			MV1SetRotationXYZ(stage[i].m_chair,VGet( 0.0f ,1.57f , 0.0f) ) ;		//影回転
			MV1SetRotationXYZ(stage[i].m_cshadow,VGet( 0.0f ,-0.785f , 0.0f) ) ;	//影回転
		}else {
			MV1SetRotationXYZ(stage[4].m_chair,VGet( 0.0f ,0.0f , 0.0f) ) ;			//影回転
			MV1SetRotationXYZ(stage[4].m_cshadow,VGet( 0.0f ,-0.785f , 0.0f) ) ;	//影回転		
			MV1SetRotationXYZ(stage[5].m_chair,VGet( 0.0f ,3.14f , 0.0f) ) ;		//影回転
			MV1SetRotationXYZ(stage[5].m_cshadow,VGet( 0.0f ,-0.785f , 0.0f) ) ;	//影回転		
		}
	}

	return 0 ;
}




