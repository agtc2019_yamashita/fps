#include "Common.h"

Transition::Transition() {
	SceneChangeInit() ;
}

Transition::~Transition() {

}

void Transition::SceneChangeInit() {
	bo_size = 4.0f ;				// トランジションサイズ
	bo_rotation = 0.0f ;			// トランジション回転率
	bo_finflg = FALSE ;				// シーンチェンジ終了 FALSE : 未 , TRUE : 終

}

void Transition::SceneChange( int spd ) {

	switch( spd ) { 

		case FAST :
			bo_size -= 0.04f ;
			bo_rotation -= 0.1f ;
			if ( bo_size < 1.4f ) 
				DrawRotaGraph( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 , 1.0 , 0.0 ,  gh_charatransbk, true ) ;
			if ( bo_size < 0.7f ) 
				DrawRotaGraph( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 , 0.5 , 0.0 ,  gh_charatransbk, true ) ;
			DrawRotaGraph( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 , (double)bo_size , bo_rotation ,  gh_charatrans, true ) ;
			break ;

		case SLOW :
			bo_size -= 0.02f ;
			bo_rotation -= 0.1f ;
			if ( bo_size < 1.4f ) 
				DrawRotaGraph( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 , 1.0 , 0.0 ,  gh_charatransbk, true ) ;
			if ( bo_size < 0.7f ) 
				DrawRotaGraph( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 , 0.5 , 0.0 ,  gh_charatransbk, true ) ;
			DrawRotaGraph( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 , (double)bo_size , bo_rotation ,  gh_charatrans, true ) ;
			break ;
	}

	if ( bo_size < 0.25 )
		bo_finflg = TRUE ;


}




