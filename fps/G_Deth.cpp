/* -----------------------------------------------------------------------------------------
|	
|   プレイヤー　やられ時処理
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include "Math.h"

int distime = 10 ;

void G_Deth()
{
	// カメラの設定-----------------------------------------------------------
	SetCameraNearFar( 5.0f,  150000.0f ) ;									// 描画距離を設定
	SetDrawArea( 0 , 0 , WINDOWSIZE_W , WINDOWSIZE_H ) ;
	if ( sc_fin_fun == FALSE ){
		SetCameraPositionAndTarget_UpVecY( VAdd( dethpos , VGet(sinf(p_cam[dethplayer].HRotate) * 85*PLYSPD  , 300.0f , cosf(p_cam[dethplayer].HRotate) * 85*PLYSPD)), dethpos ) ;
	}else{
		if ( dethpos.z >= 0 )
			SetCameraPositionAndTarget_UpVecY( VAdd( dethpos , VGet( 0.0f , 300.0f , -800.0f )) , dethpos) ;
		else
			SetCameraPositionAndTarget_UpVecY( VAdd( dethpos , VGet( 0.0f , 300.0f , 800.0f )) , dethpos) ;

	}
	SetCameraScreenCenter( WINDOWSIZE_W/2, WINDOWSIZE_H/2 );            // 画面分割設定

	MV1SetRotationXYZ(p1mark,VGet(0.0f, 4.71f , 0.0f)) ; 
	MV1SetRotationXYZ(p2mark,VGet(0.0f, 4.71f , 0.0f)) ; //1p2pポインタ回転
	
	// --- 風船移動
	for ( int i = 0 ; i < 10 ; i++ ){
		balloon[i].Move() ;
	}
	// 風船アニメーション
	for ( int i = 0 ; i < 10 ; i++ ){
		balloon[i].Play_Anim( ) ;
		MV1SetAttachAnimTime( balloon[i].mh_balloon,balloon[i].attachidx,balloon[i].playtime) ;
	}

	// やられアニメーション&アクション----------------------------------------
	for ( int i = 0 ; i < 2 ; i++ ){
		if (dethplayer == i ){
			g_player[i].AnimMode_Deth() ;
			distime-- ;
			if ( distime <= 0 ){
				g_player[i].pos.y = -300 ; 
				distime = 10 ;
			}
			g_player[i].Move() ;

		}
		g_player[i].Play_Anim() ;
		MV1SetAttachAnimTime(g_player[i].model_hnd,g_player[i].attachidx,g_player[i].playtime) ;
	}

	//爆発の演出
	for ( int y = 0  ; y < 2 ; y++ ){                     
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].exploflg == TRUE ){
				p_bullet[y][x].Boomhandl() ;
			}
		}
	}

	// 弾のヒットチェック
	for ( int y = 0 ,  i = 1 ; y < 2 ; y++ , i-- ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE ){
				p_bullet[y][x].BulletHitCheck( g_player[i].pos ) ;
			}
		}
	}
	// 弾アクション---------------------------------
	for ( int y = 0 ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE )
				p_bullet[y][x].Move() ;
		}
	}

	// やられアニメーション終わりチェック---------------------------------
	if ( ((dethplayer == 0) && (g_player[PLAYER1].playtime >= g_player[PLAYER1].anim_totaltime))
		|| ((dethplayer == 1) && (g_player[PLAYER2].playtime >= g_player[PLAYER2].anim_totaltime) ) ){

		game_mode = eG_Respawn ;
	}

	// 描画
	DrawLoop() ;
	if ( dethplayer == 0 ){
		printfDx( "aaa" ) ;
		DrawRotaGraph( (WINDOWSIZE_W / 2)  , (WINDOWSIZE_H/4) * 3 , 1.4  , 0 , gh_1P_deth , true , 0, 0 ) ;
	}else{	
		DrawRotaGraph( (WINDOWSIZE_W / 2)  , (WINDOWSIZE_H/4) * 3 , 1.4  , 0 , gh_2P_deth , true , 0, 0 ) ;
	}

	// リスポーン時の初期セット
	if ( game_mode == eG_Respawn ){
		g_player[PLAYER1].pRespawn( P1_INIT_POS ) ;
		g_player[PLAYER2].pRespawn( P2_INIT_POS ) ;
		p_cam[PLAYER1].InitSet( PLAYER1 , P1_INIT_ROT ) ;		// p1カメラ
		p_cam[PLAYER2].InitSet( PLAYER2 , P2_INIT_ROT ) ;		// p2カメラ

		// 弾のリセット
		for ( int j = 0 ; j < BULLETMAX+add_bullet ; j++ ){
			p_bullet[PLAYER1][j].usingflg = FALSE ;
			p_bullet[PLAYER2][j].usingflg = FALSE ;
		}
	}
}

void DethInit()
{

	game_mode = eG_Deth ;		// モード切替
	// やられプレイヤーの設定-------------------------------------------------
	if ( dethplayer == 0 ){
		dethpos = g_player[PLAYER1].pos ;
	}else if ( dethplayer == 1 ){
		dethpos = g_player[PLAYER2].pos ;
	}
}
