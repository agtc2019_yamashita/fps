class Bullet
{
	public :
		Bullet() ;	// コンストラクタ
		~Bullet() ; // デストラクタ

		void InitSet( VECTOR pos , VECTOR spd , float hrotate ) ;
		bool BulletHitCheck( VECTOR player_pos ) ; // Bulletのヒットチェック
		void Move( ) ;
		void Boomset( ) ;                       //爆発のセット	
		void Boomhandl( ) ;						//爆発の演出

		void EffectLoad() ;						// --- エフェクトモデル読み込み

		// --- 銃弾使用
		VECTOR	spd				; // スピード
		VECTOR	pos				; // ポジション
		int		mhnd			; // モデルハンドル
		int		shadow			; // 弾丸影のハンドル
		int		booms			; // 爆発のハンドル
		int		booms2			; // 爆発２の	
		int		refcnt			; // 反射カウント
		bool	refflg			; // 反射フラグ
		bool	usingflg		; // 使用フラグ	爆発エフェクト終了でリロード
		bool	exploflg		; // 爆発フラグ
		bool	bullet_hitflag	; // 弾ヒットフラグ
		bool	ply_hitflg		; // プレイヤーにヒットしたかチェック
		VECTOR  deth_plypos     ; // エフェクト用デスプレイヤーポジション		
		float   burst			; // 黄色爆発の拡大倍率
		float   burst2			; // オレンジ爆発の拡大倍率
		int     plusminusflag   ;	// 1　プラス　-1　マイナス
		int     plusminusflag2  ;	// 1　プラス　-1　マイナス

		// --- エフェクト使用
		VECTOR	ef_pos			; // エフェクトポジション
		VECTOR	ef_spd			; // エフェクトスピード
		int		ef_mhnd[9]		; // エフェクトモデルハンドル
		int		ef_mcnt			; // エフェクトモデルカウント
		int		ef_timecnt		; // 待ち 
		float	ef_rotate		; // エフェクト回転率
		float	ef_minusspd		; // 減速
		bool	ef_usingflg		; // 使用フラグ


} ;