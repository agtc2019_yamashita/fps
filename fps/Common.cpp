#include "Common.h"      // --- ゲーム共通のヘッダーファイル
/* -----------------------------------------------------------------------------------------
|
|
|       データ宣言
|
|
+ --------------------------------------------------------------------------------------- */
/* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
|       変数、構造体データ
+ -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
Player		g_player[2] ;
Player		t_player[3] ;
FpsCamera   p_cam[2] ;
Bullet      p_bullet[2][BULLETMAX+1] ;
Stage		stage[BLOCKMAX] ;
Crown		crown ;
Time		limtime ;
MiniMap		minimap ;
Sound		sound ;
Transition	trans ;					// シーンチェンジオブジェクト
Drone		drone ;					// ドローンオブジェクト
Balloon		balloon[10] ;			// バルーンオブジェクト

int			game_mode		;		// ゲームモード管理用
VECTOR      dethpos			;		// プレイヤー死亡時座標
int			dethplayer		;		// 死亡プレイヤー 1:1p 2:2p
VECTOR		balloonpos[10]  ;		// バルーンオブジェクト

float       skyroll = 0.0f ;       // 空を回転させる

// モデルハンドル------------------------------------------------
int			ground			;		// 地面				モデル
int			ground_castle	;		// 地面				モデル

int			kabe			;		// 壁				モデル
int			sky				;		// 空				モデル
int			sky_castle		;		// 地面				モデル
int			player1			;		// プレーヤー		モデル
int			player2			;		// プレーヤー		モデル
int			bullet[2]		;		// 弾丸				モデル
int			mh_players[2][3];		// プレイヤーのモデルハンドル
int         land			;		// 土台				モデル
int			cursor[2]		;		// 矢印				モデル
int         boom            ;		// 爆発             モデル
int         boom2           ;		// 爆発             モデル
int         p1mark          ;       // player1ポインタ  モデル
int         p2mark          ;       // player2ポインタ  モデル
int			castle			;		// 城				モデル
int			door_1			;		// ドア				モデル
int			door_2			;		// ドア				モデル
int			m_banner_red	;		// 赤バナー			モデルハンド
int			m_banner_blue	;		// 青バナー			モデルハンド
VECTOR		rbanner_pos		;		// 赤バナー			ポジション
VECTOR		bbanner_pos		;		// 青バナー			ポジション


// 画像ハンドル--------------------------------------------------
int			gh_usebullet[2]	;		// 赤弾						画像ハンドル
int			gh_nonbullet	;		// 白弾						画像ハンドル
int			gh_stage		;		// ステージ					画像ハンドル
int			gh_minicube		;		// ミニマップ木箱			画像ハンドル
int			gh_minichair	;		// ミニマップ椅子			画像ハンドル
int			gh_miniplayer[2];		// ミニマッププレイヤー		画像ハンドル
int			gh_numberback	;		// 数字背景					画像ハンドル
int			gh_number[3]	;		// 数字						画像ハンドル
int			gh_score		;		// SCORE					画像ハンドル
int			gh_go			;		// GO						画像ハンドル
int			gh_finish		;		// 終了						画像ハンドル
int			gh_lrbutton		;		// 操作説明LRボタン			画像ハンドル
int			gh_atkbutton	;		// 操作説明攻撃ボタン		画像ハンドル
int			gh_cmrbutton	;		// 操作説明カメラボタン		画像ハンドル
int			gh_lratk		;		// 操作説明カメラボタン		画像ハンドル
int			gh_oukanget		;		// 操作説明王冠取得ボタン	画像ハンドル
int			gh_oukangetaf	;		// 操作説明王冠取得ボタン	画像ハンドル
int			gh_oukandes		;		// 操作説明王冠ロスボタン	画像ハンドル
int         shadow1         ;		// 弾丸の影					画像ハンドル
int			gh_explosion	;		// 爆発						画像ハンドル
int			gh_waku			;		// 枠線						画像ハンドル
int			gh_charaselect_waku ;	// キャラセレ時枠		画像ハンドル
int			gh_kill			;		// キル						画像ハンドル
int			gh_ready		;		// 準備完了					画像ハンドル
int			gh_blackball	;
int			gh_charatrans	;		// キャラトランジション		画像ハンドル
int			gh_charatransbk	;		// キャラトランジション背景	画像ハンドル
int			gh_option_A		;		// 操作方法○射撃
int			gh_option_B		;		// 操作方法LR射撃
int			gh_charasel_bo	;		// キャラ選択ブラックアウト
int			gh_option_bo	;		// 操作方法ブラックアウト
int			gh_start		;		// スタート用	画像ハンドル
int			gh_number321[3]	;		// 1 2 3　が入る			画像ハンドル
int			gh_V			;		// V	画像ハンドル
int			gh_S			;		// S	画像ハンドル
int			gh_sc_font		;		// シーンチェンジ時文字		画像ハンドル
int			gh_sc_fontbk	;		// シーンチェンジ時文字背景	画像ハンドル
int			gh_brackoutsc	;		// 暗転処理					画像ハンドル
int			gh_title[7]		;		// タイトル文字				画像ハンドル
int			gh_1P_deth		;		// 1Pやられ表示				画像ハンドル
int			gh_2P_deth		;		// 2Pやられ表示				画像ハンドル
int			gh_subtitle		;		// サブタイトル文字			画像ハンドル
int			gh_startop		;		// スタートボタン			画像ハンドル


int			goalpha = 0		;		// 画像透過変数(GO) 
int			goadd = 6		;		// 画像透過変数  
double		go_size = 5.0f	;		// GO画像サイズ
double		go_rot = 0		;		// GO画像回転
int			scf_alpha		;		// スクリーンチェンジ透過
int			scbk_alpha		;		// スクリーンチェンジ背景透過
float		scf_posx		;		// スクリーンチェンジ文字座標
bool		sc_finish		;		// スクリーンチェンジ文字終了
bool		sc_fin_fun		;		// スクリーンチェンジ関数終了チェックフラグ
int			usebulletcnt	;		// 使われている弾カウント
int			infotime		;		// お知らせ描画時間
int			rd				;		// ランダムくん

int			Pad[2]			;		// ジョイパッド入力 (要素数 0 : 1P , 1 : 2P)

int			add_bullet = 0	;		// 30秒処理で増やす弾数

