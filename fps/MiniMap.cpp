/* -----------------------------------------------------------------------------------------
|	
|   ミニマップ 処理
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"

/* ----------------------------------------------------- */
/*					　コンストラクタ					 */
/* ----------------------------------------------------- */
MiniMap::MiniMap() {
	// --- ミニマップ初期セット
	MiniMapInit() ;
}

/* ----------------------------------------------------- */
/*					　デストラクタ						 */
/* ----------------------------------------------------- */
MiniMap::~MiniMap() {

}

/* ----------------------------------------------------- */
/*					ミニマップ初期セット				 */
/* ----------------------------------------------------- */
void MiniMap::MiniMapInit() {

	mapcenter_x = WINDOWSIZE_W / 2 ;			// ミニマップの中心X座標
	mapcenter_y = MINIMAPSIZE_H / 2 + 10 ;		// ミニマップの中心Y座標

}

void MiniMap::DrawMiniMap() {

	// --- ステージ描画
	DrawRotaGraph( mapcenter_x  , mapcenter_y , 1.1, 0.0 , gh_stage, true ) ;

	// --- 木箱 & 椅子表示
	if ( sc_fin_fun == FALSE ){
		for ( int i = 0 ; i < 9 ; i++ ) {
			// --- ステージ描画
			DrawRotaGraph( mapcenter_x + stage[i].p_block.z / 10.0f , 
							mapcenter_y + stage[i].p_block.x / 10.0f , 0.08 , 0 , gh_minicube, true ) ;
		}
	}else{	
		for ( int i = 0 ; i < 3 ; i++ ) {
			DrawRotaGraph( mapcenter_x + stage[i].p_block.z / 10.0f , 
							mapcenter_y + stage[i].p_block.x / 10.0f , 0.08 , 0 , gh_minicube, true ) ;
		}
		for ( int i = 0 ; i < 6 ; i++ ){
			if ( i < 4 ){
				DrawRotaGraph( mapcenter_x + stage[i].p_chair.z / 10.0f , 
								mapcenter_y + stage[i].p_chair.x / 10.0f , 0.08 , 1.57 , gh_minichair, true ) ;
			}else{
				DrawRotaGraph( mapcenter_x + stage[i].p_chair.z / 10.0f , 
								mapcenter_y + stage[i].p_chair.x / 10.0f , 0.08 , 0.0 , gh_minichair, true ) ;
			}
		}
	}

	// --- 弾表示
	for ( int y = 0 ; y < 2 ; y++ ) {

		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ) {
			if ( p_bullet[y][x].usingflg == TRUE ) {
				// --- ステージ描画
				DrawRotaGraph( mapcenter_x + p_bullet[y][x].pos.z / 10.0f , 
								mapcenter_y + p_bullet[y][x].pos.x / 10.0f , 0.1 , 0 , gh_usebullet[y], true ) ;
			}
		}
	}

	// --- プレイヤー表示
	for ( int i = 0 ; i < 2 ; i++ ) {

		player[i].x = g_player[i].pos.z / 10.0f ;
		player[i].y = g_player[i].pos.x / 10.0f + 5 ;

		// --- プレイヤー描画
		DrawRotaGraph( mapcenter_x + player[i].x , 
						mapcenter_y + player[i].y , 0.1 , 0 , gh_miniplayer[i], true ) ;

	}
}
