/* -----------------------------------------------------------------------------------------
|	
|   キャラ選択時
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include "Math.h"

// このファイル(cpp)内でのグローバル変数
int		roteflg[2] ;					// 回転しているかどうか
int		selectchara[2] = { 0 , 0 } ;	// キャラ設定 0;magic 1:neet 2:angel
float	camera_angle_select[2] ;		// セレクト画面時のカメラ角度
float   cursor_spd[2] = { 1.0f , 1.0f } ;
float   cursor_rot[2] = { 0.0f , 0.0f} ;
bool	selectflg[2] = {FALSE ,FALSE} ;	// 決定確認(操作方法)
VECTOR  select_camera[2] ;				// セレクト画面でのカメラ
VECTOR	select_add[2] ;					// セレクト画面でのカメラ移動量			
VECTOR	cursor_pos[2] = { VGet( 0 , 370.0f , 0 ) , VGet( 0 , 370.0f , 0 )} ;	// カーソルのポジション
bool    cursor_drawflg = FALSE ;
int		timecnt = 0	;					// キャラ設定時のタメ
bool	chara_flg[2] = {FALSE ,FALSE} ;	// TRUEで操作方法設定へ
int		now_op_mode[2] = { 0 , 0 } ;				// 操作方法 0:○射撃	1:LR射撃					
bool	pushflg[2] = { FALSE , FALSE } ;		//　決定長押し無効
bool	cpushflg[2] = { FALSE , FALSE } ;		// キャンセル長押し無効
bool	soundflg = FALSE ;						// サウンド処理を一回だけにするため
bool	rpush_flg[2] = { FALSE , FALSE } ;		// ボタン同時押し管理
bool	cursorflg[2] = { FALSE , FALSE } ;		// ボタン同時押し(操作選択)
void CharaSelect()
{

	SetCameraNearFar( 5.0f,  150000.0f ) ;								// 描画距離を設定

	sound.VolumeAdd( ) ;

	//カメラ注視点座標
	select_add[0] = VGet( BANNER_POSX , 50.0f , BANNER_POSZ ) ;					
	select_add[1] = VGet( BANNER_POSX , 50.0f , -BANNER_POSZ ) ;					
	MV1SetPosition( m_banner_red , VGet(BANNER_POSX, 200.0f, BANNER_POSZ) )  ; // バナー座標 
	MV1SetPosition( m_banner_blue , VGet(BANNER_POSX, 200.0f, -BANNER_POSZ) )    ; // バナー座標 

	//角度を0にリセット
	for ( int i = 0 ; i < 2 ; i++ ) {
		if ( camera_angle_select[i] > 6.28f ){                            
			camera_angle_select[i] = 0.0f ;
		}
		if (camera_angle_select[i] < -6.28f )
		{
			camera_angle_select[i] = 0.0f ;
		}
	
		//キャラクターに向いてる角度になったらカメラが止まる
		if( ( camera_angle_select[i] < 2.15f && camera_angle_select[i] > 2.05f ) || ( camera_angle_select[i] < 0.05f && camera_angle_select[i] > -0.05f ) 
			|| ( camera_angle_select[i] < 4.25 && camera_angle_select[i] > 4.15) ||  ( camera_angle_select[i] > -2.15f && camera_angle_select[i] < -2.05f ) 
			|| ( camera_angle_select[i] > -4.25 && camera_angle_select[i] < -4.15) && (chara_flg == FALSE) ) {

			// 矢印描画ON 
			cursor_drawflg = TRUE ;

			// キャラセレクト決定　解除-------------------------------
			if ((Pad[i] & PAD_INPUT_B || CheckHitKey(KEY_INPUT_SPACE) == 1) && (chara_flg[i] == FALSE) ){ // コントローラー○
				chara_flg[i] = TRUE ;	// 準備完了
				pushflg[i] = TRUE ;
				sound.PlaySoundSE( s_Commit ) ;
			}else if ( (((Pad[i] & PAD_INPUT_B)== 0) && CheckHitKey(KEY_INPUT_SPACE) == 0) && (chara_flg[i] == TRUE) ){
				pushflg[i] = FALSE ;
			}

			//左右キーを押したらカメラが動き始める
			if( ((Pad[i] & PAD_INPUT_LEFT) || (CheckHitKey( KEY_INPUT_A ) == 1)) && (chara_flg[i] == FALSE) )
			{
				cursor_drawflg = FALSE ;		// 矢印描画off
				roteflg[i] = 1 ;
				camera_angle_select[i] += 0.20f ;
				selectchara[i]-- ;
				if ( selectchara[i] <= -1 )
					selectchara[i] = 2 ;
				sound.PlaySoundSE( s_Charaslc ) ;
			}
			if( ((Pad[i] & PAD_INPUT_RIGHT) || (CheckHitKey( KEY_INPUT_D ) == 1)) && (chara_flg[i] == FALSE) )
			{
				cursor_drawflg = FALSE ;		// 矢印描画off
				roteflg[i] = -1 ;
				camera_angle_select[i] -= 0.20f ;
				selectchara[i]++ ;
				if ( selectchara[i] >= 3 )
					selectchara[i] = 0 ;
				sound.PlaySoundSE( s_Charaslc ) ;
			}
		}
		else {
			// 矢印描画OFF
			cursor_drawflg = FALSE ;
			//キャラクターに向いていない角度じゃなかったら回る
			if(roteflg[i] == 1)//左右フラグ
			{
				camera_angle_select[i] += 0.05f ;
			}
			if(roteflg[i] == -1)
			{
				camera_angle_select[i] -= 0.05f ;
			}

		}

		// カメラ角度修正-----------------------------------------------------------
		if ( camera_angle_select[i] < 2.15f && camera_angle_select[i] > 2.05f )
			camera_angle_select[i] = 2.10f ;
		if ( camera_angle_select[i] < 0.05f && camera_angle_select[i] > -0.05f )
			camera_angle_select[i] = 0.0f ;
		if ( camera_angle_select[i] < 4.25 && camera_angle_select[i] > 4.15)
			camera_angle_select[i] = 4.20f ;
		if ( camera_angle_select[i] > -2.15f && camera_angle_select[i] < -2.05f )
			camera_angle_select[i] = 4.20f ;
		if( camera_angle_select[i] > -4.25 && camera_angle_select[i] < -4.15 )
			camera_angle_select[i] = 2.10f ;
	
		select_camera[i].x = 1100 * sin(camera_angle_select[i]) + select_add[i].x ;
		select_camera[i].y = 300.0f + select_add[i].y ;           //カメラ位置をセット
		select_camera[i].z = 1100 * cos(camera_angle_select[i]) + select_add[i].z ;
		
		// 矢印のポジション設定-----------------------------------
		if ( cursor_pos[i].y <= 360.0f || cursor_pos[i].y >= 400.0f  )
			cursor_spd[i] *= -1 ;		

		cursor_rot[i] += 0.314f / 2;		//カーソルの回転
		cursor_pos[i].x = 620 * sin(camera_angle_select[i]) + select_add[i].x ;
		cursor_pos[i].z = 620 * cos(camera_angle_select[i]) + select_add[i].z ;
		cursor_pos[i].y += cursor_spd[i] ;
		MV1SetRotationXYZ( cursor[i] ,VGet(0.0f,cursor_rot[i],0.0f)) ; 	// モデルの回転

		// 矢印描画---------------------
		MV1SetPosition( cursor[i] ,cursor_pos[i] ) ;

		SetCameraPositionAndTargetAndUpVec( select_camera[i], VGet(0.0f , 0.0f , 0.0f), VGet(0.0f , 1.0f , 0.0f) ) ;

		MV1SetPosition( mh_players[i][Magic]  , 
					VGet(0.0f + select_add[i].x , select_add[i].y+50 , 500.0f + select_add[i].z));							// キャラクターの座標セット
		MV1SetPosition( mh_players[i][Neet]  ,
			VGet(-sqrt(3.0f)*(500.0f/2)  + select_add[i].x , select_add[i].y+50 , -sqrt(500.0f*500.0f - pow((500/2),2.0)*3) + select_add[i].z));							// モデルの座標を設定
		MV1SetPosition( mh_players[i][Angel] , 
			VGet( sqrt(3.0f)*(500.0f/2)  + select_add[i].x , select_add[i].y+50 , -sqrt(500.0f*500.0f - pow((500/2),2.0)*3) + select_add[i].z));							// モデルの座標を設定

		MV1SetRotationXYZ( mh_players[i][Magic],VGet(0.0f,3.14f,0.0f)) ; 		// モデルの回転
		MV1SetRotationXYZ( mh_players[i][Neet] ,VGet(0.0f,3.14f/3,0.0f)) ; 		// モデルの回転
		MV1SetRotationXYZ( mh_players[i][Angel],VGet(0.0f,-3.14f/3,0.0f)) ; 	// モデルの回転

		SetCameraPositionAndTarget_UpVecY( select_camera[i], VGet(select_add[i].x , -100 + select_add[i].y , select_add[i].z) ) ;
		SetDrawArea( (WINDOWSIZE_W/2) *i, 0, WINDOWSIZE_W/2*(i+1), WINDOWSIZE_H );	               // 画面分割設定(描画範囲)
		SetCameraScreenCenter( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i) , WINDOWSIZE_H/2 );            // 画面分割設定

		drone.Move() ;	// ドローン

		// 風船アニメーション
		for ( int j = 0 ; j < 10 ; j++ ){
			// --- 風船移動
			balloon[j].Move() ;
			balloon[j].Play_Anim( ) ;
			MV1SetAttachAnimTime( balloon[j].mh_balloon,balloon[j].attachidx,balloon[j].playtime) ;
		}

		// 各モデルのMV1DrawModelを実行
		DrawLoop() ;

		// キャラセレクト時　モデル描画
		for ( int x = 0 ; x < 3 ; x++ )			
			MV1DrawModel( mh_players[i][x] ) ;
		

		if ( i == 0 ){
			MV1DrawModel( m_banner_red ) ;
		}else{
			MV1DrawModel( m_banner_blue ) ;
		}
		if ( cursor_drawflg == TRUE ){	// カーソル描画
			MV1DrawModel( cursor[i]) ;
		}

		// 操作方法決定処理----------------------------------------
		if ( chara_flg[i] == TRUE ){

			// キャラ選択解除 
			if( (Pad[i] & PAD_INPUT_C || (CheckHitKey( KEY_INPUT_B ) == 1)) && (selectflg[i] == FALSE) && ( cpushflg[i] == FALSE ) && ( pushflg[i] == FALSE ) ){
				chara_flg[i] = FALSE ;
				sound.PlaySoundSE( s_Cancel ) ;
			}

			// 操作選択---------------------------------------------
			if( (Pad[i] & PAD_INPUT_UP || (CheckHitKey( KEY_INPUT_UP ) == 1)) && (selectflg[i] == FALSE) && ( now_op_mode[i] == 1) && (cursorflg[i] == FALSE) ){
				now_op_mode[i] = 0 ;	// ○射撃へ
				cursorflg[i] = TRUE ;
				sound.PlaySoundSE( s_Cursor ) ;
			}else if ( ((Pad[i] & PAD_INPUT_UP) == 0) && (CheckHitKey( KEY_INPUT_UP ) == 0) ) {
				cursorflg[i] = FALSE ;
			}
			if( (Pad[i] & PAD_INPUT_DOWN || (CheckHitKey( KEY_INPUT_DOWN ) == 1)) && (selectflg[i] == FALSE) && ( now_op_mode[i] == 0) && (cursorflg[i] == FALSE) ){
				now_op_mode[i] = 1 ;	// LR射撃へ
				cursorflg[i] = TRUE ;
				sound.PlaySoundSE( s_Cursor ) ;
			}else if ( ((Pad[i] & PAD_INPUT_DOWN) == 0) && (CheckHitKey( KEY_INPUT_DOWN ) == 0) ) {
				cursorflg[i] = FALSE ;
			}

			// 操作決定　解除------------------------------------------ 
			if( (Pad[i] & PAD_INPUT_B || (CheckHitKey( KEY_INPUT_SPACE ) == 1)) && (selectflg[i] == FALSE) && ( pushflg[i] == FALSE) ){
				selectflg[i] = TRUE ;
				g_player[i].operation_mode = now_op_mode[i] ;
				p_cam[i].operation_mode = now_op_mode[i] ;
				rpush_flg[i] = TRUE ;
				sound.PlaySoundSE( s_Ready ) ;
			}
			if (((Pad[i] & PAD_INPUT_B) == 0) && (CheckHitKey( KEY_INPUT_SPACE ) == 0)){
				rpush_flg[i] = FALSE ;
			}
			if( (Pad[i] & PAD_INPUT_C || (CheckHitKey( KEY_INPUT_B ) == 1))  && (selectflg[i] == TRUE) && ( rpush_flg[i] == FALSE) && ( timecnt < 60) ){
				selectflg[i] = FALSE ;
				timecnt = 0	;			// タメ時間のリセット
				trans.SceneChangeInit() ;
				cpushflg[i] = TRUE ;
				sound.PlaySoundSE( s_Cancel ) ;
			}else if  ( ((Pad[i] & PAD_INPUT_C) == 0) && (CheckHitKey( KEY_INPUT_B ) == 0) && cpushflg[i] == TRUE ){
				cpushflg[i] = FALSE ; 
			}

			// コントローラー描画

			DrawRotaGraph( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i)  , WINDOWSIZE_H/2   , 1  , 0 , gh_charasel_bo , true , 0, 0 ) ;
			DrawRotaGraph( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i)  , WINDOWSIZE_H/4   , 1  , 0 , gh_option_A , FALSE , 0, 0 ) ;
			DrawRotaGraph( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i)  , WINDOWSIZE_H/4*3 , 1  , 0 , gh_option_B , FALSE , 0, 0 ) ;
			DrawRotaGraph( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i)  , (WINDOWSIZE_H/4)+(WINDOWSIZE_H/4*2*(1-now_op_mode[i])) , 1  , 0 , gh_option_bo , true , 0, 0 ) ;	
		}
		
		// 準備完了描画--------------------------------------
		if ( selectflg[i] == TRUE && chara_flg[i] == TRUE )		
			DrawRotaGraph( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i)  , WINDOWSIZE_H*3/4 ,0.8 , 0 ,  gh_ready, true ) ;
	}

	// 枠線描画-----------
	SetDrawArea( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H );		// 画面分割の設定(描画範囲)
	DrawGraph( 0  , 0 , gh_charaselect_waku, true ) ;

	// 次のゲームモードへ移行＆初期セット　両プレイヤーが準備完了時のタメの処理を追加予定
	if ((selectflg[0] == TRUE) && (selectflg[1] == TRUE) ){

		timecnt++ ;
		if ( timecnt > 60 ) {
			SetDrawArea( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H );		// 画面分割の設定(描画範囲)
			trans.SceneChange( SLOW ) ;								// シーンチェンジ(遅い)
			sound.now_playsound = s_GamePlay ;
			sound.PlaySoundBGM() ;

		}
		if ( trans.bo_finflg == TRUE ){

			// 黒丸描画---------------------------------------------

			// 一秒立ったら次のモードへ
			Pad[PLAYER1] = 0 ;
			Pad[PLAYER2] = 0 ;
			timecnt = 0 ;			// 準備完了時のカウント初期化

			game_mode = eCharaShow;     //ゲームモード転換
			
			// バナー位置設定
			MV1SetPosition( m_banner_red , VGet(BANNER_POSX+1500, 200.0f,  BANNER_POSZ+700) )  ; // バナー座標 
			MV1SetPosition( m_banner_blue , VGet(BANNER_POSX+1500, 200.0f, BANNER_POSZ-700) )    ; // バナー座標 
			
			// 初期セット------------------------------------------------------------------------------
			g_player[0].InitSet( PLAYER1 , P1_INIT_POS , mh_players[PLAYER1][selectchara[PLAYER1]] , selectchara[PLAYER1] ) ;	// プレイヤー(モデル)の座標
			g_player[1].InitSet( PLAYER2 , P2_INIT_POS , mh_players[PLAYER2][selectchara[PLAYER2]] , selectchara[PLAYER2] ) ;	// プレイヤー(モデル)の座標
			p_cam[0].InitSet( PLAYER1 , P1_INIT_ROT ) ;					// p1カメラ
			p_cam[1].InitSet( PLAYER2 , P2_INIT_ROT ) ;					// p2カメラ
		}
	}

}



