#pragma once
#include <d3d9.h>
#include <d3d9types.h>
#include <d3dx9tex.h>

#pragma warning(disable:4482)

//CGraphicクラスの生成と破棄はこれで行う。
enum LGraphicDisplayMode {
	FullScreen,
	WindowMode,
};

//-------------------------------------------
//	Direct3D　初期化　解放
//-------------------------------------------
bool LGraphic_Create( HWND hWnd, LGraphicDisplayMode mode = LGraphicDisplayMode::WindowMode );
void LGraphic_Release();	

//Direct3Dデバイスを参照する
LPDIRECT3DDEVICE9 GetDevice();

//-------------------------------------------
//	描画
//-------------------------------------------
void LGraphic_ClearAndBegin();//描画前に呼ぶ
void LGraphic_EndAndPresent();//描画後に呼ぶ
//テクスチャを描画
void LGraphic_Draw( float left, float top, float right, float bottom, LPDIRECT3DTEXTURE9 texture );

