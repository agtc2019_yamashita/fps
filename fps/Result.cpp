/* -----------------------------------------------------------------------------------------
|	
|   結果発表時　処理
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>

// このファイル(cpp)内での関数 ----------------------------
void ResultInit() ;
void Result3DDraw() ;
void ResultUIDraw() ;
void CrownMove() ;
void WinPlayer_1() ;
void WinPlayer_2() ;
void CameraMove( int winer ) ;

// このファイル(cpp)内でのグローバル変数 ----------------------------
VECTOR  result_camera ;				// --- リザルトで使用するカメラ
VECTOR	result_add ;				// --- カメラ加速度
int		killnum ;					// --- キルカウント仮格納変数
int		res_time = 0 ;				// --- リザルト内時間
int		res_rand[2] = { 0 , 99 } ;	// --- ジャカジャカランダム数値格納
int		wl_drawflg = 0 ;			// --- 勝敗引き分けフラグ	
int		result_time = 0 ;			// --- リザルト内時間
int		cooltime = 0 ;				// --- ボーナス前クールタイム
int		num_kari[2] ;				// --- ランダム仮取得
int		num_1[2] ;					// --- 1P点数
int		num_2[2] ;					// --- 2P点数
int		num_posy[2] ;				// --- 点数Y座標(勝利判定時、勝者だけ下に少しずらす)
float	num_size[2] ;				// --- 数字サイズ		
int		bonusb_alpha = 5 ;			// --- ボーナス背景	画像	アルファ値
int		bonus_alpha  = 5 ;			// --- ボーナス		画像	アルファ値
float	bonus_posx   = 2000.0f ;	// --- ボーナス		画像	X座標
float	camera_angle_result ;		// --- カメラアングル
bool	initflg = 0 ;				// --- 初期セットを一回しか呼ばない
bool	bonusgh_finish = FALSE ;	// --- ボーナス画像終了( FALSE : 終わってない , TRUE : 終わった )
int		windrawflg ;				// --- 0 : FALSE , 1 : 1P , 2 : 2P
float	player_rota1 ;				// --- プレイヤー回転
float	player_rota2 ;				// --- プレイヤー回転
bool	ballooninitflg = FALSE ;	// --- 風船初期セットフラグ
bool	rollflg = FALSE ;			// --- スネアロールflg

// --- UIモデル
int		gh_bonusbk ;				// --- 王冠ボーナス背景画像
int		gh_bonus ;					// --- 王冠ボーナス文字画像
int		gh_win_1p ;					// --- １P勝利画像
int		gh_win_2p ;					// --- ２P勝利画像

// --- 3Dモデル


void Result()
{
	sound.VolumeAdd() ;

	if ( initflg == 0 ) {
		// --- リザルト初期セット
		ResultInit() ;
		sound.now_playsound = s_Roll ;
		sound.PlaySoundBGM() ;
	}

	// --- 結果発表前ジャカジャカ
	if ( res_time < 180 ) {

		if ( rollflg == FALSE ) {
			rollflg = TRUE ;
			sound.PlaySoundSE( s_Roll ) ;
		}
		res_time++ ;
		res_rand[0]++ ;
		if ( res_rand[0] > 99 )
			res_rand[0] = 0 ;
		res_rand[1]-- ;
		if ( res_rand[1] < 1 )
			res_rand[1] = 99 ;
		num_kari[0] = res_rand[0] / 10 ;			// --- 十の位計算
		num_kari[1] = res_rand[1] / 10 ;			// --- 十の位計算
		num_1[0] = res_rand[1] - num_kari[1] * 10 ;	// --- 一の位計算
		num_1[1] = res_rand[0] - num_kari[0] * 10 ;	// --- 一の位計算
		num_2[0] = res_rand[1] - num_kari[1] * 10 ;	// --- 一の位計算
		num_2[1] = res_rand[0] - num_kari[0] * 10 ;	// --- 一の位計算


	}
	else {		// --- 結果発表表示----------------------------------------------------

		StopSoundMem( sound.s_roll ) ;		// --- スネアロールを止める
		if ( rollflg == TRUE ) {
			rollflg = FALSE ;
			sound.PlaySoundSE( s_Jan ) ;
		}
		// --- 数値初期化
		for ( int i = 0 ; i < 2 ; i++ ) {
			num_1[i] = 0 ;
			num_2[i] = 0 ;
		}

		num_1[0] = g_player[PLAYER1].killcnt / 10 ;				// --- 十の位計算
		num_1[1] = g_player[PLAYER1].killcnt - num_1[0] * 10 ;	// --- 一の位計算
		num_2[0] = g_player[PLAYER2].killcnt / 10 ;				// --- 十の位計算
		num_2[1] = g_player[PLAYER2].killcnt - num_2[0] * 10 ;	// --- 一の位計算

		cooltime++ ;
		// --- 同点だったら王冠を持っている方に加点
		if ( (g_player[PLAYER1].killcnt == g_player[PLAYER2].killcnt) && (wl_drawflg == 0) && (cooltime > 60) ) {

			// --- 1Pが持っていた時間の方が長かったら
			if ( g_player[PLAYER1].crownkeep > g_player[PLAYER2].crownkeep ) {
				wl_drawflg = 1 ;
			}
			else {		// --- 2Pが持っていた時間の方が長かったら
				wl_drawflg = 2 ;
			}
		}

		// --- 同点フラグが立っていたら、王冠移動
		if ( wl_drawflg != 0 ) {
			cooltime = 0 ;
			if (  bonusgh_finish == TRUE ) {
				result_time++ ;
				if ( result_time < 60 ) {
					crown.CrownStay() ;
				}
				else {
					crown.s_crown.y = 5.0f ;
					CrownMove() ;
				}
			}
		}
		else if ( cooltime > 60 ) {		// --- 勝者決定後---------------------------------------------------
			for ( int i = 0 ; i < 2 ; i++ ) {
				num_1[i] = 0 ;
				num_2[i] = 0 ;
			}
			num_1[0] = g_player[PLAYER1].killcnt / 10 ;				// --- 十の位計算
			num_1[1] = g_player[PLAYER1].killcnt - num_1[0] * 10 ;	// --- 一の位計算
			num_2[0] = g_player[PLAYER2].killcnt / 10 ;				// --- 十の位計算
			num_2[1] = g_player[PLAYER2].killcnt - num_2[0] * 10 ;	// --- 一の位計算


			// --- 1Pが勝っていたら勝利アニメーション
			if (g_player[PLAYER1].killcnt > g_player[PLAYER2].killcnt) {
				CameraMove( PLAYER1 ) ;
				WinPlayer_1( ) ;
			}
			else {		// --- 2Pが勝っていたら勝利アニメーション
				CameraMove( PLAYER2 ) ;
				WinPlayer_2( ) ;
			}
			// --- 風船移動
			for ( int i = 0 ; i < 10 ; i++ )
				balloon[i].Move() ;
			// 風船アニメーション
			for ( int i = 0 ; i < 10 ; i++ ){
				balloon[i].Play_Anim( ) ;
				MV1SetAttachAnimTime( balloon[i].mh_balloon,balloon[i].attachidx,balloon[i].playtime) ;
			}
		}
	}
	// --- ドローン移動
	drone.Move() ;	


	// --- リザルト3Dモデル描画
	Result3DDraw() ;
	// --- リザルトUI画像描画
	ResultUIDraw() ;


}


/* ------------------------------------------------------------------------- */
/*								初期セット									 */
/* ------------------------------------------------------------------------- */
void ResultInit()
{

	srand( (unsigned)time (NULL) ) ;			//乱す

	// --- UI画像読み込み
	gh_bonusbk			= LoadGraph	( "..\\Models\\bonus_bk.png" ) ;				// --- 王冠ボーナス背景		画像ハンドル
	gh_bonus			= LoadGraph	( "..\\Models\\bonus.png" ) ;					// --- 王冠ボーナス			画像ハンドル
	gh_win_1p			= LoadGraph	( "..\\Models\\win_1p.png" ) ;					// --- 1p勝利				画像ハンドル
	gh_win_2p			= LoadGraph	( "..\\Models\\win_2p.png" ) ;					// --- 2p勝利				画像ハンドル

	crown.s_crown		= VGet( 0.0f , 5.0f , 5.0f ) ;
	crown.crown_size	= VGet( 1.0f , 1.0f , 1.0f ) ;

	crown.updownflg = FALSE ;
	
	// --- 全画面へ切り替え
	SetDrawArea( 0 , 0, WINDOWSIZE_W , WINDOWSIZE_H );							// --- 画面分割設定(描画範囲)
	SetCameraScreenCenter( WINDOWSIZE_W/2 , WINDOWSIZE_H/2 );					// --- 画面分割設定

	// --- 座標設定 -----------------------------------------------------------
	g_player[PLAYER1].pos = VGet( 0.0f , 35.0f ,  420.0f ) ;					// --- プレイヤーの座標
	g_player[PLAYER2].pos = VGet( 0.0f , 35.0f , -420.0f ) ;					// --- プレイヤーの座標
	result_camera		  = VGet( -700.0f , 200.0f , 0.0f ) ;					// --- カメラの座標

	player_rota1 = 1.07f ;
	player_rota2 = 2.07f ;

	MV1SetPosition( g_player[PLAYER1].model_hnd , g_player[PLAYER1].pos );			// --- キャラクターの座標セット
	MV1SetPosition( g_player[PLAYER2].model_hnd , g_player[PLAYER2].pos );			// --- キャラクターの座標セット
	MV1SetRotationXYZ( g_player[PLAYER1].model_hnd,VGet(0.0f,player_rota1,0.0f)) ; 	// --- モデルの回転
	MV1SetRotationXYZ( g_player[PLAYER2].model_hnd,VGet(0.0f,player_rota2,0.0f)) ; 	// --- モデルの回転
	MV1SetScale( g_player[PLAYER1].model_hnd , VGet( 0.8f , 0.8f , 0.8f ) ) ;		// --- プレイヤーの拡縮率
	MV1SetScale( g_player[PLAYER2].model_hnd , VGet( 0.8f , 0.8f , 0.8f ) ) ;		// --- プレイヤーの拡縮率

	// --- カメラの座標と注視点をセット
	SetCameraPositionAndTarget_UpVecY( result_camera, VGet( 0.0f , 140.0f , 0.0f ) ) ;

	// --- 木箱の表示をOFFに
	stage[0].drawflg = FALSE ;

	crown.p_crown.x = 0.0f ;
	crown.p_crown.y = 5.0f ;
	crown.p_crown.z = 0.0f ;

	crown.range_crown[0] = crown.p_crown.y - 20.0f ;
	crown.range_crown[1] = crown.p_crown.y + 20.0f ;

	result_add = VGet( 200.0f , 0.0f , 0.0f ) ;	
	camera_angle_result = -1.5f ;

	num_size[PLAYER1] = 2.0f ;
	num_size[PLAYER2] = 2.0f ;

	num_posy[PLAYER1] = 100 ; 
	num_posy[PLAYER2] = 100 ;


	MV1SetPosition( crown.m_crown , crown.p_crown );			// --- 王冠の座標セット


	initflg = 1 ;
}

/* ------------------------------------------------------------------------- */
/*							リザルト3D描画									 */
/* ------------------------------------------------------------------------- */
void Result3DDraw()
{
	// --- モデルを描画
	MV1DrawModel( ground_castle );							// --- ゲームステージ
	MV1DrawModel( sky_castle );								// --- 空
	MV1DrawModel( g_player[PLAYER1].model_hnd );			// --- プレイヤー1
	MV1DrawModel( g_player[PLAYER2].model_hnd );			// --- プレイヤー2
	for ( int i = 0 ; i < BLOCKMAX ; i++ ){
		if ( stage[i].drawflg == TRUE )
			MV1DrawModel( stage[i].m_block ) ;				// --- 木箱
	}
	if ( wl_drawflg != 0 ) {
		MV1DrawModel( crown.m_crown );	
	}

	MV1DrawModel( drone.mh_drone ) ;						// ドローン表示

	for ( int i = 0 ; i < 10 ; i++ )
		MV1DrawModel( balloon[i].mh_balloon ) ;			// 風船表示

}

/* ------------------------------------------------------------------------- */
/*							リザルトUI描画									 */
/* ------------------------------------------------------------------------- */
void ResultUIDraw()
{
 	// --- 十の位描画(1P)
	DrawRectRotaGraph((WINDOWSIZE_W / 4 - 80) - (SCORESIZE_W / 2) , num_posy[PLAYER1] ,SCORESIZE_W * num_1[0],0,
		SCORESIZE_W,SCORESIZE_H, num_size[PLAYER1] , 0.0 , gh_score,TRUE, 0 , 0) ;
	// --- 一の位描画(1P)
	DrawRectRotaGraph((WINDOWSIZE_W / 4 - 80) + (SCORESIZE_W * 2) , num_posy[PLAYER1] ,SCORESIZE_W * num_1[1],0,
		SCORESIZE_W,SCORESIZE_H, num_size[PLAYER1] , 0.0 , gh_score,TRUE, 0 , 0) ;

	// --- 十の位描画(2P)
	DrawRectRotaGraph( (WINDOWSIZE_W / 4 * 3 + 80) - (SCORESIZE_W / 2) , num_posy[PLAYER2] ,SCORESIZE_W * num_2[0],0,
		SCORESIZE_W,SCORESIZE_H, num_size[PLAYER2] , 0.0 , gh_score,TRUE, 0 , 0) ;
	// --- 一の位描画(2P)
	DrawRectRotaGraph( (WINDOWSIZE_W / 4 * 3 + 80) + (SCORESIZE_W * 2) , num_posy[PLAYER2] ,SCORESIZE_W * num_2[1],0,
		SCORESIZE_W,SCORESIZE_H, num_size[PLAYER2] , 0.0 , gh_score,TRUE, 0 , 0) ;

	if ( wl_drawflg != 0 ) {
		if ( bonus_posx > WINDOWSIZE_W / 2 ) { 
			bonusb_alpha += 2 ;
			bonus_alpha += 2 ;
			bonus_posx -= 7.8f ;
		}
		else {
			bonusb_alpha -= 2 ;
			bonus_alpha -= 2 ;
			bonus_posx -= 7.8f ;

			if ( bonusb_alpha < 0 )
				bonusgh_finish = TRUE ;
		}

		// --- ボーナス背景
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, bonusb_alpha ) ;
		DrawRotaGraph( WINDOWSIZE_W / 2 , 200 , 1.0 , 0.0 , gh_bonusbk , TRUE , 0 , 0 ) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		// --- ボーナス文字
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, bonus_alpha ) ;
		DrawRotaGraph( (int)bonus_posx , 200 , 1.0 , 0.0 , gh_bonus , TRUE , 0 , 0 ) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

	}

	if ( windrawflg == 1 )
		DrawRotaGraph( WINDOWSIZE_W / 4 * 3 , WINDOWSIZE_H / 4 * 3 , 1.0 , 0.0 , gh_win_1p , TRUE , 0 , 0 ) ;
	if ( windrawflg == 2 )
		DrawRotaGraph( WINDOWSIZE_W / 4 , WINDOWSIZE_H / 4 * 3 , 1.0 , 0.0 , gh_win_2p , TRUE , 0 , 0 ) ;

}

/* ------------------------------------------------------------------------- */
/*					引き分け処理 : 王冠移動									 */
/* ------------------------------------------------------------------------- */
void CrownMove( )
{
	// --- 王冠が上に行くまで
	if ( crown.p_crown.y < 400.0f ) {

		// --- 1Pだったら左に
		if ( wl_drawflg == 1 ) {
			crown.p_crown.y += crown.s_crown.y ;
			crown.p_crown.z += crown.s_crown.z + 0.02f ;
		}
		// --- 2Pだったら右に
		if ( wl_drawflg == 2 ) {
			crown.p_crown.y += crown.s_crown.y ;
			crown.p_crown.z -= (crown.s_crown.z + 0.02f) ;
		}

		// --- 王冠回転
		crown.r_crown += 3.14f / 80 ;
		// --- 王冠縮小
		crown.crown_size.x -= 0.01f ;
		crown.crown_size.y -= 0.01f ;
		crown.crown_size.z -= 0.01f ;
	
		MV1SetPosition( crown.m_crown , crown.p_crown );					// --- 王冠の座標セット
		MV1SetScale( crown.m_crown , crown.crown_size ) ;					// --- 王冠拡縮セット
		MV1SetRotationXYZ( crown.m_crown ,VGet(0.0f,crown.r_crown,0.0f)) ; 	// --- 王冠の回転

	}
	else {
		if ( wl_drawflg == 1 ){ 
			g_player[PLAYER1].killcnt++ ;
			sound.PlaySoundSE( s_Jan ) ;
		}
		if ( wl_drawflg == 2 ) { 
			g_player[PLAYER2].killcnt++ ;
			sound.PlaySoundSE( s_Jan ) ;
		}
		wl_drawflg = 0 ;
		result_time = 0 ;
	}

}

/* ------------------------------------------------------------------------- */
/*					勝敗処理 : プレイヤー1勝利								 */
/* ------------------------------------------------------------------------- */
void WinPlayer_1( )
{
	g_player[PLAYER1].AnimMode_Win() ;
	g_player[PLAYER1].Play_Anim() ;
	MV1SetAttachAnimTime(g_player[PLAYER1].model_hnd,g_player[PLAYER1].attachidx,g_player[PLAYER1].playtime) ;

}

/* ------------------------------------------------------------------------- */
/*					勝敗処理 : プレイヤー2勝利								 */
/* ------------------------------------------------------------------------- */
void WinPlayer_2( )
{
	g_player[PLAYER2].AnimMode_Win() ;
	g_player[PLAYER2].Play_Anim() ;
	MV1SetAttachAnimTime(g_player[PLAYER2].model_hnd,g_player[PLAYER2].attachidx,g_player[PLAYER2].playtime) ;

}


/* ------------------------------------------------------------------------- */
/*					勝敗処理 : 勝敗カメラ移動								 */
/* ------------------------------------------------------------------------- */
void CameraMove( int winer )
{
	sound.PlaySoundSE( s_Kansei ) ;
	if ( winer == PLAYER1 ) {
		if ( camera_angle_result < -0.5 ) {
			camera_angle_result += 0.01f ;
//			result_add.y -= 0.03f ;
			result_add.z -= 1.0f ;
		}
		else {
			result_add.y = 0.0f ;
			windrawflg = 1 ;
			if ( player_rota1 < 2.37 )
				player_rota1 += 0.05 ;
		}
	
		result_camera.x = 1000 * sin(camera_angle_result) + result_add.x ;
		result_camera.y = result_camera.y + result_add.y ;           //カメラ位置をセット
		result_camera.z = 1000 * cos(camera_angle_result) + result_add.z ;
		SetCameraPositionAndTarget_UpVecY( result_camera, VGet(result_add.x , -100 + result_add.y , result_add.z) ) ;
		MV1SetRotationXYZ( g_player[PLAYER1].model_hnd,VGet(0.0f,player_rota1,0.0f)) ; 	// --- モデルの回転

		// --- 風船座標初期セット
		if ( ballooninitflg == FALSE ) {
			ballooninitflg = TRUE ;
			balloonpos[0] = VGet( 420 , -1300 , 250 ) ;
			balloonpos[1] = VGet( 280 ,  -600 , 250 ) ;
			balloonpos[2] = VGet( 140 ,  -800 , 250 ) ;
			balloonpos[3] = VGet(   0 , -1000 , 250 ) ;
			balloonpos[4] = VGet( 350 ,  -700 , 150 ) ;
			balloonpos[5] = VGet(  70 ,  -100 , 150 ) ;
			balloonpos[6] = VGet( 420 ,  -300 ,  50 ) ;
			balloonpos[7] = VGet( 280 ,  -900 ,  50 ) ;
			balloonpos[8] = VGet( 140 ,  -500 ,  50 ) ;
			balloonpos[9] = VGet(   0 ,  -200 ,  50 ) ;

			// バルーンオブジェクト初期セット
			for ( int i = 0 ; i < 10 ; i++ ){
				balloon[i].BalloonInit( balloonpos[i] , (i%5) ) ;
			}
		}
		if ( num_size[PLAYER1] < 3.0f ) 
			num_size[PLAYER1] += 0.1f ;
		if ( num_posy[PLAYER1] < 150 )
			num_posy[PLAYER1]++ ;
	}
	else {				// --- 2P
		if ( camera_angle_result > -2.5 ) {
			camera_angle_result -= 0.01f ;
//			result_add.y -= 0.03f ;
			result_add.z += 1.0f ;
		}
		else {
			result_add.y = 0.0f ;
			windrawflg = 2 ;
			if ( player_rota2 > 0.77 )
				player_rota2 -= 0.05 ;
		}
	
		result_camera.x = 1000 * sin(camera_angle_result) + result_add.x ;
		result_camera.y = result_camera.y + result_add.y ;           //カメラ位置をセット
		result_camera.z = 1000 * cos(camera_angle_result) + result_add.z ;
		SetCameraPositionAndTarget_UpVecY( result_camera, VGet(result_add.x , -100 + result_add.y , result_add.z) ) ;

		MV1SetRotationXYZ( g_player[PLAYER2].model_hnd,VGet(0.0f,player_rota2,0.0f)) ; 	// --- モデルの回転

		if ( ballooninitflg == FALSE ) {
			ballooninitflg = TRUE ;
			balloonpos[0] = VGet( -420 , -1300 , 250 ) ;
			balloonpos[1] = VGet( -280 ,  -600 , 250 ) ;
			balloonpos[2] = VGet( -140 ,  -800 , 250 ) ;
			balloonpos[3] = VGet(    0 , -1000 , 250 ) ;
			balloonpos[4] = VGet( -350 ,  -700 , 150 ) ;
			balloonpos[5] = VGet(  -70 ,  -100 , 150 ) ;
			balloonpos[6] = VGet( -420 ,  -300 ,  50 ) ;
			balloonpos[7] = VGet( -280 ,  -900 ,  50 ) ;
			balloonpos[8] = VGet( -140 ,  -500 ,  50 ) ;
			balloonpos[9] = VGet(    0 ,  -200 ,  50 ) ;

			// バルーンオブジェクト初期セット
			for ( int i = 0 ; i < 10 ; i++ ){
				balloon[i].BalloonInit( balloonpos[i] , (i%5) ) ;
			}
		}
		if ( num_size[PLAYER2] < 3.0f ) 
			num_size[PLAYER2] += 0.1f ;
		if ( num_posy[PLAYER2] < 150 )
			num_posy[PLAYER2]++ ;
	}
}








