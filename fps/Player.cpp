#include <windows.h>
#include "Common.h"
#include <math.h>

Player::Player()
{
	killcnt = 0 ;
	dethcnt = 0 ;
	dethflg = 0 ; // 死亡管理
}
Player::~Player()
{
}

/* -----------------------------------------------------------------------------------------
|	プレイヤーの初期セット
|       mode	0:1P 1:2P の設定をする 
|		initpos 初期位置の設定
+ --------------------------------------------------------------------------------------- */
void Player::InitSet( int mode , VECTOR initpos , int model , int playertype )
{
	pmode		= mode								; // 1p 2pの設定
	pos			= initpos							; // 初期座標のセット
	running		= TRUE								; // 走り中かどうかのフラグ
	model_hnd	= MV1DuplicateModel(model)			; // 選択したキャラのモデルハンドル
	playtime	= 0.0f								; // キャラのアニメ進捗時間
	rootflm		= MV1SearchFrame(model_hnd,"root")	;

	MV1SetScale( model_hnd ,VGet(0.5f , 0.5f , 0.5f) )	; // 壁			サイズ調整

	if ( playertype == Angel ) {
		// アニメーションの読み込み--------------------------------------
		anim_nutral = MV1LoadModel("..\\Models\\tensh_Anim_Wait.mv1") ;

		// プレイヤー立ちアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_nutral) ;

		anim_action_totaltime[eStand] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;

		// アニメーションの読み込み--------------------------------------
		anim_win = MV1LoadModel("..\\Models\\Player_Anim_Win.mv1") ;

		// プレイヤー勝ちアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_win) ;

		anim_action_totaltime[eWin] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;

		// アニメーションの読み込み--------------------------------------
		anim_deth = MV1LoadModel("..\\Models\\Player_Deth.mv1") ;

		// プレイヤーデスアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_deth) ;

		anim_action_totaltime[eDeth] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;

		//	走りアニメーションはここに追加-------------------------------
		anim_run = MV1LoadModel("..\\Models\\tensh_Anim_Run.mv1") ;

		// プレイヤー走りアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_run) ;

		anim_action_totaltime[eRun] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;
	
		// アニメーションして動いてもその場で動いてるような状態
		MV1SetFrameUserLocalMatrix(model_hnd,rootflm,MGetIdent()) ;
	}
	else {
		// アニメーションの読み込み--------------------------------------
		anim_nutral = MV1LoadModel("..\\Models\\Player_Anim_Wait.mv1") ;

		// プレイヤー立ちアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_nutral) ;

		anim_action_totaltime[eStand] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;

		// アニメーションの読み込み--------------------------------------
		anim_win = MV1LoadModel("..\\Models\\Player_Anim_Win.mv1") ;

		// プレイヤー勝ちアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_win) ;

		anim_action_totaltime[eWin] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;

		// アニメーションの読み込み--------------------------------------
		anim_deth = MV1LoadModel("..\\Models\\Player_Deth.mv1") ;

		// プレイヤーデスアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_deth) ;

		anim_action_totaltime[eDeth] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;

		//	走りアニメーションはここに追加-------------------------------
		anim_run = MV1LoadModel("..\\Models\\Player_Walk.mv1") ;

		// プレイヤー走りアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,anim_run) ;

		anim_action_totaltime[eRun] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;
	
		// アニメーションして動いてもその場で動いてるような状態
		MV1SetFrameUserLocalMatrix(model_hnd,rootflm,MGetIdent()) ;
	}

}

/* -----------------------------------------------------------------------------------------
|	
|       タイトル時のアニメーションセット処理
|		
+ --------------------------------------------------------------------------------------- */
void Player::TitleInitSet( int mode , VECTOR initpos , int model , int playertype )
{
	pmode		= mode								; // 1p 2pの設定
	pos			= initpos							; // 初期座標のセット
	running		= TRUE								; // 走り中かどうかのフラグ
	model_hnd	= MV1DuplicateModel(model)			; // 選択したキャラのモデルハンドル
	playtime	= 0.0f								; // キャラのアニメ進捗時間
	rootflm		= MV1SearchFrame(model_hnd,"root")	;

	if ( playertype == Magic ) {
		//	ニートタイトルアニメーションはここに追加-------------------------------
		animtitle_m = MV1LoadModel("..\\Models\\PlayerAnimTitle_magic.mv1") ;

		// ニートタイトルアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,animtitle_m) ;

		anim_action_totaltime[eMagicTitle] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;
	
		// アニメーションして動いてもその場で動いてるような状態
		MV1SetFrameUserLocalMatrix(model_hnd,rootflm,MGetIdent()) ;
		nowanim = eMagicTitle ;
		attachidx = MV1AttachAnim(model_hnd,0,animtitle_m) ;
		anim_totaltime = anim_action_totaltime[eMagicTitle] ;
	}
	if ( playertype == Neet ) {
		//	ニートタイトルアニメーションはここに追加-------------------------------
		animtitle_n = MV1LoadModel("..\\Models\\PlayerAnimTitle_neet.mv1") ;

		// ニートタイトルアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,animtitle_n) ;

		anim_action_totaltime[eNeetTitle] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;
	
		// アニメーションして動いてもその場で動いてるような状態
		MV1SetFrameUserLocalMatrix(model_hnd,rootflm,MGetIdent()) ;
		nowanim = eNeetTitle ;
		attachidx = MV1AttachAnim(model_hnd,0,animtitle_n) ;
		anim_totaltime = anim_action_totaltime[eNeetTitle] ;
	}
	if ( playertype == Angel ) {
		//	ニートタイトルアニメーションはここに追加-------------------------------
		animtitle_t = MV1LoadModel("..\\Models\\PlayerAnimTitle_tensh.mv1") ;

		// ニートタイトルアニメの時間を取得
		tmp_attachidx = MV1AttachAnim(model_hnd,0,animtitle_t) ;

		anim_action_totaltime[eTenshTitle] = MV1GetAttachAnimTotalTime(model_hnd,tmp_attachidx) ;
		MV1DetachAnim(model_hnd,tmp_attachidx) ;
	
		// アニメーションして動いてもその場で動いてるような状態
		MV1SetFrameUserLocalMatrix(model_hnd,rootflm,MGetIdent()) ;
		nowanim = eTenshTitle ;
		attachidx = MV1AttachAnim(model_hnd,0,animtitle_t) ;
		anim_totaltime = anim_action_totaltime[eTenshTitle] ;
	}


}

/* -----------------------------------------------------------------------------------------
|	
|       プレイヤーの入力時処理
|		
+ --------------------------------------------------------------------------------------- */
void Player::pAction( )
{

	// スピード初期化
	spd = VGet( 0.0f , 0.0f , 0.0f ) ;

	// コントローラーの設定
	switch ( pmode )
	{
		case 0 :	// 1P操作
			// 移動------------------------------------------------
			if( (Pad[PLAYER1] & PAD_INPUT_UP) || (CheckHitKey( KEY_INPUT_W ) == 1) )
			{
				spd.x += sinf(p_cam[PLAYER1].HRotate) * PLYSPD;
				spd.z += cosf(p_cam[PLAYER1].HRotate) * PLYSPD;				
			}
			if( (Pad[PLAYER1] & PAD_INPUT_DOWN) || (CheckHitKey( KEY_INPUT_S ) == 1) )
			{
				spd.x -= sinf(p_cam[PLAYER1].HRotate) * PLYSPD;
				spd.z -= cosf(p_cam[PLAYER1].HRotate) * PLYSPD;				
			}
			if( (Pad[PLAYER1] & PAD_INPUT_LEFT) || (CheckHitKey( KEY_INPUT_A ) == 1) )
			{
				spd.x += sinf(p_cam[PLAYER1].HRotate - DX_PI_F / 2) * PLYSPD;
				spd.z += cosf(p_cam[PLAYER1].HRotate - DX_PI_F / 2) * PLYSPD;	
				// 斜め移動調整
				if( (Pad[PLAYER1] & PAD_INPUT_UP) || (Pad[PLAYER1] & PAD_INPUT_DOWN) ){
					spd.x /= 1.41421356 ;
					spd.z /= 1.41421356 ;
				}
			}
			if( (Pad[PLAYER1] & PAD_INPUT_RIGHT) || (CheckHitKey( KEY_INPUT_D ) == 1) )
			{
				spd.x += sinf(p_cam[PLAYER1].HRotate + DX_PI_F / 2) * PLYSPD;
				spd.z += cosf(p_cam[PLAYER1].HRotate + DX_PI_F / 2) * PLYSPD;				
				// 斜め移動調整
				if( (Pad[PLAYER1] & PAD_INPUT_UP) || (Pad[PLAYER1] & PAD_INPUT_DOWN) ){
					spd.x /= 1.41421356 ;
					spd.z /= 1.41421356 ;
				}
			}

			switch ( operation_mode)
			{
				case 0 :	// ○射撃-----------------------
					// 弾発射
					if ( ((Pad[PLAYER1] & PAD_INPUT_B) || (CheckHitKey( KEY_INPUT_F ) == 1)) && (  shotflg == FALSE ) )
					{
						for ( int i = 0 ; i < BULLETMAX+add_bullet ; i++){
							if ( (p_bullet[PLAYER1][i].usingflg == FALSE) && ( p_bullet[PLAYER1][i].exploflg == FALSE) ){
								sound.PlaySoundSE( s_Shot ) ;
								p_bullet[PLAYER1][i].InitSet( g_player[PLAYER1].pos , VGet( sinf(p_cam[PLAYER1].HRotate), 0.0f ,
																											cosf(p_cam[PLAYER1].HRotate)) , p_cam[PLAYER1].HRotate ) ;
								shotflg = TRUE ;
								break ;
							}
						}
					}else if ( (Pad[PLAYER1] & PAD_INPUT_B) == 0 ) {
						shotflg = FALSE ;
					}
					break ;

				case 1 :	// LR射撃-----------------------

					// 射撃処理
					if ( ((Pad[PLAYER1] & PAD_INPUT_R) || (Pad[PLAYER1] & PAD_INPUT_L) 
						|| (Pad[PLAYER1] & PAD_INPUT_Y)|| (Pad[PLAYER1] & PAD_INPUT_Z)
						|| (CheckHitKey( KEY_INPUT_F ) == 1)) && ( shotflg == FALSE) )
					{
						for ( int i = 0 ; i < BULLETMAX+add_bullet ; i++){
							if ( (p_bullet[PLAYER1][i].usingflg == FALSE) && ( p_bullet[PLAYER1][i].exploflg == FALSE) ){
								sound.PlaySoundSE( s_Shot ) ;				// --- 銃撃音処理
								p_bullet[PLAYER1][i].InitSet( g_player[PLAYER1].pos , VGet( sinf(p_cam[PLAYER1].HRotate) , 0.0f , cosf(p_cam[PLAYER1].HRotate)) , p_cam[PLAYER1].HRotate ) ;
								shotflg = TRUE ;
								break ;
							}
						}
					}else if ( ((Pad[PLAYER1] & PAD_INPUT_R) == 0) && ((Pad[PLAYER1] & PAD_INPUT_L) == 0)
						&& ((Pad[PLAYER1] & PAD_INPUT_Y) == 0) && ((Pad[PLAYER1] & PAD_INPUT_Z) == 0)
						&& (CheckHitKey( KEY_INPUT_F ) == 0) ) {
						shotflg = FALSE ;
					}
					break ;
			}

			//	プレイヤー1の現在地
			if ( (CheckHitKey( KEY_INPUT_B )) == 1 ){
				printfDx( " x = %f" , pos.x ) ;
				printfDx( " y = %f" , pos.y ) ;
				printfDx( " z = %f\n" , pos.z ) ;
			}

			break ;

		case 1 : // 2p操作
		
			// 移動--------------------------------------------------------------------
			if( (Pad[PLAYER2] & PAD_INPUT_UP) || (CheckHitKey( KEY_INPUT_I ) == 1) )
			{
				spd.x += sinf(p_cam[PLAYER2].HRotate) * PLYSPD;
				spd.z += cosf(p_cam[PLAYER2].HRotate) * PLYSPD;				
			}
			if( (Pad[PLAYER2] & PAD_INPUT_DOWN) || (CheckHitKey( KEY_INPUT_K ) == 1) )
			{
				spd.x -= sinf(p_cam[PLAYER2].HRotate) * PLYSPD;
				spd.z -= cosf(p_cam[PLAYER2].HRotate) * PLYSPD;				
			}
			if( (Pad[PLAYER2] & PAD_INPUT_LEFT) || (CheckHitKey( KEY_INPUT_J ) == 1) )
			{
				spd.x += sinf(p_cam[PLAYER2].HRotate - DX_PI_F / 2) * PLYSPD;
				spd.z += cosf(p_cam[PLAYER2].HRotate - DX_PI_F / 2) * PLYSPD;
				// 斜め移動調整
				if( (Pad[PLAYER2] & PAD_INPUT_UP) || (Pad[PLAYER2] & PAD_INPUT_DOWN) ){
					spd.x /= 1.41421356 ;
					spd.z /= 1.41421356 ;
				}
			}
			if( (Pad[PLAYER2] & PAD_INPUT_RIGHT) || (CheckHitKey( KEY_INPUT_L ) == 1) )
			{
				spd.x += sinf(p_cam[PLAYER2].HRotate + DX_PI_F / 2) * PLYSPD;
				spd.z += cosf(p_cam[PLAYER2].HRotate + DX_PI_F / 2) * PLYSPD;				
				// 斜め移動調整
				if( (Pad[PLAYER2] & PAD_INPUT_UP) || (Pad[PLAYER2] & PAD_INPUT_DOWN) ){
					spd.x /= 1.41421356 ;
					spd.z /= 1.41421356 ;
				}
			}

			switch ( operation_mode)
			{
				case 0 :	// ○射撃-----------------------
					// 弾発射
					if ( ((Pad[PLAYER2] & PAD_INPUT_B) || (CheckHitKey( KEY_INPUT_H ) == 1)) && (  shotflg == FALSE ) )
					{
						for ( int i = 0 ; i < BULLETMAX+add_bullet ; i++){
							if ( (p_bullet[PLAYER2][i].usingflg == FALSE) && ( p_bullet[PLAYER2][i].exploflg == FALSE ) ){
								sound.PlaySoundSE( s_Shot ) ;
								p_bullet[PLAYER2][i].InitSet( g_player[PLAYER2].pos , VGet( sinf(p_cam[PLAYER2].HRotate), 0.0f ,
																											cosf(p_cam[PLAYER2].HRotate)) , p_cam[PLAYER2].HRotate ) ;
								shotflg = TRUE ;
								break ;
							}
						}
					}else if ( (Pad[PLAYER2] & PAD_INPUT_B) == 0 ) {
						shotflg = FALSE ;
					}
					break ;

				case 1 :	// LR射撃-----------------------

					// 射撃処理
					if ( ((Pad[PLAYER2] & PAD_INPUT_R) || (Pad[PLAYER2] & PAD_INPUT_L) 
						|| (Pad[PLAYER2] & PAD_INPUT_Y)|| (Pad[PLAYER2] & PAD_INPUT_Z)
						|| (CheckHitKey( KEY_INPUT_H ) == 1)) && ( shotflg == FALSE) )
					{
						for ( int i = 0 ; i < BULLETMAX+add_bullet ; i++){
							if (( p_bullet[PLAYER2][i].usingflg == FALSE) && ( p_bullet[PLAYER2][i].exploflg == FALSE) ){
								sound.PlaySoundSE( s_Shot ) ;				// --- 銃撃音処理
								p_bullet[PLAYER2][i].InitSet( g_player[PLAYER2].pos , VGet( sinf(p_cam[PLAYER2].HRotate) , 0.0f , cosf(p_cam[PLAYER2].HRotate)) , p_cam[PLAYER2].HRotate ) ;
								shotflg = TRUE ;
								break ;
							}
						}
					}else if ( ((Pad[PLAYER2] & PAD_INPUT_R) == 0) && ((Pad[PLAYER2] & PAD_INPUT_L) == 0)
						&& ((Pad[PLAYER2] & PAD_INPUT_Y) == 0) && ((Pad[PLAYER2] & PAD_INPUT_Z) == 0)
						&& (CheckHitKey( KEY_INPUT_H ) == 0) ) {
						shotflg = FALSE ;
					}
					break ;
			}
			break ;
		
	} 
}

void Player::pRespawn( VECTOR respos )
{
	pos	= respos	; // 初期座標のセット
	dethflg = FALSE ;
}

/* -----------------------------------------------------------------------------------------
|	
|       プレイヤーと壁の当たり判定関数
|			blockpos :
+ --------------------------------------------------------------------------------------- */
void Player::PlayerHitcheck( VECTOR blockpos )			
{
	VECTOR blo_pos ;

	// 箱とのヒットチェック
	if ( sc_fin_fun == FALSE ){
		for ( int i = 0 ; i <  BLOCKMAX ; i++ )
		{
			blo_pos = stage[i].p_block  ;

			if( (pos.z < blo_pos.z + BLOCKSIZE_OFF) && (pos.z > blo_pos.z -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x - (BLOCKSIZE_OFF -20.0f)) && (pos.x > blo_pos.x - BLOCKSIZE_OFF) && (spd.x > 0.0f) )
			{
				spd.x = 0.0f ;
			}
			if( (pos.z < blo_pos.z +  BLOCKSIZE_OFF) && (pos.z > blo_pos.z -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x + BLOCKSIZE_OFF)  && (pos.x > blo_pos.x +  (BLOCKSIZE_OFF - 20.0f)) && (spd.x < 0.0f) )
			{
				spd.x = 0.0f ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x +  BLOCKSIZE_OFF) && (pos.z > blo_pos.z - BLOCKSIZE_OFF) && (pos.z < blo_pos.z - (BLOCKSIZE_OFF - 20.0f)) && (spd.z > 0) )
			{
				spd.z = 0.0f ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x +  BLOCKSIZE_OFF) && (pos.z > blo_pos.z + (BLOCKSIZE_OFF - 20.0f)) && (pos.z < blo_pos.z + BLOCKSIZE_OFF) && (spd.z < 0) )
			{
				spd.z = 0.0f ;
			}
		}
	}else{
		for ( int i = 0 ; i < CHAIRMAX ; i++ ){
			
			blo_pos = stage[i].p_chair  ;

			if ( i < 4 ){
				if( (pos.z < blo_pos.z + CHAIRSIZEZ_OFF) && (pos.z > blo_pos.z -  CHAIRSIZEZ_OFF) && (pos.x < blo_pos.x - (CHAIRSIZEX_OFF -20.0f)) && (pos.x > blo_pos.x - CHAIRSIZEX_OFF) && (spd.x > 0.0f) )
				{
					spd.x = 0.0f ;
				}
				if( (pos.z < blo_pos.z +  CHAIRSIZEZ_OFF) && (pos.z > blo_pos.z -  CHAIRSIZEZ_OFF) && (pos.x < blo_pos.x + CHAIRSIZEX_OFF)  && (pos.x > blo_pos.x +  (CHAIRSIZEX_OFF - 20.0f)) && (spd.x < 0.0f) )
				{
					spd.x = 0.0f ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEX_OFF) && (pos.x < blo_pos.x +  CHAIRSIZEX_OFF) && (pos.z > blo_pos.z - CHAIRSIZEZ_OFF) && (pos.z < blo_pos.z - (CHAIRSIZEZ_OFF - 20.0f)) && (spd.z > 0) )
				{
					spd.z = 0.0f ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEX_OFF) && (pos.x < blo_pos.x +  CHAIRSIZEX_OFF) && (pos.z > blo_pos.z + (CHAIRSIZEZ_OFF - 20.0f)) && (pos.z < blo_pos.z + BLOCKSIZE_OFF) && (spd.z < 0) )
				{
					spd.z = 0.0f ;
				}
			}else{
				if( (pos.z < blo_pos.z + CHAIRSIZEX_OFF) && (pos.z > blo_pos.z -  CHAIRSIZEX_OFF) && (pos.x < blo_pos.x - (CHAIRSIZEZ_OFF -20.0f)) && (pos.x > blo_pos.x - CHAIRSIZEZ_OFF) && (spd.x > 0.0f) )
				{
					spd.x = 0.0f ;
				}
				if( (pos.z < blo_pos.z +  CHAIRSIZEX_OFF) && (pos.z > blo_pos.z -  CHAIRSIZEX_OFF) && (pos.x < blo_pos.x + CHAIRSIZEZ_OFF)  && (pos.x > blo_pos.x +  (CHAIRSIZEZ_OFF - 20.0f)) && (spd.x < 0.0f) )
				{
					spd.x = 0.0f ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEZ_OFF) && (pos.x < blo_pos.x +  CHAIRSIZEX_OFF) && (pos.z > blo_pos.z - CHAIRSIZEX_OFF) && (pos.z < blo_pos.z - (CHAIRSIZEX_OFF - 20.0f)) && (spd.z > 0) )
				{
					spd.z = 0.0f ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEZ_OFF) && (pos.x < blo_pos.x +  CHAIRSIZEZ_OFF) && (pos.z > blo_pos.z + (CHAIRSIZEX_OFF - 20.0f)) && (pos.z < blo_pos.z + CHAIRSIZEX_OFF) && (spd.z < 0) )
				{
					spd.z = 0.0f ;
				}
			}

		}

		for ( int i = 0 ; i < 3 ; i++ ){
			blo_pos = stage[i].p_block  ;

			if( (pos.z < blo_pos.z + BLOCKSIZE_OFF) && (pos.z > blo_pos.z -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x - (BLOCKSIZE_OFF -20.0f)) && (pos.x > blo_pos.x - BLOCKSIZE_OFF) && (spd.x > 0.0f) )
			{
				spd.x = 0.0f ;
			}
			if( (pos.z < blo_pos.z +  BLOCKSIZE_OFF) && (pos.z > blo_pos.z -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x + BLOCKSIZE_OFF)  && (pos.x > blo_pos.x +  (BLOCKSIZE_OFF - 20.0f)) && (spd.x < 0.0f) )
			{
				spd.x = 0.0f ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x +  BLOCKSIZE_OFF) && (pos.z > blo_pos.z - BLOCKSIZE_OFF) && (pos.z < blo_pos.z - (BLOCKSIZE_OFF - 20.0f)) && (spd.z > 0) )
			{
				spd.z = 0.0f ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_OFF) && (pos.x < blo_pos.x +  BLOCKSIZE_OFF) && (pos.z > blo_pos.z + (BLOCKSIZE_OFF - 20.0f)) && (pos.z < blo_pos.z + BLOCKSIZE_OFF) && (spd.z < 0) )
			{
				spd.z = 0.0f ;
			}
		}
	}

	// 壁とのヒットチェック-----------------------------------
	if ( pos.z >= STAGESIZE_Z * STAGESCALE && spd.z > 0 )
	{
		spd.z = 0.0f ;
	}
	if ( pos.z <= -STAGESIZE_Z * STAGESCALE && spd.z < 0)
	{
		spd.z = 0.0f ;
	}
	if ( pos.x >= STAGESIZE_X * STAGESCALE && spd.x > 0 )
	{
		spd.x = 0.0f ;
	}
	if ( pos.x <= -STAGESIZE_X * STAGESCALE && spd.x < 0 )
	{
		spd.x = 0.0f ;
	}
	
}

void Player::Move()
{
	// --- プレイヤー移動
	pos.x += spd.x ;
	pos.y += spd.y ;
	pos.z += spd.z ;

}

/* -----------------------------------------------------------------------------------------
|
|       立ちアニメーションに切り替える
|
+ --------------------------------------------------------------------------------------- */
int Player::AnimMode_Nutral( )
{
	// キーが入力されて立ち中だったら走りアニメにする
	nowanim = eStand ;
	running = FALSE ;
	MV1DetachAnim(model_hnd,attachidx) ;
	attachidx = MV1AttachAnim(model_hnd,0,anim_nutral) ;
	anim_totaltime = anim_action_totaltime[eStand] ;
	return ( 0 ) ;
}

/* -----------------------------------------------------------------------------------------
|
|       走りアニメーションに切り替える
|
+ --------------------------------------------------------------------------------------- */
int Player::AnimMode_Run( )
{
	// キーが入力されて立ち中だったら走りアニメにする
	nowanim = eRun ;
	running = TRUE ;
	MV1DetachAnim(model_hnd,attachidx) ;
	attachidx = MV1AttachAnim(model_hnd,0,anim_run) ;
	anim_totaltime = anim_action_totaltime[eRun] ;
	return ( 0 ) ;
}

/* -----------------------------------------------------------------------------------------
|
|       やられアニメーションに切り替える
|
+ --------------------------------------------------------------------------------------- */
int Player::AnimMode_Deth( )
{
	// やられ状態をonに
	nowanim = eDeth ;
	running = FALSE ;
	dethflg = TRUE ;
	MV1DetachAnim(model_hnd,attachidx) ;
	attachidx = MV1AttachAnim(model_hnd,0,anim_deth) ;
	anim_totaltime = anim_action_totaltime[eDeth] ;
	return ( 0 ) ;
}

/* -----------------------------------------------------------------------------------------
|
|       勝利アニメーションに切り替える
|
+ --------------------------------------------------------------------------------------- */
int Player::AnimMode_Win( )
{
	// 勝利状態をonに
	nowanim = eWin ;
	MV1DetachAnim(model_hnd,attachidx) ;
	attachidx = MV1AttachAnim(model_hnd,0,anim_win) ;
	anim_totaltime = anim_action_totaltime[eWin] ;
	return ( 0 ) ;
}

/* -----------------------------------------------------------------------------------------
|
|       アニメーションの再生
|
+ --------------------------------------------------------------------------------------- */
int Player::Play_Anim( )
{
	// アニメーション進行
	switch ( nowanim ){
		case eStand :
			playtime += 0.5f ;
			if(playtime> anim_totaltime){
				playtime = 0.0f ;
			}
			break ;

		case eRun :
			playtime += 1.3f ;
			if(playtime> anim_totaltime){
				playtime = 0.0f ;
			}
			break ;

		case eDeth :
			playtime += 0.5f ;
			break ;

		case eWin :
			playtime += 0.5f ;
			break ;

		case eMagicTitle :
			playtime += 0.1f ;
			if(playtime> anim_totaltime){
				playtime = 0.0f ;
			}
			break ;

		case eNeetTitle :
			playtime += 0.3f ;
			if(playtime> anim_totaltime){
				playtime = 0.0f ;
			}
			break ;

		case eTenshTitle :
			playtime += 0.5f ;
			if(playtime> anim_totaltime){
				playtime = 0.0f ;
			}
			break ;


	}
	
	// モデルの移動
//	MV1SetPosition(chara_model,pos);

	return ( 0 ) ;
}


