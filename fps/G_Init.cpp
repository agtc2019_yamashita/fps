/* -----------------------------------------------------------------------------------------
|	
|   初期セット	主にモデルの読み込み　初期座標セットなど
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
void G_Init()
{
	// UI画像読み込み---------------------------------------------------------
	gh_usebullet[0]		= LoadGraph	( "..\\Models\\RedBullet.png" ) ;		// 赤弾						画像読み込み 
	gh_usebullet[1]		= LoadGraph	( "..\\Models\\BlueBullet.png" ) ;		// 青弾						画像読み込み 
	gh_nonbullet		= LoadGraph	( "..\\Models\\NonBullet.png" ) ;		// 使用済み弾				画像読み込み
	gh_stage			= LoadGraph	( "..\\Models\\stage.png" ) ;			// ミニマップ				画像読み込み
	gh_miniplayer[0]	= LoadGraph	( "..\\Models\\redplayer.png" ) ;		// ミニマップ赤プレイヤー	画像読み込み
	gh_miniplayer[1]	= LoadGraph	( "..\\Models\\buleplayer.png" ) ;		// ミニマップ青プレイヤー	画像読み込み
	gh_minicube			= LoadGraph	( "..\\Models\\minicube.png" ) ;		// ミニマップ木箱			画像読み込み
	gh_numberback		= LoadGraph	( "..\\Models\\numberback.png" ) ;		// 時計背景					画像読み込み
	gh_number[0]		= LoadGraph	( "..\\Models\\number_g.png" ) ;		// 緑数字					画像読み込み
	gh_number[1]		= LoadGraph	( "..\\Models\\number_o.png" ) ;		// 橙数字					画像読み込み
	gh_number[2]		= LoadGraph	( "..\\Models\\number_r.png" ) ;		// 赤数字					画像読み込み
	gh_go				= LoadGraph	( "..\\Models\\Go.png" ) ;				// GO						画像読み込み
	gh_score			= LoadGraph	( "..\\Models\\score.png" ) ;			// スコア数字				画像読み込み
	gh_finish			= LoadGraph	( "..\\Models\\finish.png" ) ;			// 終了						画像読み込み
	gh_lrbutton			= LoadGraph	( "..\\Models\\LRBUTTON.png" ) ;		// 操作説明LRカメラボタン	画像読み込み
	gh_atkbutton		= LoadGraph	( "..\\Models\\attackBUTTON.png" ) ;	// 操作説明〇攻撃ボタン		画像読み込み
	gh_cmrbutton		= LoadGraph	( "..\\Models\\cameraButton.png" ) ;	// 操作説明〇□カメラボタン	画像読み込み
	gh_lratk			= LoadGraph	( "..\\Models\\LRAttack.png" ) ;		// 操作説明LR攻撃ボタン		画像読み込み
	gh_oukanget			= LoadGraph	( "..\\Models\\oukanget.png" ) ;		// 操作説明王冠取得ボタン	画像読み込み
	gh_oukangetaf		= LoadGraph	( "..\\Models\\crowngetinfo.png" ) ;	// 操作説明王冠取得後ボタン	画像読み込み
	gh_oukandes			= LoadGraph	( "..\\Models\\oukandes.png" ) ;		// 操作説明王冠ロスボタン	画像読み込み
	gh_explosion		= LoadGraph	( "..\\Models\\Bom.png" ) ;				// 爆発						画像読み込み
	gh_waku				= LoadGraph	( "..\\Models\\waku.png" ) ;			// 枠線						画像読み込み
	gh_charaselect_waku	= LoadGraph	( "..\\Models\\ChaSele_Waku.png" ) ;	// キャラセレクト枠線		画像読み込み
	gh_kill				= LoadGraph	( "..\\Models\\Kill.png" ) ;			// キル						画像読み込み
	gh_ready			= LoadGraph	( "..\\Models\\Ready.png" ) ;			// 準備完了					画像読み込み
	gh_blackball		= LoadGraph	( "..\\Models\\blackball.png" ) ;		// トランジション			画像読み込み
	gh_charatrans		= LoadGraph	( "..\\Models\\charatrans.png" ) ;		// トランジション			画像読み込み
	gh_charatransbk		= LoadGraph	( "..\\Models\\charatransbk.png" ) ;	// トランジション背景		画像読み込み
	gh_option_A			= LoadGraph	( "..\\Models\\option_2.png" ) ;		// 操作方法○射撃			画像読み込み
	gh_option_B			= LoadGraph	( "..\\Models\\option.png" ) ;			// 操作方法LR射撃			画像読み込み
	gh_charasel_bo		= LoadGraph	( "..\\Models\\charasel_bo.png" ) ;		// キャラ選択ブラックアウト 画像読み込み
	gh_option_bo		= LoadGraph	( "..\\Models\\option_bo.png" ) ;		// 操作方法ブラックアウト	画像読み込み
	gh_start			= LoadGraph	( "..\\Models\\start.png" ) ;			// start					画像読み込み
	gh_number321[0]		= LoadGraph	( "..\\Models\\1.png" ) ;				// 1						画像読み込み
	gh_number321[1]		= LoadGraph	( "..\\Models\\2.png" ) ;				// 2						画像読み込み
	gh_number321[2]		= LoadGraph	( "..\\Models\\3.png" ) ;				// 3						画像読み込み
	gh_V				= LoadGraph	( "..\\Models\\big_V.png" ) ;			// V						画像読み込み
	gh_S				= LoadGraph	( "..\\Models\\big_S.png" ) ;			// S						画像読み込み
	gh_sc_font			= LoadGraph	( "..\\Models\\stage_jump.png" ) ;		// シーンチェンジ時文字		画像読み込み
	gh_sc_fontbk		= LoadGraph	( "..\\Models\\bonus_bk.png" ) ;		// シーンチェンジ時文字背景 画像読み込み
	gh_brackoutsc		= LoadGraph	( "..\\Models\\sc_brackout.png" ) ;		// 暗転処理					画像読み込み
	gh_title[0]			= LoadGraph ( "..\\Models\\R_title.png" ) ;			// タイトル文字 "R"			画像読み込み
	gh_title[1]			= LoadGraph ( "..\\Models\\F_title.png" ) ;			// タイトル文字 "F"			画像読み込み
	gh_title[2]			= LoadGraph ( "..\\Models\\E_title.png" ) ;			// タイトル文字 "E"			画像読み込み
	gh_title[3]			= LoadGraph ( "..\\Models\\L_title.png" ) ;			// タイトル文字 "L"			画像読み込み
	gh_title[4]			= LoadGraph ( "..\\Models\\L_title.png" ) ;			// タイトル文字 "L"			画像読み込み
	gh_title[5]			= LoadGraph ( "..\\Models\\E_title.png" ) ;			// タイトル文字 "E"			画像読み込み
	gh_title[6]			= LoadGraph ( "..\\Models\\T_title.png" ) ;			// タイトル文字 "T"			画像読み込み
	gh_1P_deth			= LoadGraph ( "..\\Models\\p1_dethT.png" ) ;		// 1Pやられ表示 
	gh_2P_deth			= LoadGraph ( "..\\Models\\p2_dethT.png" ) ;		// 2Pやられ表示
	gh_subtitle			= LoadGraph ( "..\\Models\\sub_title.png" ) ;		// サブタイトル文字			画像読み込み
	gh_startop			= LoadGraph ( "..\\Models\\start_option.png" ) ;	// スタート文字				画像読み込み

	// モデル読み込み----------------------------------------------------------
	ground				= MV1LoadModel( "..\\Models\\rs_stage.mv1" )	;		// 地面			モデル読み込み
	ground_castle		= MV1LoadModel( "..\\Models\\stage_castle.mv1" );		// 地面			モデル読み込み //
	sky_castle			= MV1LoadModel( "..\\Models\\kakoi.mv1" )		;	// 空			モデル読み込み //
	sky					= MV1LoadModel( "..\\Models\\sky1102.mv1" )		;	// 空			モデル読み込み
	bullet[0]			= MV1LoadModel( "..\\Models\\Ball2.mv1" )		;	// 弾丸赤		モデル読み込み //
	bullet[1]			= MV1LoadModel( "..\\Models\\Ball1.mv1" )	    ;	// 弾丸青		モデル読み込み //
	shadow1				= MV1LoadModel( "..\\Models\\BallShadow.mv1" )  ;   // 弾丸の影		モデル読み込み //
	land				= MV1LoadModel( "..\\Models\\game_ground.mv1" ) ;   // 地面			モデル読み込み
	cursor[0]			= MV1LoadModel( "..\\Models\\cursor.mv1" )   ;		// 矢印1P		モデル読み込み //
	cursor[1]			= MV1DuplicateModel(cursor[0]) ;					// 矢印2P		モデル読み込み //
	boom				= MV1LoadModel( "..\\Models\\boom.mv1" ) ;          // 爆発			モデル読み込み //
	boom2				= MV1LoadModel( "..\\Models\\boom2.mv1" ) ;			// 爆発			モデル読み込み //
	p1mark				= MV1LoadModel( "..\\Models\\p1.mv1" ) ;			// P1ポインタ	モデル読み込み //
	p2mark				= MV1LoadModel( "..\\Models\\p2.mv1" ) ;			// P2ポインタ   モデル読み込み //
	castle				= MV1LoadModel( "..\\Models\\castle.mv1" ) ;		// 城			モデル読み込み
	door_1				= MV1LoadModel( "..\\Models\\door.mv1" ) ;			// ドア			モデル読み込み
	door_2				= MV1DuplicateModel(door_1) ;						// ドア			モデル読み込み
	m_banner_red		= MV1LoadModel( "..\\Models\\BannerRed.mv1" ) ;    // バナー(赤)	モデル読み込み
	m_banner_blue		= MV1LoadModel( "..\\Models\\Banner.mv1" ) ;	   // バナー(青)	モデル読み込み

	// キャラセレクト用モデル読み込み
	mh_players[0][0] = MV1LoadModel( "..\\Models\\Magic_Red.mv1" )	;		// プレーヤーのモデル読み込み
	mh_players[0][1] = MV1LoadModel( "..\\Models\\PlayerRsRed.mv1" )	;	// プレーヤーのモデル読み込み
	mh_players[0][2] = MV1LoadModel( "..\\Models\\tenshRed.mv1" )	;		// プレーヤーのモデル読み込み
	mh_players[1][0] = MV1LoadModel( "..\\Models\\Magic_Bule.mv1" )	;		// プレーヤーのモデル読み込み
	mh_players[1][1] = MV1LoadModel( "..\\Models\\PlayerRsBlue.mv1" )	;	// プレーヤーのモデル読み込み
	mh_players[1][2] = MV1LoadModel( "..\\Models\\tenshBlue.mv1" )	;		// プレーヤーのモデル読み込み
	


	// --- サウンド読み込み
	sound.SoundInit() ;

	// --- モデルの影の読み込み
	g_player[PLAYER1].shadow = MV1DuplicateModel(shadow1) ;					// shadow model loading
	g_player[PLAYER2].shadow = MV1DuplicateModel(shadow1) ;					// shadow model loading
	MV1SetScale( g_player[PLAYER1].shadow ,VGet( 0.5f , 0.5f , 0.5f )) ;	// 弾丸shadow サイズ調整
	MV1SetScale( g_player[PLAYER2].shadow ,VGet( 0.5f , 0.5f , 0.5f )) ;	// 弾丸shadow サイズ調整
	
	// 弾のモデル読み込み------------------------------------------------------
	for ( int y = 0 ; y < 2 ; y++ ){
		for (int x = 0 ; x < BULLETMAX+1 ; x++ ){
			p_bullet[y][x].mhnd = MV1DuplicateModel(bullet[y]) ;
			p_bullet[y][x].shadow = MV1DuplicateModel(shadow1) ;							// shadow model loading
			p_bullet[y][x].booms = MV1DuplicateModel(boom) ;								// boom model loading
			p_bullet[y][x].booms2 = MV1DuplicateModel(boom2) ;								// boom2 model loading			
			MV1SetScale( p_bullet[y][x].mhnd ,VGet( 0.2f , 0.2f , 0.2f )) ;					// 弾丸サイズ調整
			MV1SetScale( p_bullet[y][x].shadow ,VGet( 0.25f , 0.25f , 0.25f )) ;			// 弾丸shadow サイズ調整
			p_bullet[y][x].EffectLoad() ;													// エフェクトロード
			for ( int z = 0 ; z < 9 ; z++ ) {
				MV1SetScale( p_bullet[y][x].ef_mhnd[z] ,VGet( 0.3f , 0.3f , 0.3f )) ;		// エフェクトのサイズ調整
			}
		}
	}

	// 座標設定----------------------------------------------------------------
	MV1SetPosition( ground, VGet(0.0f, -100.0f, 0.0f) )			; // 地面の座標を設定
	MV1SetPosition( ground_castle, VGet(0.0f, -100.0f, 0.0f) )	; // 地面の座標を設定
	MV1SetPosition( sky_castle, VGet(0.0f, -1000.0f, 0.0f) )	; // 空の座標を設定
	MV1SetPosition( kabe, VGet(0.0f, 100.0f, 1000.0f) )			; // 壁の座標を設定
	MV1SetPosition( sky, VGet(0.0f, -1000.0f, 0.0f) )			; // 空の座標を設定
	MV1SetPosition( land, VGet(500.0f, -160.0f, 0.0f) )			; // 空の座標を設定
	MV1SetPosition( p1mark, VGet(0.0f, 200.0f, 0.0f) )			; // p1ポインタの座標を設定
	MV1SetPosition( castle, VGet(2000.0f, 0.0f, 0.0f) )		    ; // 城の座標を設定
	MV1SetPosition( door_1, VGet(1935.0f, 127.0f, 138.0f) )	    ; // ドアの座標を設定
	MV1SetPosition( door_2, VGet(1935.0f, 127.0f, -138.0f) )    ; // ドアの座標を設定
	MV1SetPosition( m_banner_red , VGet(BANNER_POSX+1500, 200.0f,  BANNER_POSZ+700) )	; // バナー座標 
	MV1SetPosition( m_banner_blue , VGet(BANNER_POSX+1500, 200.0f, BANNER_POSZ-700) )    ; // バナー座標 

	// その他オブジェクトの初期セット-------------------------------
	StageInit()			; // 箱の座標を設定
	crown.CrownInit()	; // 王冠初期セット
	drone.DroneInit( )  ; // ドローン初期セット 
	balloonpos[0] = VGet( 1450 , -1000 , 950 ) ;
	balloonpos[1] = VGet( 1450 , -500 , 0 ) ;
	balloonpos[2] = VGet( 1450 , -1300 , -950 ) ;
	balloonpos[3] = VGet( -1450 , -800 , 950 ) ;
	balloonpos[4] = VGet( -1450 , -1800 , 0 ) ;
	balloonpos[5] = VGet( -1450 , -200 , -950 ) ;
	balloonpos[6] = VGet( 575 , -700 , -2200 ) ;
	balloonpos[7] = VGet( -575 , -600 , -2200 ) ;
	balloonpos[8] = VGet( 575 , -100 , 2200 ) ;
	balloonpos[9] = VGet( -575 , -1400 , 2200 ) ;

	// バルーンオブジェクト初期セット
	for ( int i = 0 ; i < 10 ; i++ ){
		balloon[i].BalloonInit( balloonpos[i] , (i%5) ) ;
	}

	// シーンチェンジ画像初期セット
	scf_alpha = 5 ;
	scbk_alpha = 5 ;
	scf_posx = 2000.0f ;

	// サイズ調整--------------------------------------------------------------
	MV1SetScale( ground ,VGet(STAGESCALE , 2.0f , STAGESCALE) )		;	// 地面			サイズ調整
	MV1SetScale( ground_castle ,VGet(STAGESCALE , 2.0f , STAGESCALE) )		;	// 地面			サイズ調整
	MV1SetScale( sky_castle ,VGet(3.0f , 3.0f , 3.0f) )					;	// 空			サイズ調整
	MV1SetScale( land ,VGet(35.0f ,35.0f , 35.0f) )					;	// 地面			サイズ
	MV1SetScale( kabe ,VGet(1.0f , 1.0f , 1.0f) )					;	// 壁			サイズ調整
	MV1SetScale( sky ,VGet(200.0f , 200.0f , 200.0f) )					;	// 空			サイズ調整
	MV1SetScale( player1 ,VGet(0.5f , 0.5f , 0.5f) )				;	// プレーヤー	サイズ調整
	MV1SetScale( player2 ,VGet(0.5f , 0.5f , 0.5f) )				;	// プレーヤー	サイズ調整
	MV1SetScale( cursor[0] ,VGet(0.08f , 0.08f , 0.08f) )				;	// プレーヤー	サイズ調整
	MV1SetScale( cursor[1] ,VGet(0.08f , 0.08f , 0.08f) )				;	// プレーヤー	サイズ調整
	MV1SetScale( p1mark ,VGet(0.4f , 0.4f , 0.4f) )					;	// プレーヤー	サイズ調整
	MV1SetScale( p2mark ,VGet(0.4f , 0.4f , 0.4f) )					;	// プレーヤー	サイズ調整
	MV1SetScale( castle ,VGet(2.0f , 2.0f , 2.0f) )					;	// 城			サイズ調整
	MV1SetScale( door_1 ,VGet(2.0f , 2.0f , 2.0f) )					;	// ドア			サイズ調整
	MV1SetScale( door_2 ,VGet(2.0f , 2.0f , 2.0f) )					;	// ドア			サイズ調整

	for ( int y = 0  ; y < 2 ; y++ ){
		for ( int x = 0 ; x < 3 ; x++ ){
			MV1SetScale( mh_players[y][x] ,VGet(0.9f , 0.9f , 0.9f) )		;	// プレーヤー	サイズ調整
		}
	}
	// 回転調整----------------------------------------------------------------
	MV1SetRotationXYZ(kabe,VGet(4.71f,0.0f,0.0f)) ;						//壁を回転させたい
	MV1SetRotationXYZ( door_1,VGet(0.0f,1.57f,0.0f)) ; 					// モデルの回転
	MV1SetRotationXYZ( door_2,VGet(0.0f,4.71f,0.0f)) ; 					// モデルの回転

	t_player[0].TitleInitSet( PLAYER1 , VGet( -1500.0f , 3.0f , 500.0f ) , 
										mh_players[PLAYER1][Magic] , Magic ) ;	// プレイヤー(モデル)の座標
	t_player[1].TitleInitSet( PLAYER2 , VGet( -1500.0f , 3.0f , 0.0f ) ,
										mh_players[PLAYER2][Neet] , Neet ) ;	// プレイヤー(モデル)の座標
	t_player[2].TitleInitSet( PLAYER1 , VGet( -1500.0f , -3.0f , -500.0f ) ,
										mh_players[PLAYER1][Angel] , Angel ) ;	// プレイヤー(モデル)の座標
	for ( int i = 0 ; i < 3 ; i++ ){
		MV1SetScale( t_player[i].model_hnd ,VGet(1.0f , 1.0f , 1.0f) )		;	// プレーヤー	サイズ調整
		MV1SetRotationXYZ( t_player[i].model_hnd ,VGet(0.0f , 1.57f , 0.0f) )		;	// プレーヤー	サイズ調整
		MV1SetPosition( t_player[i].model_hnd , t_player[i].pos ) ; 
	}

	// 後に変更する処理はここから下へ----------------------------------------------------------
	int white = GetColor( 255, 255, 255 );  // 文字描画用
	usebulletcnt = 0 ;						// 発射弾カウント

	sound.PlaySoundBGM( ) ;

}



