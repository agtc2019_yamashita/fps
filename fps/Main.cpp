//- 以下、テストコード (DxChara.xを使用)-//
#include "Common.h"

int WINAPI WinMain( HINSTANCE, HINSTANCE, LPSTR, int )
{

	ChangeWindowMode( FALSE );

	// ウインドウサイズの変更
	SetGraphMode(WINDOWSIZE_W,WINDOWSIZE_H,32) ;

	//SetWindowText( "DxLib:" DXLIB_VERSION_STR );
	if ( DxLib_Init( ) == -1 ) return -1;
	game_mode = eG_Init ; 

	SetDrawScreen( DX_SCREEN_BACK );				// 裏画面の設定
	while ( ProcessMessage( ) == 0 && CheckHitKey( KEY_INPUT_ESCAPE ) == 0 ) {
	
		ClearDrawScreen( );

		if ( sc_fin_fun != TRUE && game_mode != eG_Play ) {
			SetLightDirection(VGet( 50.0f , -200.0f , 50.0f ) ) ;
			SetLightAmbColor( GetColorF( 0.1 , 0.1 , 0.1 ,0.1) ) ;
		}
		skyroll -= 0.001f ;

		MV1SetRotationXYZ(sky,VGet(0.0f,skyroll,0.0f)) ;						//空を回転させたい


		switch ( game_mode )
		{
			case eG_Init :
				G_Init() ;
				game_mode = eTitle ;	// 次のゲームモードへ移行
				break ;

			case eTitle :
				Pad[PLAYER1] = GetJoypadInputState( DX_INPUT_PAD1 );        //パッド1入力取得
				Pad[PLAYER2] = GetJoypadInputState( DX_INPUT_PAD2 );        //パッド2入力取得
				Title() ;
				break ;

			case eCharaSelect :
				Pad[PLAYER1] = GetJoypadInputState( DX_INPUT_PAD1 );        //パッド1入力取得
				Pad[PLAYER2] = GetJoypadInputState( DX_INPUT_PAD2 );        //パッド2入力取得
				CharaSelect() ;	
				break ;

			case eCharaShow :
				CharaShow() ;
				break ;

			case eG_Play :
				Pad[PLAYER1] = GetJoypadInputState( DX_INPUT_PAD1 );        //パッド1入力取得
				Pad[PLAYER2] = GetJoypadInputState( DX_INPUT_PAD2 );        //パッド2入力取得
				G_Play() ;
				
				break ;

			case eG_Deth :
				G_Deth() ;

				break ;

			case eG_Respawn :
				G_Respawn() ;
				break ;

			case eG_SceneChange :
				G_SceneChange() ;
				break ;

			case eG_End :
				G_End() ;
				game_mode = eResult ;	// 次のゲームモードへ移行
				break ;


			case eResult :
				Pad[PLAYER1] = GetJoypadInputState( DX_INPUT_PAD1 );        //パッド1入力取得
				Pad[PLAYER2] = GetJoypadInputState( DX_INPUT_PAD2 );        //パッド2入力取得
				Result() ;

				break ; 

		}
		// --- BGMの音量を下げる
		sound.VolumeSub() ;

		ScreenFlip( );
	}
	DeleteGraph( gh_nonbullet ) ;
	DxLib_End( );
	return 0;
}//-----------------------------------------------------------------------------
// メッセージプロシージャ
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Name: InitApp()
// Desc: Initialize Window
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// DirectX 初期セット
// Name: InitDirectX()
// Desc: Initialize DirectX
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Name: Uninitialize()
// Desc: UnInitialize DirectX
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
