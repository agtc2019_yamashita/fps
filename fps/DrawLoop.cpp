/* -----------------------------------------------------------------------------------------
|	
|   描画処理　主に MV1DrawModel を実行
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include "Math.h"

void DrawLoop( )
{
	// モデルの座標設定----------------------------------------------------------------------
	MV1SetPosition( g_player[PLAYER1].model_hnd , g_player[PLAYER1].pos );							// キャラクターの座標セット
	MV1SetRotationXYZ( g_player[PLAYER1].model_hnd,VGet(0.0f,p_cam[PLAYER1].HRotate+3.14,0.0f)) ; 	// モデルの回転
	MV1SetPosition( p1mark, VAdd( g_player[PLAYER1].pos , VGet( 0.0f , 150.0f , 0.0f )) ) ;         //  1pポインタの座標設定
	MV1SetPosition( g_player[PLAYER1].shadow , VAdd( g_player[PLAYER1].pos , VGet( 0.0f , -95.0f , 0.0f ))  ) ;     // プレイヤー1の影の座標セット

	MV1SetPosition( g_player[PLAYER2].model_hnd , g_player[PLAYER2].pos );							// モデルの座標を設定
	MV1SetRotationXYZ( g_player[PLAYER2].model_hnd,VGet(0.0f,p_cam[PLAYER2].HRotate+3.14,0.0f)) ; 	// モデルの回転
	MV1SetPosition( p2mark, VAdd( g_player[PLAYER2].pos , VGet( 0.0f , 150.0f , 0.0f )) ) ;         //  2pポインタの座標設定
	MV1SetPosition( g_player[PLAYER2].shadow , VAdd( g_player[PLAYER2].pos , VGet( 0.0f , -95.0f , 0.0f ))  ) ;   

	if( game_mode == eG_Play )
	{
		if( g_player[PLAYER1].pos.x < g_player[PLAYER2].pos.x )
		{
			MV1SetRotationXYZ(p1mark,VGet(0.0f, -atan( ( g_player[PLAYER1].pos.z - g_player[PLAYER2].pos.z )/( g_player[PLAYER1].pos.x - g_player[PLAYER2].pos.x ) ) * 6.28f / 6.0f  ,0.0f)) ;   //1p2pポインタ回転
		}
		else
		{
			MV1SetRotationXYZ(p1mark,VGet(0.0f, -atan( ( g_player[PLAYER1].pos.z - g_player[PLAYER2].pos.z )/( g_player[PLAYER1].pos.x - g_player[PLAYER2].pos.x ) ) * 6.28f / 6.0f + 3.14f  ,0.0f)) ;  //1p2pポインタ回転
		}

		if( g_player[PLAYER1].pos.x < g_player[PLAYER2].pos.x )
		{
			MV1SetRotationXYZ(p2mark,VGet(0.0f, -atan( ( g_player[PLAYER1].pos.z - g_player[PLAYER2].pos.z )/( g_player[PLAYER1].pos.x - g_player[PLAYER2].pos.x ) ) * 6.28f / 6.0f + 3.14f ,0.0f)) ;   //1p2pポインタ回転
		}
		else
		{
			MV1SetRotationXYZ(p2mark,VGet(0.0f, -atan( ( g_player[PLAYER1].pos.z - g_player[PLAYER2].pos.z )/( g_player[PLAYER1].pos.x - g_player[PLAYER2].pos.x ) ) * 6.28f / 6.0f + 0.00f  ,0.0f)) ;  //1p2pポインタ回転
		}
	
		MV1DrawModel( p1mark );							// p1ポインタの表示
		MV1DrawModel( p2mark );	                        // p2ポインタの表示
	
	}

	// 弾の座標セット
	for ( int y = 0 ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE ) {
				MV1SetPosition( p_bullet[y][x].mhnd , p_bullet[y][x].pos );												// プレイヤー弾の座標セット
				MV1SetPosition( p_bullet[y][x].shadow ,VGet(p_bullet[y][x].pos.x , -99.0f , p_bullet[y][x].pos.z ) );   // プレイヤー1弾の影の座標セット liu
			}
		}
	}
	// エフェクトの座標セット
	for ( int y = 0 ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].ef_usingflg == TRUE ) {
				for ( int z = 0 ; z < 9 ; z++ ) {
					MV1SetPosition( p_bullet[y][x].ef_mhnd[z] , p_bullet[y][x].ef_pos );								// プレイヤーエフェクトの座標セット
					MV1SetRotationXYZ( p_bullet[y][x].ef_mhnd[z],VGet(0.0f,p_bullet[y][x].ef_rotate + 3.14f,0.0f)) ; 	// モデルの回転
				}
				p_bullet[y][x].ef_timecnt++ ;
				if (p_bullet[y][x].ef_timecnt > 10 ) {
					p_bullet[y][x].ef_timecnt = 0 ;
					p_bullet[y][x].ef_mcnt++ ;
				}
				if ( p_bullet[y][x].ef_mcnt >= 8 )
					p_bullet[y][x].ef_usingflg = FALSE ;
			}
		}
	}

	// モデルの表示---------------------------------------------------------------
	if ( limtime.time > 30 ) {
		MV1DrawModel( ground );								// ステージの表示
		MV1DrawModel( sky );								// 空
	}
	if ( limtime.time <= 30 && sc_fin_fun == TRUE ) {
		MV1DrawModel( ground_castle );							// ステージの表示
		MV1DrawModel( sky_castle );								// 空
	}

	MV1DrawModel( land );								// ステージの表示
	if ( game_mode >= eG_Play && game_mode != eG_Deth ) {
		MV1DrawModel( crown.m_crown ) ;					// 王冠表示
	}
	if ( game_mode == eG_Deth && crown.crowngetflg != 0 )
		MV1DrawModel( crown.m_crown ) ;					// 王冠表示

	MV1DrawModel( drone.mh_drone ) ;					// ドローン表示
	MV1SetRotationXYZ( castle,VGet(0.0f,1.57f,0.0f)) ; 	// 城の回転

	MV1DrawModel( door_1 ) ;					// ドア表示
	MV1DrawModel( door_2 ) ;					// ドア表示

	// --- シーンチェンジまで描画
	if ( sc_fin_fun == FALSE ) 
		MV1DrawModel( castle );							// 城の表示
	if ( game_mode != eG_SceneChange ) {
		for ( int i = 0 ; i < 10 ; i++ )
			MV1DrawModel( balloon[i].mh_balloon ) ;			// 風船表示
	}
	// プレイヤー描画
	if ( game_mode >= eCharaShow ){
		MV1DrawModel( g_player[PLAYER1].model_hnd );	// 1P表示
		MV1DrawModel( g_player[PLAYER2].model_hnd );	// 2P表示
	}

	// 箱OR椅子表示
	if( game_mode != eCharaSelect ){ 
		if ( sc_fin_fun == FALSE ){
			for ( int i = 0 ; i < BLOCKMAX ; i++ ){
				MV1DrawModel( stage[i].m_block ) ;
				MV1DrawModel( stage[i].m_shadow ) ;
			}
		}else{		
			for ( int i = 0 ; i < CHAIRMAX ; i++ ){
				MV1DrawModel( stage[i].m_chair ) ;
				MV1DrawModel( stage[i].m_cshadow ) ;
			}
			for ( int i = 0 ; i < 3 ; i++ ){
				MV1DrawModel( stage[i].m_block ) ;
				MV1DrawModel( stage[i].m_shadow ) ;			
			}
		}
	}

	// バナー表示
	if( game_mode != eCharaSelect ){ 
		MV1DrawModel( m_banner_red ) ;
		MV1DrawModel( m_banner_blue ) ;
	}


	// 弾＆弾の影表示
	for ( int i = 0 ; i < BULLETMAX+add_bullet ; i++ ){
		if ( p_bullet[PLAYER1][i].usingflg == TRUE )
		{
			MV1DrawModel( p_bullet[PLAYER1][i].mhnd );
			MV1DrawModel( p_bullet[PLAYER1][i].shadow );
		}
		if ( p_bullet[PLAYER2][i].usingflg == TRUE )
		{	
			MV1DrawModel( p_bullet[PLAYER2][i].mhnd );
			MV1DrawModel( p_bullet[PLAYER2][i].shadow );
		}
		// --- エフェクト表示
		if ( p_bullet[PLAYER1][i].ef_usingflg == TRUE )
		{	
			MV1DrawModel( p_bullet[PLAYER1][i].ef_mhnd[p_bullet[PLAYER1][i].ef_mcnt] );
		}
		if ( p_bullet[PLAYER2][i].ef_usingflg == TRUE )
		{	
			MV1DrawModel( p_bullet[PLAYER2][i].ef_mhnd[p_bullet[PLAYER2][i].ef_mcnt] );
		}

	}

	// 爆発の描画
	for ( int y = 0  ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if (  p_bullet[y][x].exploflg == TRUE ){
				MV1DrawModel( p_bullet[y][x].booms );
				MV1DrawModel( p_bullet[y][x].booms2 );
			}
		}
	}

	if ( game_mode >= eG_Play ) {
		MV1DrawModel( g_player[PLAYER1].shadow );       // 1P影
		MV1DrawModel( g_player[PLAYER2].shadow );       // 2P影
	}
}



/* -----------------------------------------------------------------------------------------
|	
|   描画処理　主に UI画像描画
|		
+ --------------------------------------------------------------------------------------- */
void UIDrawLoop() {

	// --- 枠線
	DrawGraph( 0  , 0 , gh_waku, true ) ;

	// 弾数表示
	for ( int y = 0 ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( (p_bullet[y][x].usingflg == TRUE) || (p_bullet[y][x].exploflg == TRUE) ) {		
				usebulletcnt++ ;
			}
		}
		for ( int i = BULLETMAX+add_bullet ; i > 0 ; i-- ){
			DrawGraph( 10 + ((WINDOWSIZE_W-110) * y) , 100 * i , gh_nonbullet, true ) ;
		}
		for ( int i = 0 ; i < (BULLETMAX+add_bullet-usebulletcnt) ; i++ )
		{
			DrawGraph( 10 + ((WINDOWSIZE_W-110) * y), 100 * (BULLETMAX+add_bullet-i) , gh_usebullet[y], true ) ;
		}
		usebulletcnt = 0 ;
	}
	minimap.DrawMiniMap() ;
	limtime.TimeOutput() ;
	ScoreCount() ;
	ManualDraw() ;

	// 王冠ゲットマニュアル画像描画(1P)
	if ( crown.getdrawflg[PLAYER1] == TRUE && game_mode == eG_Play ) {
		DrawRotaGraph( WINDOWSIZE_W / 4 , WINDOWSIZE_H / 2 ,
									1.0 , 0 , gh_oukanget , TRUE , 0 , 0) ;
	}
	// 王冠ゲットマニュアル画像描画(2P)
	if ( crown.getdrawflg[PLAYER2] == TRUE && game_mode == eG_Play ) {
		DrawRotaGraph( (WINDOWSIZE_W / 4) * 3 , WINDOWSIZE_H / 2 ,
									1.0 , 0 , gh_oukanget , TRUE , 0 , 0) ;
	}
	// 王冠ゲット画像描画(1P)
	if ( crown.crowngetflg == (PLAYER1 + 1) && game_mode == eG_Play && crown.getafdrawflg == TRUE ) {
		infotime++ ;
		DrawRotaGraph( WINDOWSIZE_W / 4 , WINDOWSIZE_H / 4 * 3 ,
									1.0 , 0 , gh_oukangetaf , TRUE , 0 , 0) ;
		if ( infotime > 60 ) {
			crown.getafdrawflg = FALSE ;
			crown.desdrawflg = 0 ;
			infotime = 0 ;
		}
	}
	// 王冠ゲット画像描画(2P)
	if ( crown.crowngetflg == (PLAYER2 + 1) && game_mode == eG_Play && crown.getafdrawflg == TRUE ) {
		infotime++ ;
		DrawRotaGraph( WINDOWSIZE_W / 4 * 3 , WINDOWSIZE_H / 4 * 3 ,
									1.0 , 0 , gh_oukangetaf , TRUE , 0 , 0) ;
		if ( infotime > 60 ) {
			crown.getafdrawflg = FALSE ;
			crown.desdrawflg = 0 ;
			infotime = 0 ;
		}
	}

	// 王冠ロスお知らせ画像描画(1P)
	if ( crown.desdrawflg == 1 ) {
		infotime++ ;
		DrawRotaGraph( WINDOWSIZE_W / 4 , WINDOWSIZE_H / 4 * 3 ,
									1.0 , 0 , gh_oukandes , TRUE , 0 , 0) ;
		if ( infotime > 60 ) {
			crown.desdrawflg = 0 ;
			infotime = 0 ;
		}
	}
	// 王冠ロスお知らせ画像描画(2P)
	if ( crown.desdrawflg == 2 ) {
		infotime++ ;
		DrawRotaGraph( (WINDOWSIZE_W / 4) * 3 , WINDOWSIZE_H / 4 * 3 ,
									1.0 , 0 , gh_oukandes , TRUE , 0 , 0) ;
		if ( infotime > 60 ) {
			crown.desdrawflg = 0 ;
			infotime = 0 ;
		}
	}

	// 38秒以下でスクリーンチェンジ画像を出していなかったら
	if ( limtime.time < 38 && sc_finish == FALSE ) {
		SC_UIMove() ;
	}

}









