
/* -----------------------------------------------------------------------------------------
|	
|   BGM ・ SE 処理
|		
+ --------------------------------------------------------------------------------------- */

class Sound {

	public :
		Sound() ;		// --- コンストラクタ
		~Sound() ;		// --- デストラクタ

		// --- イカれたメンバ関数達
		void SoundInit( ) ;							// --- サウンドの読み込みなど
		void PlaySoundBGM( ) ;						// --- BGM再生
		void PlaySoundSE( int sound_number ) ;		// --- SE再生
		void VolumeAdd( ) ;							// --- 音量を上げる
		void VolumeSub( ) ;							// --- 音量を下げる

		// --- イカれたメンバ変数達
		char        b_title_path[ 256 ]		;	// BGM パス
		char        string[ 256 ]			;	// pass格納する
		char        s_boom_path[ 256 ]		;	// 爆発音ファイル
		char        s_shoot_path[ 256 ]		;	// SEパス
		int         b_title_handle			;	// タイトル		BGMハンドル
		int			b_charaslc_handle       ;   // キャラ選		BGMハンドル
	    int        	b_gplay_handle          ;   // 戦闘			BGMハンドル
	    int        	b_g2play_handle         ;   // 戦闘			BGMハンドル
	    int        	b_result_handle         ;   // リザルト		BGMハンドル
		int		    b_loopstartposition		;	// スタート位置
		int         b_loopendposition		;	// 終了位置
		int         s_boom					;	// 爆発音
		int         s_shoot					;	// 発砲音
		int         s_charaslc              ;   // キャラ選音
		int         s_commit                ;   // 決定音
		int         s_cancel                ;   // キャンセル音
		int			s_ready					;	// 準備完了音
		int			s_cursor				;	// 操作選択
		int			s_countdown				;	// カウントダウン
		int			s_roll					;	// スネアロール
		int			s_jan					;	// スネアロール締め
		int			s_crownget				;	// 王冠ゲット
		int			s_kansei				;	// 歓声
		int			s_vs					;	// VS時SE

		int			volume_title			;	// タイトル音量
		int			volume_charaselect		;	// キャラ選択音量
		int			volume_gameplay			;	// メインゲーム音量
		int			volume_gameplay2		;	// メインゲーム2音量
		int			volume_result			;	// リザルト音量
		int			fade_out				;	// 音量フェードアウト
		bool		changesoundflg			;	// 30秒処理

		int			now_playsound			;	// 何のBGMを流す・流れている


} ;