#include "Common.h"

float	sc_camerapos	=	1200.0f ;		// カメラX座標
float	sc_cameraadd	=	0.5f ;			// カメラ足しこみ速度
bool	sc_initflg		=	FALSE ;			// 初期セットは一回だけ
float	door1_rota		=	1.57f ;			// 左扉
float	door2_rota		=	4.71f ;			// 右扉
VECTOR	door_pos ;
bool	otflg = FALSE ;

void G_SceneChange( ) 
{
	if ( sc_initflg == FALSE ) {

		sc_initflg = TRUE ;			// --- 初期セットは一回だけ
		add_bullet = 1 ;
		stage[1].p_block = VGet( -1000.0f , -20.0f , 1800.0f ) ;
		stage[2].p_block = VGet( 1000.0f , -20.0f , -1800.0f ) ;
		for ( int i = 1 ; i < 3 ; i ++ ){
			MV1SetPosition( stage[i].m_block , stage[i].p_block );		// モデルの座標を設定
			MV1SetPosition( stage[i].m_shadow , VGet( stage[i].p_block.x+78.5,  -99.0f , stage[i].p_block.z+78.5) );		// モデルの座標を設定
		}

		// カメラの設定-----------------------------------------------------------
		SetDrawArea( 0 , 0 , WINDOWSIZE_W , WINDOWSIZE_H ) ;
		SetCameraScreenCenter( WINDOWSIZE_W/2, WINDOWSIZE_H/2 );            // 画面分割設定
		trans.SceneChangeInit() ;

	}
	SetCameraPositionAndTarget_UpVecY( VGet(sc_camerapos , 200.0f , 0.0f), VGet(2000.0f , 200.0f , 0.0f) ) ;
	sc_cameraadd += 0.17f ;
	sc_camerapos += sc_cameraadd ;

	if ( door1_rota < 3.26 )
		door1_rota += 0.02f ;
	if ( door2_rota > 3.02 )
		door2_rota -= 0.02f ;

	MV1SetRotationXYZ(door_1,VGet(0.0f,door1_rota,0.0f)) ;						//壁を回転させたい
	MV1SetRotationXYZ(door_2,VGet(0.0f,door2_rota,0.0f)) ;						//壁を回転させたい

	// 各モデルのMV1DrawModelを実行
	DrawLoop() ;

	trans.SceneChange( FAST ) ;								// シーンチェンジ(遅い)

	// --- 終了処理
	if ( trans.bo_finflg == TRUE ) {
		g_player[PLAYER1].pRespawn( P1_INIT_POS ) ;
		g_player[PLAYER2].pRespawn( P2_INIT_POS ) ;
		p_cam[PLAYER1].InitSet( PLAYER1 , P1_INIT_ROT ) ;		// p1カメラ
		p_cam[PLAYER2].InitSet( PLAYER2 , P2_INIT_ROT ) ;		// p2カメラ

		// 弾のリセット
		for ( int i = 0 ; i < BULLETMAX+add_bullet ; i++ ){
			p_bullet[PLAYER1][i].usingflg = FALSE ;
			p_bullet[PLAYER2][i].usingflg = FALSE ;
		}



		sc_fin_fun = TRUE ;
		limtime.time = 30 ;
		limtime.time_stopflg = FALSE ;
		game_mode = eG_Respawn ; 
	}

}


void SC_UIMove() 
{
	if ( scf_posx > WINDOWSIZE_W / 2 ) { 
		scbk_alpha += 2 ;
		scf_alpha += 2 ;
		scf_posx -= 7.8f ;
	}
	else {
		scbk_alpha -= 2 ;
		scf_alpha -= 2 ;
		scf_posx -= 7.8f ;

		if ( scbk_alpha < 0 )
			sc_finish = TRUE ;
	}

	// --- アナウンス背景
	SetDrawBlendMode( DX_BLENDMODE_ALPHA, scbk_alpha ) ;
	DrawRotaGraph( WINDOWSIZE_W / 2 , 200 , 1.0 , 0.0 , gh_sc_fontbk , TRUE , 0 , 0 ) ;
	SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
	// --- アナウンス文字
	SetDrawBlendMode( DX_BLENDMODE_ALPHA, scf_alpha ) ;
	DrawRotaGraph( (int)scf_posx , 200 , 1.0 , 0.0 , gh_sc_font , TRUE , 0 , 0 ) ;
	SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

}



