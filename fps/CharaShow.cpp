/* -----------------------------------------------------------------------------------------
|	
|   対戦前のキャラ表示
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include "Math.h"

int areamove = WINDOWSIZE_W/2 ;		// 描画範囲設定用変数
int showtimecnt = 0 ;				// シーン切替のタイミング調整
int vs_move	= WINDOWSIZE_W ;
int vsrottime = 5 ;
int vsrot = 1 ;
bool vs_soundflg = FALSE ;
void CharaShow()
{

	// 描画範囲移動
	if ( areamove > 0){
		areamove -= 12 ;
		vs_move -= 24 ;
	}else{
		vsrottime-- ;
	} 

	if ( vsrottime <= 0 ){
		vsrottime = 5 ;
		vsrot *= -1 ;
		if ( vs_soundflg == FALSE ){
			sound.PlaySoundSE( s_Vs ) ;
			vs_soundflg = TRUE ;
		}
	}

	p_cam[PLAYER1].SetPos( g_player[PLAYER1].pos) ;
	p_cam[PLAYER2].SetPos( g_player[PLAYER2].pos) ;
	p_cam[PLAYER1].pos.x += 180 ;
	p_cam[PLAYER1].pos.z += 280 ;
	p_cam[PLAYER2].pos.x += 180 ;
	p_cam[PLAYER2].pos.z -= 280 ;

	// 1P描画--------------------------------------------------------------------------
	SetCameraNearFar( 50.0f,  150000.0f ) ;												// 描画距離を設定
	SetCameraPositionAndTarget_UpVecY( p_cam[PLAYER1].pos , g_player[PLAYER1].pos ) ; 	// カメラの位置と向きの設定
	SetDrawArea( -areamove, 0, (WINDOWSIZE_W/2)-areamove, WINDOWSIZE_H );	            // 画面分割設定(描画範囲)
	SetCameraScreenCenter( ((WINDOWSIZE_W/4)-areamove) , WINDOWSIZE_H/2 );				// 画面分割設定

	// 各モデルのMV1DrawModelを実行
	DrawLoop() ;
	DrawRectGraph( -areamove , 0 , 0 , 0 , WINDOWSIZE_W/2 , WINDOWSIZE_H , gh_charaselect_waku , TRUE , FALSE , FALSE ) ; 
	DrawRotaGraph( ((WINDOWSIZE_W/4)-vs_move)+340+(-2* vsrot) , WINDOWSIZE_H/2 , 1.8 , -0.02*vsrot , gh_V , TRUE , FALSE , FALSE ) ; 

	// 2P描画--------------------------------------------------------------------------
	SetCameraNearFar( 200.0f,  150000.0f ) ;												// 描画距離を設定
	SetCameraPositionAndTarget_UpVecY( p_cam[PLAYER2].pos , g_player[PLAYER2].pos ) ; 	// カメラの位置と向きの設定
	SetDrawArea( (WINDOWSIZE_W/2)+areamove, 0, WINDOWSIZE_W+areamove, WINDOWSIZE_H );	// 画面分割設定(描画範囲)
	SetCameraScreenCenter( areamove + (WINDOWSIZE_W/4*3) , WINDOWSIZE_H/2 );            // 画面分割設定

	// 各モデルのMV1DrawModelを実行
	DrawLoop() ;
	DrawRectGraph( (WINDOWSIZE_W/2)+areamove , 0 , WINDOWSIZE_W/2 , 0 , WINDOWSIZE_W/2 , WINDOWSIZE_H , gh_charaselect_waku , TRUE , FALSE , FALSE ) ; 
	DrawRotaGraph( vs_move + (WINDOWSIZE_W/4*3)-340+(2*vsrot) , WINDOWSIZE_H/2 , 1.8 , 0.02*vsrot , gh_S , TRUE , FALSE , FALSE ) ; 

	// 画面移動完了時のタメ
	if ( areamove <= 0 ){
		areamove = 0 ;
		showtimecnt++ ;

		// シーン切替
		if ( showtimecnt >= 120)
			game_mode = eG_Play ;
	}

}
