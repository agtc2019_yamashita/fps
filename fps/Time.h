
class Time {

	public :
		// --- メンバ関数
		Time() ;
		~Time() ;

		int TimeInit() ;
		int TimeOutput() ;

		// --- メンバ変数
		int m_number[10] ;						// --- 0〜9の数字

		int		oldtime ;						// --- パソコン立ち上げてからの時間
		int		newtime ;						// --- 更新された時間
		int		time    ;						// --- 時間格納変数
		int		time_1	;						// --- 十の位時間
		int		time_2	;						// --- 一の位時間
		int		timecnt ;						// --- 1秒カウント
		int		numbercolor ;					// --- 文字色
		float	numbersize ;					// --- 数字の拡縮率
		bool	sizeflg ;						// --- 大きさフラグ
		int		finishwaittime ;				// --- 終了待機時間
		int		finishalpha	;					// --- 画像透過変数(FINISH) 
		int		finishadd		;				// --- 画像透過変数(FINISH)
		int		finishadd_ypos		;			// --- 終了画像移動量Y
		float	finishpos_y	;					// --- 終了画像座標Y
		float	finishpos_x1 ;					// --- 1P終了画像座標X
		float	finishpos_x2 ;					// --- 2P終了画像座標X
		int		startcount ;					// --- 321カウントダウン
		double	number321size ;					// --- 数字のサイズ
		double	startsize ;						// --- startのサイズ
		int		numalpha ;						// --- 数字透過
		int		startalpha ;					// --- start透過
		int		numpos ;						// --- 数字ポジション
		
		bool	time_stopflg ;					// --- 時間を止めるフラグ( FALSE == 動く , TRUE == 止まる ) 		
		
		bool	soundflg ;						// --- カウントダウンSE様
} ;