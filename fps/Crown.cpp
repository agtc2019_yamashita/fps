#include <windows.h>
#include "Common.h"
#include <stdlib.h>
#include <time.h>


/* ----------------------------------------------------- */
/*					　コンストラクタ					 */
/* ----------------------------------------------------- */
Crown::Crown() {

}

/* ----------------------------------------------------- */
/*					　デストラクタ						 */
/* ----------------------------------------------------- */
Crown::~Crown() {

}

/* ----------------------------------------------------- */
/*					　	初期セット						 */
/* ----------------------------------------------------- */
int Crown::CrownInit() {

	m_crown = MV1LoadModel( "..\\Models\\crown.mv1")	;

	srand( (unsigned)time (NULL) ) ;			//乱す


	crowngetflg = 0 ;
	updownflg = TRUE ;
	p_crown.x = stage[0].p_block.x ;
	p_crown.y = stage[0].p_block.y + 220.0f ;
	p_crown.z = stage[0].p_block.z ;

	range_crown[0] = p_crown.y - 30.0f ;
	range_crown[1] = p_crown.y + 30.0f ;

	getdrawflg[PLAYER1] = FALSE ;
	getafdrawflg = FALSE ;
	crown.s_crown.y = 1.0f ;

	MV1SetPosition( m_crown , p_crown );						// モデルの座標を設定

	return 0 ;
}

/* ----------------------------------------------------- */
/*					　王冠ヒットチェック				 */
/* ----------------------------------------------------- */
int Crown::CrownCheck() {

	if ( limtime.time > 0 ) {
		for ( int i = 0 ; i < 2 ; i++ ) {
			// --- ブロック範囲チェック
			if (   (((p_crown.x - 250.0f) <= g_player[i].pos.x) && ((p_crown.x + 250.0f) >= g_player[i].pos.x))
				&& (((p_crown.z - 250.0f) <= g_player[i].pos.z) && ((p_crown.z + 250.0f) >= g_player[i].pos.z)) 
				&& crowngetflg == 0 ) {

				if ( i == 0 ){
					getdrawflg[PLAYER1] = TRUE ;
					if ( (Pad[PLAYER1] & PAD_INPUT_A) || (CheckHitKey(KEY_INPUT_C ) == 1) ){
						sound.PlaySoundSE( s_Crownget ) ;
						getafdrawflg = TRUE ;
						crowngetflg = i + 1 ;
					}
				}
				else {
					getdrawflg[PLAYER2] = TRUE ;
					if ( Pad[PLAYER2] & PAD_INPUT_A || (CheckHitKey(KEY_INPUT_N ) == 1) ){
						sound.PlaySoundSE( s_Crownget ) ;
						getafdrawflg = TRUE ;
						crowngetflg = i + 1 ;
					}
				}
			}
			else {
				if ( i == 0 ){
					getdrawflg[PLAYER1] = FALSE ;
				}
				else {
					getdrawflg[PLAYER2] = FALSE ;
				}

			}
		}
	}
	return 0 ;
}

/* ----------------------------------------------------- */
/*					　王冠移動							 */
/* ----------------------------------------------------- */
void Crown::CrownMove( int playernum ) {

	if ( crowngetflg == 1 && playernum == PLAYER1 ) {
		p_crown.x = g_player[PLAYER1].pos.x ;
		p_crown.y = g_player[PLAYER1].pos.y + 300.0f ;
		p_crown.z = g_player[PLAYER1].pos.z ;
		MV1SetPosition( m_crown , p_crown );						// モデルの座標を設定

	}

	if ( crowngetflg == 2 && playernum == PLAYER2 ) {
		p_crown.x = g_player[PLAYER2].pos.x ;
		p_crown.y = g_player[PLAYER2].pos.y + 300.0f ;
		p_crown.z = g_player[PLAYER2].pos.z ;
		MV1SetPosition( m_crown , p_crown );						// モデルの座標を設定

	}


}

/* ----------------------------------------------------- */
/*					　王冠破棄処理						 */
/* ----------------------------------------------------- */
int Crown::CrownDest() {
	

	if ( sc_fin_fun == FALSE ){
		rd = rand( ) % BLOCKMAX ;

		desdrawflg = crowngetflg ;
		crowngetflg = 0 ;

		p_crown.x = stage[rd].p_block.x ;
		p_crown.y = stage[rd].p_block.y + 220.0f ;
		p_crown.z = stage[rd].p_block.z ;
		MV1SetPosition( m_crown , p_crown );						// モデルの座標を設定

	}else{
		rd = rand( ) % 3 ;

		desdrawflg = crowngetflg ;
		crowngetflg = 0 ;

		switch ( rd ){
			case 0 :
			p_crown.x = stage[0].p_block.x ;
			p_crown.y = stage[0].p_block.y + 220.0f ;
			p_crown.z = stage[0].p_block.z ;
			break ;

			case 1 :
			p_crown.x = stage[1].p_block.x ;
			p_crown.y = stage[1].p_block.y + 220.0f ;
			p_crown.z = stage[1].p_block.z ;
			break ;

			case 2 :
			p_crown.x = stage[2].p_block.x ;
			p_crown.y = stage[2].p_block.y + 220.0f ;
			p_crown.z = stage[2].p_block.z ;
			break ;

		}

		MV1SetPosition( m_crown , p_crown );						// モデルの座標を設定
	}

	range_crown[0] = p_crown.y - 30.0f ;
	range_crown[1] = p_crown.y + 30.0f ;

	getdrawflg[PLAYER1] = FALSE ;
	getdrawflg[PLAYER2] = FALSE ;

	return 0 ;
}

/* ----------------------------------------------------- */
/*					　王冠その場回転					 */
/* ----------------------------------------------------- */
void Crown::CrownStay() {

	if ( updownflg == TRUE ) {
		if ( p_crown.y <= range_crown[0]  || p_crown.y >= range_crown[1]  )
			s_crown.y *= -1 ;
	}
	else {
		s_crown.y = 0.0f ;
	}
	r_crown += 3.14 / 80 ;

	p_crown.y += s_crown.y ;

	MV1SetPosition( m_crown , p_crown );					// --- 王冠の座標セット
	MV1SetRotationXYZ( m_crown ,VGet(0.0f,r_crown,0.0f)) ; 	// モデルの回転


}
