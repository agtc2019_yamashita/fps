#include <windows.h>
#include "Common.h"

FpsCamera::FpsCamera()
{
	// カメラの回転値を初期化
	HRotate = 0.0f ;
}

FpsCamera::~FpsCamera()
{

}

void FpsCamera::InitSet( int mode , float Hrot )
{
	// 1p 2pの設定
	pmode = mode ;
	HRotate = Hrot ;
}

void FpsCamera::HorizonAngle()
{
	
	
	// キーでカメラの水平方向回転値を変更
	switch ( pmode )
	{
		case 0 :	// 1p操作
			switch ( operation_mode )
			{
				case 0 : //　LRカメラ回転 ----------------------------
					if( Pad[PLAYER1] & PAD_INPUT_L)
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( Pad[PLAYER1] & PAD_INPUT_R)
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}

					if( CheckHitKey( KEY_INPUT_Q ) == 1 )
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( CheckHitKey( KEY_INPUT_E ) == 1 )
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}
					break ;

				case 1 :	// ○■カメラ回転----------------------
					if( Pad[PLAYER1] & PAD_INPUT_X)
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( Pad[PLAYER1] & PAD_INPUT_B)
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}

					if( CheckHitKey( KEY_INPUT_Q ) == 1 )
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( CheckHitKey( KEY_INPUT_E ) == 1 )
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}
					break ;
			}
			break ;

		case 1 :
			switch ( operation_mode )
			{
				case 0 : //　LRカメラ回転 ----------------------------
					if( Pad[PLAYER2] & PAD_INPUT_L)
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( Pad[PLAYER2] & PAD_INPUT_R)
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}

					if( CheckHitKey( KEY_INPUT_U ) == 1 )
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( CheckHitKey( KEY_INPUT_O ) == 1 )
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}
					break ;

				case 1 :	// ○■カメラ回転----------------------
					if( Pad[PLAYER2] & PAD_INPUT_X)
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( Pad[PLAYER2] & PAD_INPUT_B)
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}

					if( CheckHitKey( KEY_INPUT_U ) == 1 )
					{
						HRotate -= DX_PI_F / CAMROTSPD ;
					}
					if( CheckHitKey( KEY_INPUT_O ) == 1 )
					{
						HRotate += DX_PI_F / CAMROTSPD ;
					}
					break ;
			}
			break ;
	}
}

void FpsCamera::SetPos( VECTOR ppos )
{
	// プレイヤーのポジションをセット
	pos = ppos ;
	
	pos.y += 60.0f ;
}


