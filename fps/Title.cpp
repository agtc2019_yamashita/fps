/* -----------------------------------------------------------------------------------------
|	
|   タイトル画面
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include "Math.h"

VECTOR		title_cam			;		// タイトルカメラ
float		camera_angle		;		// カメラ角度

// --- タイトル文字
float		title_posx[7]  ;		// --- X座標
float		title_posy[7]  ;		// --- Y座標
float		title_spdx[7]  ;		// --- X速度
float		title_spdy[7]  ;		// --- Y速度
float		tfont_rota[7] ;			// --- 回転率
float		tfont_rotaspd ;			// --- 回転率
float		tfont_size[7] ;			// --- 拡縮率
float		subtitle ;				// --- サブタイトル拡縮率
bool		tfont_rfeflg[7] ;		// --- 反射フラグ
int			tfont_rotacnt ;			// --- 反射後回まで回れる
int			start_drawflg = 0 ;		// --- スタートボタン描画[ 0 : 描画しない 1 : 描画する ]
int			button_check = 3 ;		// --- FALSE : 押されてない TRUE : 押されている 3 : 初期スルー
int			start_drawcnt = 0 ;		// --- 点滅回数
int			start_cooltime = 0 ;	// --- スタート表示クールタイム



bool		titleinitflg = FALSE ;		// タイトル初期セットは一回



// --- プロトタイプ宣言
void Title_Init() ;				// --- タイトル初期セット
void Title_FontMove() ;			// --- タイトル文字移動
void Title_FontDraw() ;			// --- タイトル描画


void Title()
{	
	if ( titleinitflg == FALSE ) {
		titleinitflg = TRUE ;
		Title_Init() ;
	}


	SetCameraNearFar( 5.0f,  150000.0f ) ;				// --- 描画距離を設定


	camera_angle -= 0.004f ;							// --- カメラ角度

	title_cam.y = 1000.0f ;								// --- カメラの高さ
	title_cam.x = 4000*sin(camera_angle) ;				// --- カメラを視点中心に回転
	title_cam.z = 4000*cos(camera_angle) ;				// --- カメラを視点中心に回転

	SetCameraPositionAndTargetAndUpVec( title_cam, VGet(0.0f , 0.0f , 0.0f), VGet(0.0f , 1.0f , 0.0f) ) ;

	drone.Move() ;	// ドローン

	// 風船アニメーション
	for ( int i = 0 ; i < 10 ; i++ ){
		// --- 風船移動
		balloon[i].Move() ;
		balloon[i].Play_Anim( ) ;
		MV1SetAttachAnimTime( balloon[i].mh_balloon,balloon[i].attachidx,balloon[i].playtime) ;
	}

	DrawLoop() ;  
	sound.VolumeAdd( ) ;

	for ( int i = 0 ; i < 3 ; i++ ){
		MV1DrawModel( t_player[i].model_hnd ) ; 
		t_player[i].Play_Anim() ;
		MV1SetAttachAnimTime( t_player[i].model_hnd,t_player[i].attachidx,t_player[i].playtime) ;
	}

	if( ((Pad[PLAYER1] & PAD_INPUT_M) || (Pad[PLAYER2] & PAD_INPUT_M) || CheckHitKey( KEY_INPUT_RETURN ) == 1) && button_check == FALSE ) 
	{
		Pad[PLAYER1] = 0 ;
		Pad[PLAYER2] = 0 ;
		button_check = TRUE ;
		sound.PlaySoundSE( s_Ready ) ;
	}

	if ( button_check == FALSE ) {
		start_cooltime++ ;
		if ( start_cooltime > 30 ) {
			start_cooltime = 0 ;
			if ( start_drawflg == 1 )
				start_drawflg = 0 ;
			else 
				start_drawflg = 1 ;
		}
	}

	// --- 点滅を速く
	if ( subtitle > 0.5f && button_check == TRUE ) {
		start_cooltime++ ;
		if ( start_cooltime > 6 ) {
			start_cooltime = 0 ;
			start_drawcnt++ ;
			if ( start_drawflg == 1 )
				start_drawflg = 0 ;
			else 
				start_drawflg = 1 ;

			if ( start_drawcnt > 20 ) {
				game_mode = eCharaSelect;
				sound.now_playsound = s_CharaSelect ;
				sound.PlaySoundBGM( ) ;
				StopSoundMem( sound.b_title_handle ) ;
			}
		}

	}

	Title_FontMove() ;
	Title_FontDraw() ;

}


void Title_Init() {

	float spd_shift = 0.0f ;

	for ( int i = 0 ; i < 7 ; i++ ) {
		title_posx[i] = (float)WINDOWSIZE_W ;		// --- X座標
		title_posy[i] = (float)WINDOWSIZE_H ;		// --- Y座標
		title_spdx[i] = -11.2f + spd_shift	;		// --- X速度
		title_spdy[i] = -10.0				;		// --- Y速度
		tfont_rota[i] = 0.0f				;		// --- 回転率
		tfont_rotaspd = 0.3f				;		// --- 回転率
		tfont_size[i] = 0.0f				;		// --- 拡縮率
		tfont_rfeflg[i] = FALSE				;		// --- 反射フラグ
		tfont_rotacnt = 0 ;					;		// --- 反射後回転回数

		spd_shift += 1.2f ;
		subtitle = 0.0f ;
	}

}

void Title_FontMove () {

	for ( int i = 0 ; i < 7 ; i++ ) {
		title_posx[i] += title_spdx[i] ;		// --- X座標
		title_posy[i] += title_spdy[i] ;		// --- Y座標

		// --- 上に行ったら反射
		if ( title_posy[i] < 100.0f ) {
			title_spdy[i] *= -1 ;
			tfont_rfeflg[i] = TRUE ;
		}

		// --- 回転して、反射したら0まで回転
		tfont_rota[i] += tfont_rotaspd ;
		if ( tfont_rota[i] > 6.28f && tfont_rfeflg[i] == FALSE )
			tfont_rota[i] = 0.0f ;

		// --- 規定位置で止める
		if ( tfont_rfeflg[i] == TRUE ) {
			if ( title_posy[i] > (float)(WINDOWSIZE_H / 9 * 3)){
				DrawRotaGraph( WINDOWSIZE_W/2 , 500.0f ,
								subtitle , 0.0f , gh_subtitle , TRUE , 0 , 0) ;
				if ( subtitle < 0.5f )
					subtitle += 0.002f ;
				else 
					if ( button_check == 3 )
						button_check = FALSE ;		// --- スタートボタンを押せるように


				title_spdx[i] = 0.0f ;
				title_spdy[i] = 0.0f ;
			}
			// --- 回転回数
			if ( tfont_rota[i] > 6.28f ) {
				tfont_rotacnt++ ;
				tfont_rota[i] = 0.0f ;
			}

			// --- 回転して、反射したら0まで回転
			if ( tfont_rota[i] > 0.0f && tfont_rotacnt >= 10  ) {
				tfont_rota[i] = 0.0f ;
				tfont_rotaspd = 0.0f ;
			}
		}

		// --- 規定サイズまで拡大
		if ( tfont_size[i] < 0.8f  ) {
			tfont_size[i] += 0.01f ;
		}

	}




}

void Title_FontDraw() {

	SetDrawArea( 0 , 0, WINDOWSIZE_W , WINDOWSIZE_H );							// --- 画面分割設定(描画範囲)

	for ( int i = 0 ; i < 7 ; i++ ) {
		DrawRotaGraph( title_posx[i] , title_posy[i] ,
								tfont_size[i] , tfont_rota[i] , gh_title[i] , TRUE , 0 , 0) ;
	}

	// --- スタートボタン描画
	if ( start_drawflg == TRUE ) 
		DrawRotaGraph( WINDOWSIZE_W / 2 , WINDOWSIZE_H / 4 * 3 , 1.0 , 0 , gh_startop , true ) ;


}



