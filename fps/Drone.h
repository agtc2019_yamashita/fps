
class Drone {
	public :
		Drone() ;
		~Drone() ;

		int DroneInit() ;	// 初期セット
		int Move();			// 回転移動

		// メンバ変数-------
		int		mh_drone ;	// モデルハンドル
		VECTOR	spd ;			// スピード
		VECTOR	pos ;			// ポジション
		float	pos_rot ;		// 回転移動角度
		double	model_rot ;		// モデルの回転
		
} ;


