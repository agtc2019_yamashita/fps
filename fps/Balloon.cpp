/* -----------------------------------------------------------------------------------------
|	
|   風船
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include <math.h>

/* ----------------------------------------------------- */
/*					　コンストラクタ					 */
/* ----------------------------------------------------- */
Balloon::Balloon(){

}

/* ----------------------------------------------------- */
/*					　デストラクタ						 */
/* ----------------------------------------------------- */
Balloon::~Balloon() {

}

/* ----------------------------------------------------- */
/*					　初期セット						 */
/* ----------------------------------------------------- */
int Balloon::BalloonInit ( VECTOR inipos , int color ){

	switch ( color )
	{
		case 0 :	// 黄色風船
			mh_balloon = MV1LoadModel( "..\\Models\\Balloon.mv1" ) ;
			break ;
		
		case 1 :	// 青色風船
			mh_balloon = MV1LoadModel( "..\\Models\\BalloonB.mv1" ) ;
			break ;

		case 2 :	// 緑色風船
			mh_balloon = MV1LoadModel( "..\\Models\\BalloonG.mv1" ) ;
			break ;

		case 3 :	// 桃色風船
			mh_balloon = MV1LoadModel( "..\\Models\\BalloonP.mv1" ) ;
			break ;

		case 4 :	// 赤色風船
			mh_balloon = MV1LoadModel( "..\\Models\\BalloonR.mv1" ) ;
			break ;
	}
	initpos = inipos ;
	pos = initpos ;
	pos_rot = 0 ;
	model_rot = 0 ;
	playtime	= 0.0f								; // キャラのアニメ進捗時間
	rootflm		= MV1SearchFrame(mh_balloon,"root")	;

	MV1SetScale( mh_balloon ,VGet(3.0f , 3.0f , 3.0f) )		; // サイズ調整

	// アニメーションの読み込み--------------------------------------
	anim_nutral = MV1LoadModel("..\\Models\\balloon_anim.mv1") ;

	// プレイヤー立ちアニメの時間を取得
	tmp_attachidx = MV1AttachAnim(mh_balloon,0,anim_nutral) ;

	anim_action_totaltime[eStand] = MV1GetAttachAnimTotalTime(mh_balloon,tmp_attachidx) ;
	MV1DetachAnim(mh_balloon,tmp_attachidx) ;

	attachidx = MV1AttachAnim(mh_balloon,0,anim_nutral) ;

	// アニメーションして動いてもその場で動いてるような状態
	MV1SetFrameUserLocalMatrix(mh_balloon,rootflm,MGetIdent()) ;

	return 0 ;
}

/* ----------------------------------------------------- */
/*					　アクション						 */
/* ----------------------------------------------------- */
int Balloon::Move() {
	
	if ( game_mode != eResult ) {
		pos.y += 3 ;
		if ( pos.y >= 2000 )
			pos.y += 2 ;
		if ( pos.y <= 0 )
			pos.y += 3 ;

		if ( pos.y >= 3000 )
			pos = initpos ;
	}
	else {
		pos.y += 3 ;
		if ( pos.y >= 1000 )
			pos.y += 2 ;
		if ( pos.y <= 0 )
			pos.y += 3 ;

		if ( pos.y >= 1500 )
			pos = initpos ;
	}
	MV1SetPosition( mh_balloon , pos ) ;	// --- モデルの座標を設定


	return 0 ;
}

/* -----------------------------------------------------------------------------------------
|
|       アニメーションの再生
|
+ --------------------------------------------------------------------------------------- */
int Balloon::Play_Anim( )
{
	anim_totaltime = anim_action_totaltime[eStand] ;

	playtime += 0.5f ;
	if(playtime> anim_totaltime)
		playtime = 0.0f ;

	return ( 0 ) ;
}





