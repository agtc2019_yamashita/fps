

class Transition {

	public :

		Transition() ;						// --- コンストラクタ
		~Transition() ;						// --- デストラクタ

		// --- メンバ関数
		void SceneChangeInit() ;			// --- 初期セット
		void SceneChange( int spd ) ;		// --- シーンチェンジ 引数 [ 0 : 速い , 1 : 遅い ]

		// --- メンバ変数
		float	bo_size ;					// トランジションサイズ
		float	bo_rotation ;				// トランジション回転率
		bool	bo_finflg ;					// シーンチェンジ終了 FALSE : 未 , TRUE : 終




} ;
