#include <windows.h>
#include "Common.h"


Time::Time() {
	// タイムの初期セット呼び出し
	TimeInit() ;
}

Time::~Time() {

}

int Time::TimeInit() {

	time			= GAMETIME ;				// --- ゲーム総時間
	numbercolor		= n_Green ;					// --- 数字色
	numbersize		= 1.0 ;						// --- 数字サイズ
	sizeflg			= 0 ;						// --- 大きくなったか
	finishalpha		= 0 ;						// --- finish画像のアルファ値
	finishadd		= 5 ;						// --- finish画像のアルファ加算値
	finishadd_ypos	= 51 ;						// --- finish画像のY座標ずらし
	finishpos_y		= WINDOWSIZE_H / 2 ;		// --- finish画像のY座標
	finishpos_x1	= WINDOWSIZE_W / 4 ;		// --- finish画像のX座標(1P)
	finishpos_x2	= WINDOWSIZE_W / 4 * 3 ;	// --- finish画像のX座標(2P)
	startcount		= 3 ;						// --- ゲーム開始前カウント	
	number321size   = 0 ;						// --- 数字のサイズ
	startsize		= 0 ;						// --- startのサイズ
	numalpha        = 0 ;						// --- 数字透過
	startalpha		= 255 ;						// --- start透過
	numpos			= WINDOWSIZE_H/2 - 380 ;	// --- カウント初期位置
	time_stopflg	= FALSE ;					// --- 動くように設定
	soundflg		= TRUE ;
	return 0 ;
}

int Time::TimeOutput() {


	if ( startcount > -2 ){
		timecnt++ ;
		if ( timecnt > 60 ) {
			startcount-- ;
			timecnt = 0 ;
			numalpha = 0 ;					// 透過度リセット
			numpos = WINDOWSIZE_H/2 - 380 ;	// ポジションリセット
			number321size = 0 ;				// サイズリセット
			if ( soundflg == TRUE ){
				sound.PlaySoundSE( s_Countdown ) ;
				soundflg = FALSE ;
			}

		}
		if ( (startcount <= 2) && (startcount >= 0) ){
			if ( (numpos <= WINDOWSIZE_H/2 + 30) && (numpos >= WINDOWSIZE_H/2 - 30) )	numpos += 4 ;
			else numpos += 13 ;
			if ( numpos < WINDOWSIZE_H/2 ){
				numalpha += 9 ;				// 透明度変化速度
				number321size += 0.17 ;		// サイズ変化速度
			}
			else if ( numpos > WINDOWSIZE_H/2 ){ 
				numalpha -= 9 ;				// 透明度変化速度
				number321size -= 0.17 ;		// サイズ変化速度
			}
			// α値設定　描画
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, numalpha ) ;
			DrawRotaGraph( (WINDOWSIZE_W/4*3) , numpos , number321size , 0 , gh_number321[startcount] , TRUE , FALSE , FALSE ) ; 
			DrawRotaGraph( (WINDOWSIZE_W/4) , numpos , number321size , 0 , gh_number321[startcount] , TRUE , FALSE , FALSE ) ; 
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}else if( startcount <= -1 ){
			if ( startsize < 2 )	startalpha -= 3 ;	// α値調整
			else startalpha -= 5 ;
			startsize += 0.075 ;			// フォントサイズ調整
			// α値設定　描画
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, startalpha ) ;
			DrawRotaGraph( (WINDOWSIZE_W/4*3) , WINDOWSIZE_H/2 , startsize , 0 , gh_start , TRUE , FALSE , FALSE ) ; 
			DrawRotaGraph( (WINDOWSIZE_W/4) , WINDOWSIZE_H/2 , startsize , 0 , gh_start , TRUE , FALSE , FALSE ) ; 
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}
	}
	
	if ( time > 0 && time_stopflg == FALSE ) {
		if ( startcount <= -1 )
			timecnt++ ;
		// --- 1秒カウント処理
		if ( timecnt > 60 ) {
			timecnt = 0 ;
			time-- ;
			// --- 残り30秒になったらオレンジ色にする
			if ( time > 10 && time <= 30 ) {
				numbercolor = n_Orange ;
			}
			// --- 残り10秒になったら赤色にする
			if ( time <= 10 ) {
				numbercolor = n_Red ;
			}
		}
		// --- 残り30秒になったらシーンチェンジ
		if ( time == 30 && time_stopflg == FALSE && sc_fin_fun == FALSE ) {
			time_stopflg = TRUE ;
			game_mode = eG_SceneChange ;
		}

		// --- 30秒になったら拡大処理を行う
		if ( time == 30 && sc_fin_fun == TRUE ) {
			sound.changesoundflg = TRUE ;
			sound.PlaySoundBGM( ) ;
			if ( numbersize < 1.6 && sizeflg == 0 )
				numbersize += 0.02f ;
			else 
				sizeflg = 1 ;

			if ( numbersize > 1.0 && sizeflg == 1  )
				numbersize -= 0.02f ;
			else 
				sizeflg = 0 ;
		}
		// --- 10秒になったら拡大処理を行う
		if ( time == 10 ) {
			if ( numbersize < 1.9 && sizeflg == 0 )
				numbersize += 0.03f ;
			else 
				sizeflg = 1 ;

			if ( numbersize > 1.0 && sizeflg == 1  )
				numbersize -= 0.03f ;
			else 
				sizeflg = 0 ;
		}

		time_1 = time / 10 ;			// --- 十の位計算
		time_2 = time - time_1 * 10 ;	// --- 一の位計算

		// --- 時計背景描画
		DrawRotaGraph( (WINDOWSIZE_W / 2)  , WINDOWSIZE_H - 88 , 0.8 , 0 , gh_numberback, true ) ;
		// --- 十の位描画
		DrawRectRotaGraph((WINDOWSIZE_W / 2) - 47 , WINDOWSIZE_H - 62 ,NUMBERSIZE_W * time_1,0,
			NUMBERSIZE_W,NUMBERSIZE_H, numbersize , 0.0 , gh_number[numbercolor],TRUE, 0 , 0) ;
		// --- 一の位描画
		DrawRectRotaGraph((WINDOWSIZE_W / 2) + 47 , WINDOWSIZE_H - 62 ,NUMBERSIZE_W * time_2,0,
			NUMBERSIZE_W,NUMBERSIZE_H, numbersize , 0.0 , gh_number[numbercolor],TRUE, 0 , 0) ;
	}

	// 終了処理
	if (time <= 0 && sc_fin_fun == TRUE ) {
		// --- 時計背景描画
		DrawRotaGraph( (WINDOWSIZE_W / 2)  , WINDOWSIZE_H - 88 , 0.8 , 0 , gh_numberback, true ) ;
		// --- 十の位描画
		DrawRectRotaGraph((WINDOWSIZE_W / 2) - 47 , WINDOWSIZE_H - 62 ,NUMBERSIZE_W * 0 ,0,
			NUMBERSIZE_W,NUMBERSIZE_H, numbersize , 0.0 , gh_number[numbercolor],TRUE, 0 , 0) ;
		// --- 一の位描画
		DrawRectRotaGraph((WINDOWSIZE_W / 2) + 47 , WINDOWSIZE_H - 62 ,NUMBERSIZE_W * 0,0,
			NUMBERSIZE_W,NUMBERSIZE_H, numbersize , 0.0 , gh_number[numbercolor],TRUE, 0 , 0) ;
		// --- 透過状態から可視化できる状態に
		if ( finishalpha < 255 ) {
			finishalpha += finishadd ;
			finishadd_ypos-- ;

		}
		// --- FINISHを出して何秒か後にシーン切り替え
		if ( finishalpha >= 255 && game_mode != eDeth ) {
			finishwaittime++ ;
			sound.now_playsound = s_Result ;
			sound.PlaySoundBGM( ) ;
			if ( finishwaittime > FINISHTIME ){
				game_mode = eG_End ;
			}
		}
		// 画像のアルファブレンドで描画
		// ( 描画した後ブレンドモードを元に戻す )
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, finishalpha ) ;
		DrawRotaGraph( (int)finishpos_x1 , (int)finishpos_y - finishadd_ypos , 1.5 , 0 , gh_finish, true ) ;
		DrawRotaGraph( (int)finishpos_x2 , (int)finishpos_y - finishadd_ypos , 1.5 , 0 , gh_finish, true ) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

	}
	return 0 ;
}



