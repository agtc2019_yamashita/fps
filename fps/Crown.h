


class Crown {

	public :
		// --- メンバ関数--------------------------------------------
		Crown() ;								// --- コンストラクタ
		~Crown() ;								// --- デストラクタ

		int		CrownInit() ;						// --- 王冠初期セット
		int		CrownCheck() ;						// --- 王冠範囲チェック
		int		CrownDest() ;						// --- 王冠破棄
		void	CrownMove(int playernum ) ;			// --- 王冠移動
		void	CrownStay() ;						// --- 王冠その場回転

		// --- メンバ変数--------------------------------------------
		VECTOR		p_crown	 ;					// --- 王冠ポジション
		VECTOR		s_crown  ;					// --- 王冠スピード
		VECTOR		crown_size ;				// --- 王冠拡縮率
		float		r_crown  ;					// --- 王冠回転率
		float		range_crown[2] ;			// --- 要素数 0 : 下限界 1 : 上限界

		int			m_crown  ;					// --- 王冠モデルハンドル
		int 		crowngetflg ;				// --- 王冠取得フラグ(0 : 無し , 1 : 1p取得 , 2 : 2p取得)
		int			getdrawflg[2] ;				// --- 王冠取得画像フラグ
		int			getafdrawflg ;				// --- 王冠取得後画像フラグ
		int			desdrawflg ;				// --- 王冠ロス画像フラグ(0 : 無し , 1 : 1pロス , 2 : 2pロス)
		int			updownflg ;					// --- 上下に動く処理をするか( FALSE = しない , TRUE = する )



} ;