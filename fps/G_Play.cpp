/* -----------------------------------------------------------------------------------------
|	
|   ゲームプレイ中 処理
|		
+ --------------------------------------------------------------------------------------- */
#include "Common.h"
#include <math.h>
void G_Play()
{

	sound.VolumeAdd( ) ;

	if ( limtime.time <= 25 )
		limtime.time_stopflg = TRUE ;

	//爆発の演出
	for ( int y = 0  ; y < 2 ; y++ ){                     
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].exploflg == TRUE ){
				p_bullet[y][x].Boomhandl() ;
			}
		}
	}

	// 入力&ヒットチェック----------------
	if ( limtime.startcount <= -1 ){
		for ( int i = 0 ; i < 2 ; i++ ){
			g_player[i].pAction() ;
			p_cam[i].HorizonAngle( ) ;
			g_player[i].PlayerHitcheck( VGet(0,0,0) ) ; // hitcheck関数
		}
	}
	// 弾のヒットチェック
	for ( int y = 0 ,  i = 1 ; y < 2 ; y++ , i-- ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE ){
//				BB_HitCheck( y , x ) ;		// 弾と弾のヒットチェック
				// プレイヤーやられ時処理
				if ( p_bullet[y][x].BulletHitCheck( g_player[i].pos ) == TRUE && limtime.time > 0 ){

					// --- 王冠を持っていなければ普通にやられる
					if ( crown.crowngetflg != (i + 1) ) {
						dethplayer = i ;
						g_player[i].playtime = 0.0f ;
						g_player[i].dethcnt++ ;
						g_player[y].killcnt++ ;
						g_player[i].spd =VGet( /*-sinf(p_cam[i].HRotate) * PLYSPD*/ 0.0f , 0.0f , /*-cosf(p_cam[i].HRotate) * PLYSPD*/ 0.0f ) ;
						DethInit() ;
					}
					if ( crown.crowngetflg == (i + 1) ){	// --- 王冠所持していれば一回無敵
						crown.CrownDest() ;					// --- 王冠を持っていたら失う
					}
				}
			}
		}
	}

	// --- 王冠の上下回転移動
	crown.CrownStay() ;
	// --- 王冠の範囲チェック
	crown.CrownCheck() ;
	// --- ドローン移動
	drone.Move() ;
	if ( sc_fin_fun == FALSE ) {
		// --- 風船移動
		for ( int i = 0 ; i < 10 ; i++ )
			balloon[i].Move() ;
	}

	// ポジション決定-----------------------------
	for ( int i = 0 ; i < 2 ; i++ ){
		g_player[i].Move();
		p_cam[i].SetPos(g_player[i].pos) ;
		crown.CrownMove( i ) ;

		// --- ゲームプレイ中で制限時間内だったら
		if ( (crown.crowngetflg == i + 1) && (game_mode == eG_Play) && (limtime.time > 0) ) {
			g_player[i].crownkeep++ ;
		}

	}

	// 弾アクション---------------------------------
	for ( int y = 0 ; y < 2 ; y++ ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){
			if ( p_bullet[y][x].usingflg == TRUE )
				p_bullet[y][x].Move() ;
		}
	}

	// アニメーション-----------------------------------------------------------------------
	for ( int i = 0 ; i < 2 ; i++ ){
		if ( Pad[i] != 0 ) {		// || CheckHitKeyAll() != 0  --- キーボードデバック用(1P,2Pの判別はできない)
			g_player[i].AnimMode_Run() ;
		}
		else {
			g_player[i].AnimMode_Nutral() ;
		}
		g_player[i].Play_Anim() ;
		MV1SetAttachAnimTime(g_player[i].model_hnd,g_player[i].attachidx,g_player[i].playtime) ;
	}

	// 風船アニメーション
	for ( int i = 0 ; i < 10 ; i++ ){
		balloon[i].Play_Anim( ) ;
		MV1SetAttachAnimTime( balloon[i].mh_balloon,balloon[i].attachidx,balloon[i].playtime) ;
	}

	// 描画--------------------------------------------------------------------------
	for ( int i = 0 ; i < 2 ; i++ ){	
		SetCameraNearFar( 35.0f,  150000.0f ) ;														// 描画距離を設定
		SetCameraPositionAndAngle( p_cam[i].pos , 0, p_cam[i].HRotate, 0 ) ;						// カメラの位置と向きの設定
		SetDrawArea( (WINDOWSIZE_W/2) *i, 0, WINDOWSIZE_W/2*(i+1), WINDOWSIZE_H );	                // 画面分割設定(描画範囲)
		SetCameraScreenCenter( (WINDOWSIZE_W/4)+((WINDOWSIZE_W/2)*i), WINDOWSIZE_H/2 );				// 画面分割設定

		// 各モデルのMV1DrawModelを実行
		DrawLoop() ;
	}

	// UI画像描画----------------------------------------------------------------------
	SetDrawArea( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H );		// 画面分割の設定(描画範囲)

	UIDrawLoop() ;			// --- UI画像描画まとめ
}

/* -----------------------------------------------------------------------------------------
|	
|   キルカウント表示 処理
|		
+ --------------------------------------------------------------------------------------- */
void ScoreCount() {
	
	int kill_1[2] ;			// 1Pキルカウント <要素数> 0 : 十の位 , 1 : 一の位
	int kill_2[2] ;			// 2Pキルカウント <要素数> 0 : 十の位 , 1 : 一の位
	float scoresize = 1.0 ;	// スコアサイズ変更

	kill_1[0] = g_player[PLAYER1].killcnt / 10 ;				// --- 十の位計算
	kill_1[1] = g_player[PLAYER1].killcnt - kill_1[0] * 10 ;	// --- 一の位計算

	kill_2[0] = g_player[PLAYER2].killcnt / 10 ;				// --- 十の位計算
	kill_2[1] = g_player[PLAYER2].killcnt - kill_2[0] * 10 ;	// --- 一の位計算

	if ( limtime.time < 30 ) {
		kill_1[0] = 10 ;
		kill_1[1] = 10 ;
		kill_2[0] = 10 ;
		kill_2[1] = 10 ;
	}

	// --- 十の位描画(1P)
	DrawRectRotaGraph(50 , 50 ,SCORESIZE_W * kill_1[0],0,
		SCORESIZE_W,SCORESIZE_H, scoresize , 0.0 , gh_score,TRUE, 0 , 0) ;
	// --- 一の位描画(1P)
	DrawRectRotaGraph(50 + SCORESIZE_W , 50 ,SCORESIZE_W * kill_1[1],0,
		SCORESIZE_W,SCORESIZE_H, scoresize , 0.0 , gh_score,TRUE, 0 , 0) ;
	// --- キル文字描画(1P)
	DrawRotaGraph(65 + (SCORESIZE_W*2) , 60 , 0.7 ,0 , gh_kill ,true , 0 , 0) ;

	// --- 十の位描画(2P)
	DrawRectRotaGraph( WINDOWSIZE_W - 150 - SCORESIZE_W , 50 ,SCORESIZE_W * kill_2[0],0,
		SCORESIZE_W,SCORESIZE_H, scoresize , 0.0 , gh_score,TRUE, 0 , 0) ;
	// --- 一の位描画(2P)
	DrawRectRotaGraph( WINDOWSIZE_W - 150 , 50 ,SCORESIZE_W * kill_2[1],0,
		SCORESIZE_W,SCORESIZE_H, scoresize , 0.0 , gh_score,TRUE, 0 , 0) ;
	// --- キル文字描画(2P)
	DrawRotaGraph( WINDOWSIZE_W - 75 , 60 , 0.7 ,0 , gh_kill ,true , 0 , 0) ;
	

}

/* -----------------------------------------------------------------------------------------
|	
|   操作方法画像表示 処理
|		
+ --------------------------------------------------------------------------------------- */
void ManualDraw() {

	double manualsize = 1.5 ;

	for ( int i = 0 ; i < 2 ; i++ ) {

		if ( g_player[i].operation_mode == 0 ) {

			if ( g_player[i].pmode == PLAYER1 ) {
				// --- LRカメラボタン描画(1P)
				DrawRectRotaGraph( 150 , 300 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_lrbutton , TRUE , 0 , 0) ;

				// --- 〇攻撃ボタン描画(1P)
				DrawRectRotaGraph( 80 , 350 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_atkbutton , TRUE , 0 , 0) ;
			}
			else {
				// --- LRカメラボタン描画(2P)
				DrawRectRotaGraph( 150 + WINDOWSIZE_W / 2 , 300 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_lrbutton , TRUE , 0 , 0) ;

				// --- 〇攻撃ボタン描画(2P)
				DrawRectRotaGraph( 80 + WINDOWSIZE_W / 2 , 350 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_atkbutton , TRUE , 0 , 0) ;
			}
		}
		if ( g_player[i].operation_mode == 1 ) {

			if ( g_player[i].pmode == PLAYER1 ) {
				// --- LR攻撃ボタン描画(1P)
				DrawRectRotaGraph( 150 , 300 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_lratk , TRUE , 0 , 0) ;

				// --- 〇□カメラボタン描画(1P)
				DrawRectRotaGraph( 130 , 350 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_cmrbutton , TRUE , 0 , 0) ;
			}
			else {
				// --- LR攻撃ボタン描画(2P)
				DrawRectRotaGraph( 150 + WINDOWSIZE_W / 2 , 300 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_lratk , TRUE , 0 , 0) ;

				// --- 〇□カメラボタン描画(2P)
				DrawRectRotaGraph( 130 + WINDOWSIZE_W / 2 , 350 + WINDOWSIZE_H / 2 , 0 , 0,
					200 , 100 , manualsize , 0.0 , gh_cmrbutton , TRUE , 0 , 0) ;
			}
		}
	}
}

/* -----------------------------------------------------------------------------------------
|	
|   弾と弾 処理
|		引数 : 撃った弾の
|		
+ --------------------------------------------------------------------------------------- */
void BB_HitCheck( int b_y , int b_x ) {

	for ( int y = 0 ,  i = 1 ; y < 2 ; y++ , i-- ){
		for ( int x = 0 ; x < BULLETMAX+add_bullet ; x++ ){

			// 自分と同じ弾以外
			if ( (y != b_y && (p_bullet[y][x].usingflg == TRUE) && (p_bullet[b_y][b_x].usingflg == TRUE)  ) ) {
				// プレイヤーと弾のヒットチェック
				if(HitCheck_Sphere_Sphere( p_bullet[b_y][b_x].pos , 28 ,
												p_bullet[y][x].pos , 28  ) == TRUE)
				{
					p_bullet[b_y][b_x].usingflg = FALSE ;
					p_bullet[y][x].usingflg = FALSE ;
					DrawRotaGraph3D( p_bullet[b_y][b_x].pos.x , p_bullet[b_y][b_x].pos.y , p_bullet[b_y][b_x].pos.z , 
																						1.0 , 0 , gh_explosion , TRUE , 0 , 0 ) ;
					break ;		// すぐ抜ける
				}
			}
		}
	}
}
