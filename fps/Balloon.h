
class Balloon {
	public :
		Balloon() ;
		~Balloon() ;

		int BalloonInit( VECTOR inipos , int color ) ;	// 初期セット
		int Move();			// 回転移動
		int Play_Anim() ;				// アニメーションの再生

		// メンバ変数-------
		int		mh_balloon	;	// モデルハンドル格納
		VECTOR  initpos ;		// 初期位置
		VECTOR	spd ;			// スピード
		VECTOR	pos ;			// ポジション
		float	pos_rot ;		// 回転移動角度
		double	model_rot ;		// モデルの回転
		
		// アニメーション関連変数------------------------------------
		int rootflm						;			// モデルのルートフレーム（骨組み）の位置を格納
		int anim_nutral					;			// アニメーション格納ハンドル
		int attachidx					;			// キャラのアタッチインデックス
		float anim_totaltime			;			// 現在のキャラアニメーションの総時間
		float playtime					;			// キャラのアニメ進捗時間
		int tmp_attachidx				;			// アニメ総時間取得のための一時変数
		float anim_action_totaltime[10] ;			// 各アクションのアニメ総時間の格納配列
		int nowanim						;			// 現在再生しているアニメーションを設定　enumの定数を使用
} ;


