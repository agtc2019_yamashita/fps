#include <windows.h>
#include "Common.h"
#include <math.h>

Bullet::Bullet()
{
	usingflg = FALSE ;	// 不使用状態をセット
	exploflg = FALSE ;	// 爆発状態OFF
}
Bullet::~Bullet()
{
}

/* -----------------------------------------------------------------------------------------
|	弾の初期セット
|      プレイヤークラスで呼び出し 
|		ppos プレイヤーポジション　pspd プレイヤーの正面方向のスピード
+ --------------------------------------------------------------------------------------- */
void Bullet::InitSet( VECTOR ppos , VECTOR pspd , float hrotate )
{
	usingflg = TRUE ;			// 使用中モードに
	refcnt = 0 ;				// 反射回数リセット
	pos = ppos ;				// プレイヤーのポジションをセット
	ef_pos = pos ;
	pos.y -= 20.0 ;				// 銃弾の高さを砲台位置にセット
	refflg = FALSE ;			// 反射フラグをFALSEに
	refcnt = 0 ;				// 反射カウント初期セット
	bullet_hitflag = FALSE ;	// 銃弾ヒットフラグをFALSEに
	ply_hitflg = FALSE ;		// プレイヤーにヒットしたか
	exploflg = FALSE ;

	ef_usingflg = TRUE ;		// 使用フラグ
	ef_rotate = hrotate ;		// プレイヤー回転率をセット
	ef_mcnt = 0 ;				// モデル番号を0に
	ef_minusspd = 0.1f ;
	// スピードセット
	spd.x = pspd.x * BULLETSPD ;
	spd.z = pspd.z * BULLETSPD ;
	spd.y = 0.0f ;

	ef_spd.x = (pspd.x * 4.0f) - ef_minusspd ;	// エフェクトスピード
	ef_spd.z = (pspd.z * 4.0f) - ef_minusspd ;	// エフェクトスピード
	ef_spd.y = 0.0f ;

	// 初期位置調整
	pos.x += spd.x * 6.0f ;
	pos.y += 8.0f ; 
	pos.z += spd.z * 6.0f ;

	ef_pos.x += spd.x * 6.0f ;
	ef_pos.y += -4.0f ; 
	ef_pos.z += spd.z * 6.0f ;
}

/* -----------------------------------------------------------------------------------------
|	弾のヒットチェック
|      弾と箱　弾と壁　弾とプレイヤー 
|		　
+ --------------------------------------------------------------------------------------- */
bool Bullet::BulletHitCheck( VECTOR player_pos )
{
	VECTOR blo_pos ;

	if ( sc_fin_fun == FALSE ){
		for ( int i = 0 ; i <  BLOCKMAX ; i++ )
		{
			blo_pos = stage[i].p_block  ;
			if ( refcnt == 0 )
				refflg = TRUE ;
			if ( refcnt == BOUNDMAX - 1 )
				refflg = FALSE ;

			if( (pos.z < blo_pos.z + BLOCKSIZE_BOFF) && (pos.z > blo_pos.z -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x - (BLOCKSIZE_BOFF -40.0f)) && (pos.x > blo_pos.x - BLOCKSIZE_BOFF) && (spd.x > 0.0f) )
			{
				spd.x *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}
			if( (pos.z < blo_pos.z +  BLOCKSIZE_BOFF) && (pos.z > blo_pos.z -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x + BLOCKSIZE_BOFF)  && (pos.x > blo_pos.x +  (BLOCKSIZE_BOFF - 40.0f)) && (spd.x < 0.0f) )
			{
				spd.x *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x +  BLOCKSIZE_BOFF) && (pos.z > blo_pos.z - BLOCKSIZE_BOFF) && (pos.z < blo_pos.z - (BLOCKSIZE_BOFF - 40.0f)) && (spd.z > 0) )
			{
				spd.z *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x +  BLOCKSIZE_BOFF) && (pos.z > blo_pos.z + (BLOCKSIZE_BOFF - 40.0f)) && (pos.z < blo_pos.z + BLOCKSIZE_BOFF) && (spd.z < 0) )
			{
				spd.z *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}

			if ( (refcnt >= BOUNDMAX) && (refflg == TRUE) )
			{
				refcnt-- ;
				refflg = FALSE ;
			}
		}
	}else{
		for ( int i = 0 ; i < CHAIRMAX ; i++ ){
		
			blo_pos = stage[i].p_chair  ;

			if ( refcnt == 0 )
				refflg = TRUE ;
			if ( refcnt == BOUNDMAX - 1 )
				refflg = FALSE ;

			if ( i < 4 ){
				if( (pos.z < blo_pos.z + CHAIRSIZEZ_BOFF) && (pos.z > blo_pos.z -  CHAIRSIZEZ_BOFF) && (pos.x < blo_pos.x - (CHAIRSIZEX_BOFF -40.0f)) && (pos.x > blo_pos.x - CHAIRSIZEX_BOFF) && (spd.x > 0.0f) )
				{
					spd.x *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
				if( (pos.z < blo_pos.z +  CHAIRSIZEZ_BOFF) && (pos.z > blo_pos.z -  CHAIRSIZEZ_BOFF) && (pos.x < blo_pos.x + CHAIRSIZEX_BOFF)  && (pos.x > blo_pos.x +  (CHAIRSIZEX_BOFF - 40.0f)) && (spd.x < 0.0f) )
				{
					spd.x *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEX_BOFF) && (pos.x < blo_pos.x +  CHAIRSIZEX_BOFF) && (pos.z > blo_pos.z - CHAIRSIZEZ_BOFF) && (pos.z < blo_pos.z - (CHAIRSIZEZ_BOFF - 40.0f)) && (spd.z > 0) )
				{
					spd.z *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEX_BOFF) && (pos.x < blo_pos.x +  CHAIRSIZEX_BOFF) && (pos.z > blo_pos.z + (CHAIRSIZEZ_BOFF - 40.0f)) && (pos.z < blo_pos.z + CHAIRSIZEZ_BOFF) && (spd.z < 0) )
				{
					spd.z *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
			}else{
				if( (pos.z < blo_pos.z + CHAIRSIZEX_BOFF) && (pos.z > blo_pos.z -  CHAIRSIZEX_BOFF) && (pos.x < blo_pos.x - (CHAIRSIZEZ_BOFF -40.0f)) && (pos.x > blo_pos.x - CHAIRSIZEZ_BOFF) && (spd.x > 0.0f) )
				{
					spd.x *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
				if( (pos.z < blo_pos.z +  CHAIRSIZEX_BOFF) && (pos.z > blo_pos.z -  CHAIRSIZEX_BOFF) && (pos.x < blo_pos.x + CHAIRSIZEZ_BOFF)  && (pos.x > blo_pos.x +  (CHAIRSIZEZ_BOFF - 40.0f)) && (spd.x < 0.0f) )
				{
					spd.x *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEZ_BOFF) && (pos.x < blo_pos.x +  CHAIRSIZEZ_BOFF) && (pos.z > blo_pos.z - CHAIRSIZEX_BOFF) && (pos.z < blo_pos.z - (CHAIRSIZEX_BOFF - 40.0f)) && (spd.z > 0) )
				{
					spd.z *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
				if( (pos.x > blo_pos.x -  CHAIRSIZEZ_BOFF) && (pos.x < blo_pos.x +  CHAIRSIZEZ_BOFF) && (pos.z > blo_pos.z + (CHAIRSIZEX_BOFF - 40.0f)) && (pos.z < blo_pos.z + CHAIRSIZEX_BOFF) && (spd.z < 0) )
				{
					spd.z *= -1.0f ;
					refcnt++ ;
					bullet_hitflag = TRUE ;
				}
			}

			if ( (refcnt >= BOUNDMAX) && (refflg == TRUE) )
			{
				refcnt-- ;
				refflg = FALSE ;
			}
		}

		for ( int i= 0  ; i < 3 ; i++ )
		{
			// 中央ブロック
			blo_pos = stage[i].p_block  ;
			if ( refcnt == 0 )
				refflg = TRUE ;
			if ( refcnt == BOUNDMAX - 1 )
				refflg = FALSE ;

			if( (pos.z < blo_pos.z + BLOCKSIZE_BOFF) && (pos.z > blo_pos.z -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x - (BLOCKSIZE_BOFF -40.0f)) && (pos.x > blo_pos.x - BLOCKSIZE_BOFF) && (spd.x > 0.0f) )
			{
				spd.x *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}
			if( (pos.z < blo_pos.z +  BLOCKSIZE_BOFF) && (pos.z > blo_pos.z -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x + BLOCKSIZE_BOFF)  && (pos.x > blo_pos.x +  (BLOCKSIZE_BOFF - 40.0f)) && (spd.x < 0.0f) )
			{
				spd.x *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x +  BLOCKSIZE_BOFF) && (pos.z > blo_pos.z - BLOCKSIZE_BOFF) && (pos.z < blo_pos.z - (BLOCKSIZE_BOFF - 40.0f)) && (spd.z > 0) )
			{
				spd.z *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}
			if( (pos.x > blo_pos.x -  BLOCKSIZE_BOFF) && (pos.x < blo_pos.x +  BLOCKSIZE_BOFF) && (pos.z > blo_pos.z + (BLOCKSIZE_BOFF - 40.0f)) && (pos.z < blo_pos.z + BLOCKSIZE_BOFF) && (spd.z < 0) )
			{
				spd.z *= -1.0f ;
				refcnt++ ;
				bullet_hitflag = TRUE ;
			}

			if ( (refcnt >= BOUNDMAX) && (refflg == TRUE) )
			{
				refcnt-- ;
				refflg = FALSE ;
			}
		}
	}

	if ( pos.z >= STAGESIZE_BZ * STAGESCALE && spd.z > 0 )
	{
		spd.z *= -1.0f ;
		refcnt++ ;
		bullet_hitflag = TRUE ;
	}
	if ( pos.z <= -STAGESIZE_BZ * STAGESCALE && spd.z < 0)
	{
		spd.z *= -1.0f ;
		refcnt++ ;
		bullet_hitflag = TRUE ;
	}
	if ( pos.x >= STAGESIZE_BX * STAGESCALE && spd.x > 0 )
	{
		spd.x *= -1.0f ;
		refcnt++ ;
		bullet_hitflag = TRUE ;
	}
	if ( pos.x <= -STAGESIZE_BX * STAGESCALE && spd.x < 0 )
	{
		spd.x *= -1.0f ;
		refcnt++ ;
		bullet_hitflag = TRUE ;
	}

	if ( refcnt >= BOUNDMAX )
	{
		usingflg = FALSE ;
		exploflg = TRUE ;
		Boomset() ;
	}

	// プレイヤーと弾のヒットチェック
	if ( game_mode == eG_Play ){
		if(HitCheck_Capsule_Capsule( pos , pos , 30 , player_pos , player_pos , 40  ) == TRUE)
		{
			deth_plypos = player_pos ;
			usingflg = FALSE ;		
			ply_hitflg = TRUE ;
			exploflg = TRUE ;
			Boomset() ;
			bullet_hitflag = TRUE ;
		}
	}

	return ply_hitflg ;
}


void Bullet::Move()
{
	pos.x += spd.x ;
	pos.z += spd.z ;
	ef_pos.x += ef_spd.x ;
	ef_pos.z += ef_spd.z ;
	ef_minusspd += 1.0 ;
}

/* -----------------------------------------------------------------------------------------
|	
|   弾の爆発初期セット    
|		　
+ --------------------------------------------------------------------------------------- */
void Bullet::Boomset()
{
	plusminusflag = 1 ;
	plusminusflag2 = 1 ;
	
	burst = 0.1f ;
	burst2 = 0.1f ;

	if ( ply_hitflg == FALSE){
		MV1SetPosition( booms, VGet(pos.x, pos.y, pos.z) )			; // 爆発の座標を設定
		MV1SetPosition( booms2, VGet(pos.x, pos.y, pos.z) )			; // 爆発2の座標を設定
	}else{	
		MV1SetPosition( booms, VGet(deth_plypos.x, deth_plypos.y, deth_plypos.z) )			; // 爆発の座標を設定
		MV1SetPosition( booms2, VGet(deth_plypos.x, deth_plypos.y, deth_plypos.z) )			; // 爆発2の座標を設定
	}
	sound.PlaySoundSE( s_Explosion ) ;

}

/* -----------------------------------------------------------------------------------------
|	
|   爆発エフェクト再生    
|		　
+ --------------------------------------------------------------------------------------- */
void Bullet::Boomhandl()
{
	MV1SetScale( booms ,VGet( burst , burst , burst) )			; // 爆発	サイズ調整
	MV1SetScale( booms2 ,VGet( burst2 , burst2 , burst2 ) )		; // 爆発	サイズ調整	

	// 壁ヒット時爆発
	if ( ply_hitflg == FALSE){
		if(plusminusflag == 1 )
		{
			burst += 0.1f ;

			if(burst >= 1.0f)
			{
				plusminusflag = -1 ;
			}
		}
		else
		{
			burst -= 0.06f ;
		}

		if(plusminusflag2 == 1 )
		{
			burst2 += 0.06f ;

			if(burst2 >= 1.0f)
			{
				plusminusflag2 = -1 ;
			}
		}
		else
		{
			burst2 -= 0.1f ;
		}

		if ( burst <= 0 ){
			burst = 0 ;
		}

		if( burst2 <= 0 )
		{
			burst2 = 0  ;
			if ( burst <= 0 ){
				exploflg = FALSE ;
			}
		}
	}else{	// プレイヤーヒット時爆発
	
		if(plusminusflag == 1 )
		{
			burst += 0.2f ;

			if(burst >= 2.0f)
			{
				
				plusminusflag = -1 ;
			}
		}
		else
		{
			burst -= 0.045f ;
		}

		if(plusminusflag2 == 1 )
		{
			burst2 += 0.12f ;

			if(burst2 >= 2.0f)
			{
				plusminusflag2 = -1 ;
			}
		}
		else
		{
			burst2 -= 0.075f ;
		}

		if ( burst <= 0 ){
			burst = 0 ;
		}

		if( burst2 <= 0 )
		{
			burst2 = 0  ;
			if ( burst <= 0 ){
				exploflg = FALSE ;
			}
		}
	}

}

/* -----------------------------------------------------------------------------------------
|	
|   エフェクトモデル読み込み		(コンストラクタで使用)    
|		　
+ --------------------------------------------------------------------------------------- */
void Bullet::EffectLoad() {

	ef_mhnd[0] = MV1LoadModel( "..\\Models\\efect_9.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[1] = MV1LoadModel( "..\\Models\\efect_8.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[2] = MV1LoadModel( "..\\Models\\efect_7.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[3] = MV1LoadModel( "..\\Models\\efect_6.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[4] = MV1LoadModel( "..\\Models\\efect_5.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[5] = MV1LoadModel( "..\\Models\\efect_4.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[6] = MV1LoadModel( "..\\Models\\efect_3.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[7] = MV1LoadModel( "..\\Models\\efect_2.mv1" )	;	// エフェクト	モデル読み込み
	ef_mhnd[8] = MV1LoadModel( "..\\Models\\efect_1.mv1" )	;	// エフェクト	モデル読み込み
}